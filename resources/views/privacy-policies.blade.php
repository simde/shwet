@extends('layouts.app')

@section('content')
    <div class="p-4 flex flex-col gap-y-5 rounded-md bg-grey-200 dark:bg-grey-800 text-grey-950 dark:text-grey-50">
        <h1 class="uppercase font-extrabold text-2xl">{{ __('Privacy policy') }}</h1>
        <p>{{ __('Date of last update: 2024-10-31') }}</p>

        <p>{{ __('The text below aims to describe the policy for protecting data entered on the current site, Shwet.') }}</p>

        <h2 class="font-bold text-l">{{ __('Collected data') }}</h2>
        <p>{{ __('When you log in to Shwet, it is necessary to go through the SiMDE authentication portal (OAuth), which itself uses the UTC authentication portal (CAS). This authentication allows the Shwet site to collect and store the following user data:') }}</p>
        <ul class="list-disc ml-6">
            <li>{{ __('First and Last Name') }}</li>
            <li>{{ __('UTC Email Address (xxx@xxx.utc.fr)') }}</li>
        </ul>
        <p>{{ __('These details are required to associate a document deposited on Shwet with a specific user, the owner.') }}</p>
        <p>{{ __('Documents are files or links added by users and include metadata to be provided by the user. This metadata includes:') }}</p>
        <ul class="list-disc ml-6">
            <li>{{ __('UV: The UV code for which the user wishes to publish the document.') }}</li>
            <li>{{ __('Semester: The semester during which the document was created or obtained by the user.') }}</li>
            <li>{{ __('Type of Activity: The type of activity to which the document is related (e.g., exam, project, lab report, etc.).') }}</li>
            <li>{{ __('Document Type: Whether the document is a correction, a subject, or if the user created it themselves (Personal).') }}</li>
            <li>{{ __('Format: Whether the document is a file (PDF, Powerpoint, Word, etc.) or a link to an online document (Google Doc, etc.).') }}</li>
            <li>{{ __('Title: (OPTIONAL) Name given to the document if the user believes it necessary.') }}</li>
            <li>{{ __('License: CC0 (corresponding to the public domain) by default, indicates the document property and conditions of its usage. (Only applies to files, not links)') }}</li>
            <li>{{ __('Anonymous: If checked, no one will know who published the document. Otherwise, the owner\'s name will be displayed.') }}</li>
        </ul>
        <p>{{ __('Once on the site, the user preferences listed below are saved by the system:') }}</p>
        <ul class="list-disc ml-6">
            <li>{{ __('Theme (dark/light)') }}</li>
            <li>{{ __('Language') }}</li>
        </ul>

        <h2 class="font-bold text-l">{{ __('Data processing') }}</h2>
        <p>{{ __('All the data listed above is stored indefinitely to maintain the most comprehensive document database possible.') }}</p>
        <p>{{ __('All users have access to documents shared by each user on the platform. Users can request anonymous sharing of a document so that other users cannot know who owns it. However, administrators have the ability to view the identity of the user who published an anonymous document.') }}</p>

        <h3 class="italic">{{ __('Subcontractors under GDPR for this processing') }}</h3>
        <ul class="list-disc ml-6">
            <li>{{ __('Eliot DEWULF, president of SiMDE') }}</li>
            <li>{{ __('Quentin BOYER, vice-president of SiMDE') }}</li>
        </ul>

        <h3 class="italic">{{ __('SiMDE and administrators') }}</h3>
        <p>{{ __('All services are hosted on the SiMDE infrastructure, a committee of the BDE-UTC.') }}</p>
        <p>{{ __('Shwet administrators are volunteer members of SiMDE. To view the list of administrators, you can contact SiMDE at') }} <a href="mailto:simde@assos.utc.fr" class="text-purple-400 hover:underline">simde@assos.utc.fr</a></p>
        <p>{{ __('Shwet administrators have full access to the stored data.') }}</p>
        <p>{{ __('For maintenance and security purposes, administrators may consult and rectify the collected data.') }}</p>
        <p>{{ __('In accordance with the French Data Protection Act of January 6, 1978, and the General Data Protection Regulation (GDPR), you have the right to access, rectify, and delete information about yourself. To exercise these rights, you can contact SiMDE at') }} <a href="mailto:simde@assos.utc.fr" class="text-purple-400 hover:underline">simde@assos.utc.fr</a></p>

        <h2 class="font-bold text-l">{{ __('Cookies') }}</h2>
        <p>{{ __('This site does not use trackers. The only cookies used are for maintaining the current session.') }}</p>
    </div>
@endsection
