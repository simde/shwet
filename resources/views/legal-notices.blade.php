@extends('layouts.app')

@section('content')
    <div class="p-4 flex flex-col gap-y-5 rounded-md bg-grey-200 dark:bg-grey-800 text-grey-950 dark:text-grey-50">
        <h1 class="uppercase font-extrabold text-2xl">{{ __('Legal notices') }}</h1>

        <h2 class="font-bold text-l">{{ __('Presentation of the service and stakeholders') }}</h2>
        <p>{{ __('This site is a service provided by the Service informatique de la Maison Des Étudiants (SiMDE), a committee of the BDE-UTC, intended for members of the Université de Technologie de Compiègne.') }}</p>
        <p>{{ __('The use by members of UTC complies with various applicable texts, including notably the computer charter of UTC.') }}</p>
        <p>{{ __('This site is developed and maintained by volunteers of SiMDE, as long as it is active.') }}</p>

        <h3 class="italic">BDE-UTC</h3>
        <dl>
            <dt>{{ __('Bureau des étudiants de l\'Université de Technologie de Compiègne') }}</dt>
            <dt>{{ __('Official acronym:') }} BDE-UTC</dt>
            <dt>{{ __('Siret:') }} 39777672500012</dt>
            <dt>{{ __('Registered office:') }}</dt>
                <dd>UTC - Centre Benjamin Franklin</dd>
                <dd>MDE</dd>
                <dd>BDE-UTC</dd>
                <dd>Rue Roger Couttolenc</dd>
                <dd>60200 Compiègne</dd>
            <dt>{{ __('Phone:') }} +33 3 44 23 44 23</dt>
        </dl>

        <h2 class="font-bold text-l">{{ __('Rights and obligations of users') }}</h2>
        <p>{{ __('By using the Shwet site, the user consents to their data being collected, stored, retained, and processed by SiMDE, and to documents published and their metadata being shared with other Shwet users.') }}</p>
        <p>{{ __('By using the Shwet site, the user agrees to:') }}
            <ul class="list-disc ml-6">
                <li>{{ __('not deposit documents on the platform of which they are not the legal owner or for which they do not have the required sharing rights.') }}</li>
                <li>{{ __('not deposit documents that are not related to activities carried out within the university framework during their course at UTC.') }}</li>
                <li>{{ __('not disclose false information.') }}</li>
                <li>{{ __('not attempt to compromise the integrity of the Shwet site in any way.') }}</li>
                <li>{{ __('contact the SiMDE at the address') }} <a href="mailto:simde@assos.utc.fr" class="text-purple-400 hover:underline">simde@assos.utc.fr</a> {{ __('upon discovery of any bug.') }}</li>
            </ul>
        </p>
    </div>
@endsection
