@extends('layouts.app')

@section('info-banner')
    <x-info-banner>
        {{ __("You're currently using the new version of Shwet, freshly developed by SiMDE. Don't hesitate to contact us if you discover a bug.") }}
    </x-info-banner>
@endsection

@section('content')
    <div id="welcome-box" class="rounded-md p-8 flex gap-8 justify-around items-center text-grey-950 bg-grey-200 dark:text-grey-50 dark:bg-grey-800">
        <x-application-logos.rounded class="h-56 w-56 fill-current hidden md:block" />
        <div id="welcome-message" class="flex flex-col items-start gap-4 max-w-md lg:max-w-2xl">
            <div class="text-start">
                <h1 class="font-bold text-lg">{{__("Welcome to Shwet!")}}</h1>
            </div>
            <div>
                <p class="text-sm leading-relaxed">{!! __("Shwet is a collaborative plateform where you can <b>consult documents</b> (past papers, tutorials, practical work, revision sheets, ...) to help you with your <b>revisions</b>.") !!}</p>
                <p class="text-sm leading-relaxed">{!! __("These documents are <b>deposited by students</b> over the semesters, so don't forget to come and <b>add yours</b> to help others!") !!}</p>
            </div>
            <div class="flex justify-center items-center w-full">
                <x-buttons.icon-text-purple class="hover:scale-110" data-modal-target="modal-document" data-modal-show="modal-document">
                    <x-slot:icon>
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor"
                             class="bi bi-file-earmark-arrow-up" viewBox="0 0 16 16">
                            <path
                                d="M8.5 11.5a.5.5 0 0 1-1 0V7.707L6.354 8.854a.5.5 0 1 1-.708-.708l2-2a.5.5 0 0 1 .708 0l2 2a.5.5 0 0 1-.708.708L8.5 7.707z"/>
                            <path
                                d="M14 14V4.5L9.5 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2M9.5 3A1.5 1.5 0 0 0 11 4.5h2V14a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h5.5z"/>
                        </svg>
                    </x-slot:icon>
                    <x-slot:text>
                        {{__("Publish a document")}}
                    </x-slot:text>

                </x-buttons.icon-text-purple>
            </div>
        </div>
    </div>
    <div id="author-box" class="rounded-md p-8 flex gap-8 justify-around items-center text-grey-950 bg-grey-200 dark:text-grey-50 dark:bg-grey-800">
        <div id="author-message" class="flex flex-col items-start gap-4 max-w-md md:max-w-4xl">
            <div class="text-start">
                <h3 class="font-bold text-md">{{__("Authors' note")}}</h3>
            </div>
            <div>
                <p class="text-sm leading-relaxed mb-3">
                    {!! __("Shwet is an application offered by ") !!}
                    <x-link :href="'mailto:simde@assos.utc.fr'">SiMDE</x-link>
                    {!! __(". This version was developed by ") !!}
                    <x-link :href="'#'">Quentin BOYER</x-link>
                    {!! __(" and ") !!}
                    <x-link :href="'https://www.linkedin.com/in/eliot-dewulf-7b27541b8'">Eliot DEWULF</x-link>
                    {!! __(" as part of a TZ-A supervised by Mr. BONNET, based on the 2015 version developed by Nicolas SZEWE, Mewen MICHEL and Samuel FAVRICHON.") !!}
                </p>
                <p class="text-sm leading-relaxed">
                    {!! __("In case of bugs, questions, or proposals for new features, feel free to contact SiMDE on your preferred social networks!") !!}
                </p>
            </div>
        </div>
    </div>
@endsection
