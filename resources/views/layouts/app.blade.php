<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <link rel="icon" type="image/x-icon" href="/favicon.ico">

        <!--Crawlers data-->
        <meta name="description" content="Shwet est la plateforme collaborative de l'Université de Technologie de Compiègne sur laquelle chaque étudiant·e peut consulter des documents (annales, TDs, TPs, fiches de révisions, ...) pour l’aider dans ses révisions.">
        <meta name="keywords" content="Shwet, UTC, Université de Technologie de Compiègne, plateforme collaborative, annales, TD, TP, fiche, médian, final, étudiant, aide, révisions, sujet, correction, document, SiMDE">
        <meta name="author" content="Shwet">
        <meta name="robots" content="index, follow">
        <meta name="revisit-after" content="7 days">
        <meta name="language" content="{{ str_replace('_', '-', app()->getLocale()) }}">
        <meta name="publisher" content="Service informatique de la Maison Des Etudiants (SiMDE)">

        <!--  Open graph-->
        <meta property="og:title" content="Shwet - Ressources pour aider les étudiant·e·s dans leurs révisions">
        <meta property="og:description" content="Shwet est la plateforme collaborative de l'Université de Technologie de Compiègne sur laquelle chaque étudiant·e peut consulter des documents (annales, TDs, TPs, fiches de révisions, ...) pour l’aider dans ses révisions.">
        <meta property="og:image" content="/images/logos/ShwetBde.png">
        <meta property="og:url" content="https://shwet.assos.utc.fr">
        <meta property="og:type" content="website">

        <!--  Twitter-->
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:site" content="@Shwet">
        <meta name="twitter:creator" content="@SiMDE">
        <meta name="twitter:title" content="Shwet - Ressources pour aider les étudiant·e·s dans leurs révisions">
        <meta name="twitter:description" content="Shwet est la plateforme collaborative de l'Université de Technologie de Compiègne sur laquelle chaque étudiant·e peut consulter des documents (annales, TDs, TPs, fiches de révisions, ...) pour l’aider dans ses révisions.">
        <meta name="twitter:image" content="/images/logos/ShwetBde.png">

        <title>{{ config('app.name', 'Shwet') }}</title>

        <!-- Fonts -->
        <link rel="preconnect" href="https://fonts.bunny.net">
        <link href="https://fonts.bunny.net/css?family=figtree:400,500,600&display=swap" rel="stylesheet" />
        <link href="https://fonts.bunny.net/css?family=abeezee:400|annie-use-your-telescope:400" rel="stylesheet" />

        <!-- Styles -->
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

        <!-- Scripts -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/flowbite/2.3.0/flowbite.min.css" rel="stylesheet" />
        @vite(['resources/css/app.css', 'resources/js/app.js'])
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js" integrity="sha512-v2CJ7UaYy4JwqLDIrZUI/4hqeoQieOmAZNXBeQyjo21dadnwR+8ZaIJVT8EE2iyI61OV8e6M8PP2/4hpQINQ/g==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    </head>
    <body class="font-sans antialiased bg-white dark:bg-grey-950">
        <div class="min-h-screen bg-white dark:bg-grey-950">
            <header class="sticky top-0 z-30 shadow">
                @include('layouts.navigation')
            </header>
            @yield('info-banner')
            <x-toast-messages />

            <!-- Page Content -->
            <main class="m-auto max-w-6xl">
                <div class="flex flex-col p-4 gap-4 ">
                    <x-favourite-courses />
                    @yield('content')
                </div>

            </main>

            <!-- Go to the top of the page button -->
            <div class="fixed bottom-3 right-3">
                <button id="top-page-button" type="button" title="{{__("Go to the top of the page")}}" class="hidden shadow bg-purple-400 hover:brightness-90 focus:outline-none font-medium rounded-full text-sm p-1.5 text-center items-center dark:bg-purple-400 dark:hover:bg-purple-400">
                    <svg class="w-6 h-6 text-grey-800 dark:text-white" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="none" viewBox="0 0 24 24">
                        <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6v13m0-13 4 4m-4-4-4 4"/>
                    </svg>
                    <span class="sr-only">Up arrow</span>
                </button>
            </div>
            <footer class="static bottom-0 w-full">
                @include('layouts.footer')
            </footer>
        </div>

        <!-- Documents Modals -->
        <x-modals.document-add/>
        <x-modals.document-report-store/>

        @auth
            @if(auth()->user()->isAdmin())
            <!-- Crawling Modal -->
            <x-modals.crawling/>
            @endif
        @endauth
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </body>
    <script>
        // handle the scroll to top button
        const topPageButton = document.getElementById('top-page-button');
        topPageButton.addEventListener('click', () => {
            window.scrollTo({ top: 0, behavior: "smooth" });
        });
        const scrollFunction = () => {
            if (
                document.body.scrollTop > 20 ||
                document.documentElement.scrollTop > 20
            ) {
                topPageButton.classList.remove("hidden");
            } else {
                topPageButton.classList.add("hidden");
            }
        };
        window.addEventListener("scroll", scrollFunction);


        // handle the theme
        @if(session()->has('theme-dark'))
            localStorage.setItem('color-theme', '{{ session('theme-dark') ? 'dark' : 'light' }}');
        @endif

        function saveThemePreferenceDark(darkTheme) {
            const csrfToken = document.head.querySelector("[name~=csrf-token][content]").content;
            console.log('Saving theme preference in database ...');
            fetch('{{route('update-dark-theme-preference')}}', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    "X-CSRF-Token": csrfToken
                },
                body: JSON.stringify({
                    darkTheme: darkTheme,
                })
            })
                .then(response => {
                    if (response.ok) {
                        console.log('Theme preference saved in database');
                    } else {
                        console.error('Failed to save theme preference');
                    }
                })
                .catch(error => {
                    console.error('Failed to save theme preference');
                })
        }


        // On page load or when changing themes, best to add inline in `head` to avoid FOUC
        if (localStorage.getItem('color-theme') === 'dark' || (!('color-theme' in localStorage) && window.matchMedia('(prefers-color-scheme: dark)').matches)) {
            document.documentElement.classList.add('dark');
        } else {
            document.documentElement.classList.remove('dark')
        }


        let themeToggleDarkIcon = document.getElementById('theme-toggle-dark-icon');
        let themeToggleLightIcon = document.getElementById('theme-toggle-light-icon');

        // Change the icons inside the button based on previous settings
        if (localStorage.getItem('color-theme') === 'dark' || (!('color-theme' in localStorage) && window.matchMedia('(prefers-color-scheme: dark)').matches)) {
            themeToggleLightIcon.classList.remove('hidden');
        } else {
            themeToggleDarkIcon.classList.remove('hidden');
        }

        let themeToggleBtn = document.getElementById('theme-toggle');

        themeToggleBtn.addEventListener('click', function() {

            // toggle icons inside button
            themeToggleDarkIcon.classList.toggle('hidden');
            themeToggleLightIcon.classList.toggle('hidden');

            // if set via local storage previously
            if (localStorage.getItem('color-theme')) {
                if (localStorage.getItem('color-theme') === 'light') {
                    // save preference in database
                    saveThemePreferenceDark(true);

                    document.documentElement.classList.add('dark');
                    localStorage.setItem('color-theme', 'dark');
                } else {
                    // save preference in database
                    saveThemePreferenceDark(false);

                    document.documentElement.classList.remove('dark');
                    localStorage.setItem('color-theme', 'light');
                }

                // if NOT set via local storage previously
            } else {
                if (document.documentElement.classList.contains('dark')) {
                    // save preference in database
                    saveThemePreferenceDark(false);

                    document.documentElement.classList.remove('dark');
                    localStorage.setItem('color-theme', 'light');
                } else {
                    // save preference in database
                    saveThemePreferenceDark(true);

                    document.documentElement.classList.add('dark');
                    localStorage.setItem('color-theme', 'dark');
                }
            }

        });

        // handle edit document modal
        function setEditDocumentProperties(documentId, course, period, activity, type, format, title, path, license, anonymous) {
            const modalEdit = document.getElementById('modal-edit-document');

            //find the children inputs of modalEdit and set their value.
            modalEdit.querySelector('#course-edit').value = course;
            modalEdit.querySelector('#select2-course-edit-container').innerText = course;
            modalEdit.querySelector('#period-edit').value = period;
            modalEdit.querySelector('#select2-period-edit-container').innerText = period.split(',')[1];
            modalEdit.querySelector('#activity-edit').value = activity;
            modalEdit.querySelector('#type-edit').value = type;
            modalEdit.querySelector('#format-edit').value = format;
            modalEdit.querySelector('#title-edit').value = title;
            modalEdit.querySelector('#anonymous-edit').checked =  Boolean(Number(anonymous));

            document.getElementById('id-edit').value = documentId;

            @php
                use App\Enums\Format as FormatEnum;
            @endphp

            if(format === '{{ FormatEnum::LINK->value }}') {
                //hide file input
                document.getElementById('file-edit-div').classList.add('hidden');
                //show link input
                document.getElementById('link-edit').classList.remove('hidden');
                document.getElementById('link-edit').value = path;
                //hide license input
                modalEdit.querySelector('#license-edit').classList.add('hidden');
            }
            else {
                //show license input
                modalEdit.querySelector('#license-edit').classList.remove('hidden');
                modalEdit.querySelector('#license-edit').value = license;
                //hide link input
                document.getElementById('link-edit').classList.add('hidden');
                //show file input
                document.getElementById('file-edit-div').classList.remove('hidden');
            }
            modalEdit.querySelector('#error-file-extension-edit').classList.add('hidden');
        }

        function setDeleteDocumentId(id) {
            document.getElementById('delete-document-id-input').value = id;
        }

        function checkLength(textAreaId, maxLength, countDisplayId) {
            const length = document.getElementById(textAreaId).value.length;
            document.getElementById(countDisplayId).innerText = length + '/' + maxLength;
        }

        function setReportDocumentStoreModalProperties(documentId, courseCode, path, format) {
            document.getElementById('add-doc-report-doc-id').value = documentId;
            document.getElementById('report-document-link').innerText = path;
            if(format === '{{ FormatEnum::FILE->value }}')
                document.getElementById('report-document-link').href = "/courses/"+courseCode+"/"+path;
            else
                document.getElementById('report-document-link').href = path;
        }
    </script>
</html>
