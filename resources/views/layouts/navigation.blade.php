@php
    use App\Models\Speciality;
@endphp
<nav class="bg-purple-400 text-grey-950">
    <div class="max-w-screen-xl flex flex-wrap items-center justify-between mx-auto p-4">
        <!-- Logo & title -->
        <a href="{{ url('/') }}" class="flex items-center space-x-3 rtl:space-x-reverse">
            <x-application-logos class="block h-8 w-auto fill-current text-grey-800 drop-shadow-2xl"/>
            <span
                class="font-annie-use-your-telescope text-3xl text-grey-950 font-bold whitespace-nowrap ">Shwet</span>
        </a>

        <!-- Navbar items -->
        <div class="flex items-center lg:order-2 space-x-3 lg:space-x-0 rtl:space-x-reverse">

            <!-- User menu -->
            <button type="button"
                    class="relative inline-flex items-center justify-center w-8 h-8 overflow-hidden bg-grey-50 rounded-full dark:bg-grey-800"
                    id="user-menu-button" aria-expanded="false" data-dropdown-toggle="user-dropdown"
                    data-dropdown-placement="bottom">
                <span class="font-medium text-grey-950 dark:text-grey-50">
                    @auth
                        {{strtoupper(substr(auth()->user()->firstname, 0, 1))}}{{strtoupper(substr(auth()->user()->lastname, 0, 1))}}
                    @else
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                             class="bi bi-person" viewBox="0 0 16 16">
                            <path
                                d="M8 8a3 3 0 1 0 0-6 3 3 0 0 0 0 6m2-3a2 2 0 1 1-4 0 2 2 0 0 1 4 0m4 8c0 1-1 1-1 1H3s-1 0-1-1 1-4 6-4 6 3 6 4m-1-.004c-.001-.246-.154-.986-.832-1.664C11.516 10.68 10.289 10 8 10s-3.516.68-4.168 1.332c-.678.678-.83 1.418-.832 1.664z"/>
                        </svg>
                    @endauth
                </span>
            </button>
            <!-- Dropdown menu -->
            <div
                class="z-40 hidden my-4 text-base list-none bg-white divide-y divide-grey-100 rounded-lg shadow dark:bg-grey-700 dark:divide-grey-600"
                id="user-dropdown">
                @auth
                    <div class="px-4 py-2">
                        <span
                            class="block text-sm text-grey-900 dark:text-white">{{auth()->user()->getFullName()}}</span>
                        <span
                            class="block text-sm  text-grey-500 truncate dark:text-grey-400">{{auth()->user()->email}}</span>
                        @if(auth()->user()->isAdmin() || auth()->user()->isModerator())
                            <span
                                class="block text-sm text-purple-400 dark:text-purple-400"><i>{{ __(auth()->user()->role->label()) }}</i></span>
                        @endif
                    </div>
                @endauth
                <ul class="py-1" aria-labelledby="user-menu-buttons-my-documents">
                    <x-nav-user-dropdown.item-link :id="'my-documents-link'" :href="route('my-documents')">
                        <x-slot:icon>
                            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" class="bi bi-file-earmark-text" viewBox="0 0 16 16">
                                <path d="M5.5 7a.5.5 0 0 0 0 1h5a.5.5 0 0 0 0-1zM5 9.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5m0 2a.5.5 0 0 1 .5-.5h2a.5.5 0 0 1 0 1h-2a.5.5 0 0 1-.5-.5"/>
                                <path d="M9.5 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V4.5zm0 1v2A1.5 1.5 0 0 0 11 4.5h2V14a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1z"/>
                            </svg>
                        </x-slot:icon>
                        <x-slot:title>
                            {{__("My documents")}}
                        </x-slot:title>
                    </x-nav-user-dropdown.item-link>
                    <x-nav-user-dropdown.item-link :id="'my-document-reports-link'" :href="route('document-reports.index')">
                        <x-slot:icon>
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-diamond" viewBox="0 0 16 16">
                            <path d="M6.95.435c.58-.58 1.52-.58 2.1 0l6.515 6.516c.58.58.58 1.519 0 2.098L9.05 15.565c-.58.58-1.519.58-2.098 0L.435 9.05a1.48 1.48 0 0 1 0-2.098zm1.4.7a.495.495 0 0 0-.7 0L1.134 7.65a.495.495 0 0 0 0 .7l6.516 6.516a.495.495 0 0 0 .7 0l6.516-6.516a.495.495 0 0 0 0-.7L8.35 1.134z"/>
                            <path d="M7.002 11a1 1 0 1 1 2 0 1 1 0 0 1-2 0M7.1 4.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0z"/>
                        </svg>
                        </x-slot:icon>
                        <x-slot:title>
                            {{__("My reports")}}
                        </x-slot:title>
                    </x-nav-user-dropdown.item-link>
                </ul>
                <ul class="py-1" aria-labelledby="user-menu-buttons-settings">
                    <x-nav-user-dropdown.item>
                        <svg width="20" height="20" viewBox="0 0 36 36" xmlns="http://www.w3.org/2000/svg"
                             xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" role="img"
                             class="me-3" preserveAspectRatio="xMidYMid meet">
                            <path fill="#ED2939" d="M36 27a4 4 0 0 1-4 4h-8V5h8a4 4 0 0 1 4 4v18z"></path>
                            <path fill="#002495" d="M4 5a4 4 0 0 0-4 4v18a4 4 0 0 0 4 4h8V5H4z"></path>
                            <path fill="#EEE" d="M12 5h12v26H12z"></path>
                        </svg>

                        <label class="inline-flex items-center cursor-pointer">
                            <input id="switch-language-toggle" type="checkbox" value=""
                                   class="sr-only peer" {{ Config::get('app.locale') === "en" ? 'checked="checked"' : ''}}>
                            <div
                                class="relative w-9 h-5 bg-purple-400 peer-focus:outline-none peer-focus:ring-purple-400 peer-focus:ring-1 rounded-full peer dark:bg-purple-400 peer-checked:after:translate-x-full rtl:peer-checked:after:-translate-x-full peer-checked:after:border-grey-50 after:content-[''] after:absolute after:top-[2px] after:start-[2px] after:bg-grey-50 after:rounded-full after:h-4 after:w-4 after:transition-all dark:border-grey-600"></div>
                        </label>

                        <svg width="20" height="20" viewBox="0 -4 28 28" fill="none"
                             xmlns="http://www.w3.org/2000/svg" class="ms-3">
                            <g clip-path="url(#clip0_503_2952)">
                                <rect width="28" height="20" rx="2" fill="white"/>
                                <mask id="mask0_503_2952" style="mask-type:alpha" maskUnits="userSpaceOnUse"
                                      x="0" y="0" width="28" height="20">
                                    <rect width="28" height="20" rx="2" fill="white"/>
                                </mask>
                                <g mask="url(#mask0_503_2952)">
                                    <rect width="28" height="20" fill="#0A17A7"/>
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                          d="M-1.28244 -1.91644L10.6667 6.14335V-1.33333H17.3334V6.14335L29.2825 -1.91644L30.7737 0.294324L21.3263 6.66667H28V13.3333H21.3263L30.7737 19.7057L29.2825 21.9165L17.3334 13.8567V21.3333H10.6667V13.8567L-1.28244 21.9165L-2.77362 19.7057L6.67377 13.3333H2.95639e-05V6.66667H6.67377L-2.77362 0.294324L-1.28244 -1.91644Z"
                                          fill="white"/>
                                    <path d="M18.668 6.33219L31.3333 -2" stroke="#DB1F35"
                                          stroke-width="0.666667" stroke-linecap="round"/>
                                    <path d="M20.0128 13.6975L31.3666 21.3503" stroke="#DB1F35"
                                          stroke-width="0.666667" stroke-linecap="round"/>
                                    <path d="M8.00555 6.31046L-3.83746 -1.67099" stroke="#DB1F35"
                                          stroke-width="0.666667" stroke-linecap="round"/>
                                    <path d="M9.29006 13.6049L-3.83746 22.3105" stroke="#DB1F35"
                                          stroke-width="0.666667" stroke-linecap="round"/>
                                    <path fill-rule="evenodd" clip-rule="evenodd"
                                          d="M0 12H12V20H16V12H28V8H16V0H12V8H0V12Z" fill="#E6273E"/>
                                </g>
                            </g>
                            <defs>
                                <clipPath id="clip0_503_2952">
                                    <rect width="28" height="20" rx="2" fill="white"/>
                                </clipPath>
                            </defs>
                        </svg>
                    </x-nav-user-dropdown.item>
                    {{-- <x-nav-user-dropdown.item-link :href="url('')" :id="'notifications-settings-link'">
                        <x-slot:icon>
                            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor"
                                 class="bi bi-bell" viewBox="0 0 16 16">
                                <path
                                        d="M8 16a2 2 0 0 0 2-2H6a2 2 0 0 0 2 2M8 1.918l-.797.161A4 4 0 0 0 4 6c0 .628-.134 2.197-.459 3.742-.16.767-.376 1.566-.663 2.258h10.244c-.287-.692-.502-1.49-.663-2.258C12.134 8.197 12 6.628 12 6a4 4 0 0 0-3.203-3.92zM14.22 12c.223.447.481.801.78 1H1c.299-.199.557-.553.78-1C2.68 10.2 3 6.88 3 6c0-2.42 1.72-4.44 4.005-4.901a1 1 0 1 1 1.99 0A5 5 0 0 1 13 6c0 .88.32 4.2 1.22 6"/>
                            </svg>
                        </x-slot:icon>
                        <x-slot:title>
                            {{__("Notifications")}}
                        </x-slot:title>
                    </x-nav-user-dropdown.item-link> --}}
                    <x-nav-user-dropdown.item-link :id="'theme-toggle'">
                        <x-slot:icon>
                            <svg id="theme-toggle-dark-icon" xmlns="http://www.w3.org/2000/svg" width="18" height="18"
                                 fill="currentColor" class="hidden bi bi-moon" viewBox="0 0 16 16">
                                <path
                                    d="M6 .278a.77.77 0 0 1 .08.858 7.2 7.2 0 0 0-.878 3.46c0 4.021 3.278 7.277 7.318 7.277q.792-.001 1.533-.16a.79.79 0 0 1 .81.316.73.73 0 0 1-.031.893A8.35 8.35 0 0 1 8.344 16C3.734 16 0 12.286 0 7.71 0 4.266 2.114 1.312 5.124.06A.75.75 0 0 1 6 .278M4.858 1.311A7.27 7.27 0 0 0 1.025 7.71c0 4.02 3.279 7.276 7.319 7.276a7.32 7.32 0 0 0 5.205-2.162q-.506.063-1.029.063c-4.61 0-8.343-3.714-8.343-8.29 0-1.167.242-2.278.681-3.286"/>
                            </svg>
                            <svg id="theme-toggle-light-icon" xmlns="http://www.w3.org/2000/svg" width="18" height="18"
                                 fill="currentColor" class="hidden bi bi-sun" viewBox="0 0 16 16">
                                <path
                                    d="M8 11a3 3 0 1 1 0-6 3 3 0 0 1 0 6m0 1a4 4 0 1 0 0-8 4 4 0 0 0 0 8M8 0a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-1 0v-2A.5.5 0 0 1 8 0m0 13a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-1 0v-2A.5.5 0 0 1 8 13m8-5a.5.5 0 0 1-.5.5h-2a.5.5 0 0 1 0-1h2a.5.5 0 0 1 .5.5M3 8a.5.5 0 0 1-.5.5h-2a.5.5 0 0 1 0-1h2A.5.5 0 0 1 3 8m10.657-5.657a.5.5 0 0 1 0 .707l-1.414 1.415a.5.5 0 1 1-.707-.708l1.414-1.414a.5.5 0 0 1 .707 0m-9.193 9.193a.5.5 0 0 1 0 .707L3.05 13.657a.5.5 0 0 1-.707-.707l1.414-1.414a.5.5 0 0 1 .707 0m9.193 2.121a.5.5 0 0 1-.707 0l-1.414-1.414a.5.5 0 0 1 .707-.707l1.414 1.414a.5.5 0 0 1 0 .707M4.464 4.465a.5.5 0 0 1-.707 0L2.343 3.05a.5.5 0 1 1 .707-.707l1.414 1.414a.5.5 0 0 1 0 .708"/>
                            </svg>
                        </x-slot:icon>
                        <x-slot:title>
                            {{__("Switch theme")}}
                        </x-slot:title>
                    </x-nav-user-dropdown.item-link>
                </ul>

                <!-- User account settings -->
                <ul class="py-1" aria-labelledby="user-menu-buttons-account">
                    <!-- if user is authenticated -->
                    @auth
                        <!-- My account -->
                        @if(session('user.provider') === 'cas')
                            <x-nav-user-dropdown.item-link :href="url('https://comptes.utc.fr/accounts-web/tools/index.xhtml')" :id="'my-account-nav-link'" :target="'_blank'">
                                <x-slot:icon>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor"
                                        class="bi bi-box-arrow-up-right" viewBox="0 0 16 16">
                                        <path fill-rule="evenodd"
                                            d="M8.636 3.5a.5.5 0 0 0-.5-.5H1.5A1.5 1.5 0 0 0 0 4.5v10A1.5 1.5 0 0 0 1.5 16h10a1.5 1.5 0 0 0 1.5-1.5V7.864a.5.5 0 0 0-1 0V14.5a.5.5 0 0 1-.5.5h-10a.5.5 0 0 1-.5-.5v-10a.5.5 0 0 1 .5-.5h6.636a.5.5 0 0 0 .5-.5"/>
                                        <path fill-rule="evenodd"
                                            d="M16 .5a.5.5 0 0 0-.5-.5h-5a.5.5 0 0 0 0 1h3.793L6.146 9.146a.5.5 0 1 0 .708.708L15 1.707V5.5a.5.5 0 0 0 1 0z"/>
                                    </svg>
                                </x-slot:icon>
                                <x-slot:title>
                                    {{__("My account")}}
                                </x-slot:title>
                            </x-nav-user-dropdown.item-link>
                        @endif

                        <!-- Logging out -->
                        <form method="POST" action="{{ route('auth.logout') }}" id="log-out-form">
                            @csrf
                            <x-nav-user-dropdown.item-link :id="'logout-nav-link'">
                                <x-slot:icon>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor"
                                         class="bi bi-power" viewBox="0 0 16 16">
                                        <path d="M7.5 1v7h1V1z"/>
                                        <path
                                            d="M3 8.812a5 5 0 0 1 2.578-4.375l-.485-.874A6 6 0 1 0 11 3.616l-.501.865A5 5 0 1 1 3 8.812"/>
                                    </svg>
                                </x-slot:icon>
                                <x-slot:title>
                                    {{__("Log out")}}
                                </x-slot:title>
                            </x-nav-user-dropdown.item-link>
                        </form>
                        <script>
                            // submit logout form
                            document.getElementById('logout-nav-link').addEventListener('click', function (event) {
                                event.preventDefault();
                                document.getElementById('log-out-form').submit();
                            });
                        </script>
                    @else
                        <x-nav-user-dropdown.item-link :href="route('auth.login')" :id="'login-nav-link'">
                            <x-slot:icon>
                                <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor"
                                     class="bi bi-box-arrow-in-right" viewBox="0 0 16 16">
                                    <path fill-rule="evenodd"
                                          d="M6 3.5a.5.5 0 0 1 .5-.5h8a.5.5 0 0 1 .5.5v9a.5.5 0 0 1-.5.5h-8a.5.5 0 0 1-.5-.5v-2a.5.5 0 0 0-1 0v2A1.5 1.5 0 0 0 6.5 14h8a1.5 1.5 0 0 0 1.5-1.5v-9A1.5 1.5 0 0 0 14.5 2h-8A1.5 1.5 0 0 0 5 3.5v2a.5.5 0 0 0 1 0z"/>
                                    <path fill-rule="evenodd"
                                          d="M11.854 8.354a.5.5 0 0 0 0-.708l-3-3a.5.5 0 1 0-.708.708L10.293 7.5H1.5a.5.5 0 0 0 0 1h8.793l-2.147 2.146a.5.5 0 0 0 .708.708z"/>
                                </svg>
                            </x-slot:icon>
                            <x-slot:title>
                                {{__("Log in")}}
                            </x-slot:title>
                        </x-nav-user-dropdown.item-link>
                    @endauth
                </ul>
            </div>

            <!-- Mobile menu toggle -->
            <button data-collapse-toggle="navbar-user" type="button"
                    class="inline-flex items-center p-2 w-8 h-8 justify-center text-grey-800 hover:text-grey-800 hover:bg-grey-50 text-sm dark:text-grey-800 rounded-lg lg:hidden dark:hover:bg-grey-800 dark:hover:text-grey-50 focus:outline-none focus:ring-2 focus:ring-grey-200 dark:focus:ring-grey-700"
                    aria-controls="navbar-user" aria-expanded="false">
                <span class="sr-only">Open main menu</span>
                <svg class="w-5 h-5" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none"
                     viewBox="0 0 17 14">
                    <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                          d="M1 1h15M1 7h15M1 13h15"/>
                </svg>
            </button>
        </div>

        <!-- Navbar links & actions -->
        <div class="items-center justify-between hidden w-full lg:flex lg:w-auto lg:order-1" id="navbar-user">
            <ul class="flex flex-col font-medium bg-grey-100 dark:bg-grey-600 lg:bg-purple-400 dark:lg:bg-purple-400 p-4 lg:p-0 mt-4 lg:mt-0 border border-grey-100 rounded-lg lg:space-x-4 xl:space-x-8 rtl:space-x-reverse lg:flex-row lg:border-0 dark:border-grey-700 lg:items-center items-start">
                <!-- Courses searchbar -->
                <li>
                    <div class="lg:max-w-40 flex items-center">
                        <label for="search-courses"
                               class="mb-2 text-sm font-medium text-grey-900 sr-only dark:text-white">
                            {{__("Search a UV ...")}}
                        </label>
                        <div
                                class="flex justify-between items-center bg-grey-50 rounded-lg dark:bg-grey-800 border-solid border-2 border-purple-400 lg:border-none dark:border-none">
                            <input type="search" id="search-courses" data-dropdown-toggle="courses-list-autocomplete"
                                   class="w-full text-grey-900 bg-grey-50 border-none text-sm rounded-md focus:outline-none focus:ring-0 dark:bg-grey-800 dark:placeholder-grey-200 dark:text-grey-50 dark:focus:ring-purple-400"
                                   placeholder="{{__("SY02 ...")}}" required autocomplete="off"/>
                            <button onclick="searchCourses()"
                                    class="m-1.5 bg-purple-400 focus:ring-2 focus:outline-none focus:ring-purple-300 font-medium rounded-md text-sm p-1.5 dark:bg-purple-400 hover:scale-105 dark:focus:ring-purple-300">
                                <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="3"
                                     stroke="currentColor" class="w-4 h-4">
                                    <path stroke-linecap="round" stroke-linejoin="round"
                                          d="m21 21-5.197-5.197m0 0A7.5 7.5 0 1 0 5.196 5.196a7.5 7.5 0 0 0 10.607 10.607Z"/>
                                </svg>
                            </button>
                        </div>
                    </div>
                    <div id="courses-list-autocomplete" class="z-40 hidden lg:max-w-40 my-4 text-base list-none bg-grey-50 rounded-lg shadow dark:bg-grey-800 max-h-56 overflow-y-auto">
                        <ul class="py-1 font-medium" role="none" id="courses-list-group">
                        </ul>
                    </div>
                </li>

                <!-- Courses types dropdown -->
                <li class="w-full lg:w-auto">
                    <x-nav-dropdown id="nav-UVs-list" :dataDropdownToggle="'uv-types-dropdown'">
                        <x-slot:icon>
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor"
                                 class="bi bi-book" viewBox="0 0 16 16">
                                <path
                                    d="M1 2.828c.885-.37 2.154-.769 3.388-.893 1.33-.134 2.458.063 3.112.752v9.746c-.935-.53-2.12-.603-3.213-.493-1.18.12-2.37.461-3.287.811zm7.5-.141c.654-.689 1.782-.886 3.112-.752 1.234.124 2.503.523 3.388.893v9.923c-.918-.35-2.107-.692-3.287-.81-1.094-.111-2.278-.039-3.213.492zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783"/>
                            </svg>
                        </x-slot:icon>
                        <x-slot:title>{{ Str::plural(__("Speciality")) }}</x-slot:title>
                        <x-slot:itemGroups>
                            @php
                                $specialities = Speciality::all();
                                $specialityGroups = [
                                    ['TC'],
                                    ['IM', 'GI', 'GB', 'GU', 'GP'],
                                    ['HuTech', 'ID-M'],
                                    ['IdS', 'HIC', 'ISC', 'CH', 'EMSSE'],
                                    ['MTSP'],
                                    ['Major A', 'Major B', 'Major C'],
                                    ['Autres']
                                ];
                                $allGroupCodes = Arr::flatten($specialityGroups);
                                $otherSpecialities = $specialities->whereNotIn('code', $allGroupCodes);
                            @endphp
                            @foreach($specialityGroups as $group)
                                <x-nav-dropdown.items-group>
                                    @foreach($specialities->whereIn('code', $group)->sortBy(function($speciality) use ($group) {
                                        return array_search($speciality->code, $group);
                                    }) as $speciality)
                                        <x-nav-dropdown.item :href="url('/courses?speciality_codes[]=' . $speciality->code)"
                                                             :active="request()->routeIs('/courses?speciality_codes[]=' . $speciality->code)">
                                            {{ $speciality->code }}
                                        </x-nav-dropdown.item>
                                    @endforeach
                                </x-nav-dropdown.items-group>
                            @endforeach
                            @if($otherSpecialities->count() > 0)
                                <x-nav-dropdown.items-group>
                                    @foreach($otherSpecialities as $speciality)
                                        <x-nav-dropdown.item :href="url('/courses?speciality_codes[]=' . $speciality->code)"
                                                             :active="request()->routeIs('/courses?speciality_codes[]=' . $speciality->code)">
                                            {{ $speciality->code }}
                                        </x-nav-dropdown.item>
                                    @endforeach
                                </x-nav-dropdown.items-group>
                            @endif
                            <x-nav-dropdown.items-group>
                                <x-nav-dropdown.item :href="url('/courses')" :active="request()->is('courses')">
                                    {{ __("All UVs") }}
                                </x-nav-dropdown.item>
                            </x-nav-dropdown.items-group>
                        </x-slot:itemGroups>
                    </x-nav-dropdown>
                </li>

                <!-- Publish document button -->
                <li class="w-full lg:w-auto">
                    <x-nav-link data-modal-target="modal-document" data-modal-show="modal-document"
                                class="cursor-pointer">
                        <x-slot:icon>
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor"
                                 class="bi bi-file-earmark-arrow-up" viewBox="0 0 16 16">
                                <path
                                    d="M8.5 11.5a.5.5 0 0 1-1 0V7.707L6.354 8.854a.5.5 0 1 1-.708-.708l2-2a.5.5 0 0 1 .708 0l2 2a.5.5 0 0 1-.708.708L8.5 7.707z"/>
                                <path
                                    d="M14 14V4.5L9.5 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2M9.5 3A1.5 1.5 0 0 0 11 4.5h2V14a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h5.5z"/>
                            </svg>
                        </x-slot:icon>
                        <x-slot:title>
                            {{ __('Publish a document') }}
                        </x-slot:title>
                    </x-nav-link>
                </li>

                {{--<!-- Statistics -->
                <li class="w-full lg:w-auto">
                    <x-nav-link :href="url('/stats')" :active="request()->is('stats')">
                        <x-slot:icon>
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor"
                                 class="bi bi-bar-chart-line" viewBox="0 0 16 16">
                                <path
                                    d="M11 2a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v12h.5a.5.5 0 0 1 0 1H.5a.5.5 0 0 1 0-1H1v-3a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v3h1V7a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1v7h1zm1 12h2V2h-2zm-3 0V7H7v7zm-5 0v-3H2v3z"/>
                            </svg>
                        </x-slot:icon>
                        <x-slot:title>
                            {{ __('Statistics') }}
                        </x-slot:title>
                    </x-nav-link>
                </li>--}}

                <!-- Administration Dropdown -->
                @auth
                    @if(auth()->user()->isAdmin())
                        <li class="w-full lg:w-auto">
                            <x-nav-dropdown id="nav-admin-menu" :dataDropdownToggle="'admin-dropdown-menu-items'">
                                <x-slot:icon>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor"
                                         class="bi bi-person-gear me-1" viewBox="0 0 16 16">
                                        <path
                                            d="M11 5a3 3 0 1 1-6 0 3 3 0 0 1 6 0M8 7a2 2 0 1 0 0-4 2 2 0 0 0 0 4m.256 7a4.5 4.5 0 0 1-.229-1.004H3c.001-.246.154-.986.832-1.664C4.484 10.68 5.711 10 8 10q.39 0 .74.025c.226-.341.496-.65.804-.918Q8.844 9.002 8 9c-5 0-6 3-6 4s1 1 1 1zm3.63-4.54c.18-.613 1.048-.613 1.229 0l.043.148a.64.64 0 0 0 .921.382l.136-.074c.561-.306 1.175.308.87.869l-.075.136a.64.64 0 0 0 .382.92l.149.045c.612.18.612 1.048 0 1.229l-.15.043a.64.64 0 0 0-.38.921l.074.136c.305.561-.309 1.175-.87.87l-.136-.075a.64.64 0 0 0-.92.382l-.045.149c-.18.612-1.048.612-1.229 0l-.043-.15a.64.64 0 0 0-.921-.38l-.136.074c-.561.305-1.175-.309-.87-.87l.075-.136a.64.64 0 0 0-.382-.92l-.148-.045c-.613-.18-.613-1.048 0-1.229l.148-.043a.64.64 0 0 0 .382-.921l-.074-.136c-.306-.561.308-1.175.869-.87l.136.075a.64.64 0 0 0 .92-.382zM14 12.5a1.5 1.5 0 1 0-3 0 1.5 1.5 0 0 0 3 0"/>
                                    </svg>
                                </x-slot:icon>
                                <x-slot:title>{{__("Admin")}}</x-slot:title>
                                <x-slot:itemGroups>
                                    <x-nav-dropdown.items-group>
                                        <x-nav-dropdown.item id="button-modal-crawling-uvs" data-modal-target="popup-modal-crawling"
                                                             data-modal-toggle="popup-modal-crawling">
                                            {{ __("Crawl UVs") }}
                                        </x-nav-dropdown.item>
                                        <x-nav-dropdown.item id="button-modal-crawling-diplomas" data-modal-target="popup-modal-crawling"
                                                             data-modal-toggle="popup-modal-crawling">
                                            {{ __("Crawl Diplomas") }}
                                        </x-nav-dropdown.item>
                                        <x-nav-dropdown.item id="nav-button-document-reports" :href="route('admin.document-reports.index')" :active="request()->is('document-reports.index')">
                                            {{ __("Document reports") }}
                                        </x-nav-dropdown.item>
                                    </x-nav-dropdown.items-group>
                                </x-slot:itemGroups>
                            </x-nav-dropdown>
                        </li>
                        <script>
                            document.getElementById('button-modal-crawling-uvs').addEventListener('click', function () {
                                fetch('/admin/crawl-uvs')
                                    .then(() => {
                                        window.location.reload();
                                    });
                            });
                            document.getElementById('button-modal-crawling-diplomas').addEventListener('click', function () {
                                fetch('/admin/crawl-diplomes')
                                    .then(() => {
                                        window.location.reload();
                                    });
                            });
                        </script>
                    @endif
                @endauth
            </ul>
        </div>
    </div>

    <script>
        let checkbox = document.querySelector("input[id=switch-language-toggle]");
        checkbox.addEventListener('change', function () {
            if (this.checked) {
                console.log("Reloading with english language");
                window.location.href = "/language/en";
            } else {
                console.log("Reloading with french language");
                window.location.href = "/language/fr";
            }
        });

        // This function is called when the user performs a search for courses
        function searchCourses() {
            // Get the text entered by the user in the search input field and remove any leading or trailing whitespace
            const searchText = document.getElementById("search-courses").value.trim().toLowerCase();

            // Check if the search text is not empty
            if (searchText !== "") {
                // If the search text is not empty, construct the URL for the search results page
                window.location.href = "/courses/" + encodeURIComponent(searchText);
            }
        }

        let courses = [];
        const searchCourseInput = document.getElementById('search-courses');
        const listItemClasses = "block px-4 py-2 text-sm text-grey-800 hover:bg-grey-100 dark:hover:bg-grey-600 dark:text-grey-200 dark:hover:text-grey-50"

        // on page load, fetch courses list from the api and display them in the autocomplete list
        fetch('/api/courses')
            .then(response => response.text())
            .then(data => {
                courses = JSON.parse(data)

                filterCourses();
            });

        // filter courses list when user types in the search input
        searchCourseInput.addEventListener('input', function() {
            filterCourses();
        });

        // search courses when user presses enter key
        searchCourseInput.addEventListener('keyup', function(event) {
            if (event.key === 'Enter') {
                searchCourses();
            }
        });

        // filter courses list based on the search input value
        function filterCourses() {
            const inputValue = searchCourseInput.value.toLowerCase();
            document.getElementById('courses-list-group').innerHTML = '';
            const filteredValues = courses.filter(value => value.code.toLowerCase().startsWith(inputValue));
            filteredValues.forEach(course => {
                addCourseToList(course);
            });
        }

        // add course to the autocomplete list
        function addCourseToList(course) {
            let listItem = document.createElement('li');
            let linkListItem = document.createElement('a');
            linkListItem.href = '/courses/' + course.code;
            linkListItem.className += listItemClasses;
            linkListItem.textContent = course.code;
            listItem.appendChild(linkListItem);
            document.getElementById('courses-list-group').appendChild(listItem);
        }
    </script>


</nav>
