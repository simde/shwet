@extends('layouts.app')

@section('content')
    <form method="GET" action="{{ route('courses.index') }}" class="flex justify-between">
        <div class="flex gap-3">
            <!-- Dropdown filter for type -->
            <x-dropdown.index :id="'dropdownTypeButton'" :dropdownId="'dropdownType'">
                <x-slot:icon>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-calculator" viewBox="0 0 16 16">
                        <path d="M12 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1zM4 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2z"/>
                        <path d="M4 2.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-.5.5h-7a.5.5 0 0 1-.5-.5zm0 4a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm0 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm0 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm3-6a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm0 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm0 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm3-6a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm0 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5z"/>
                    </svg>
                </x-slot:icon>
                <x-slot:title>
                    {{ __('Type of course') }}
                </x-slot:title>
                <x-slot:dropdownItems>
                    <x-dropdown.item-checkbox :inputId="'type-all'" :name="'types[]'" :value="'all'"
                                              :checked="(empty($filters['types']) || in_array('all', $filters['types']))">
                        <x-slot:label>
                            {{ __('Select all') }}
                        </x-slot:label>
                    </x-dropdown.item-checkbox>
                    @foreach($types as $type)
                        <x-dropdown.item-checkbox :inputId="'type-'.$type->value" :name="'types[]'" :value="$type->value"
                                                  :checked="(!empty($filters['types']) && in_array($type->value, $filters['types']))">
                            <x-slot:label>
                                {{ $type->value }}
                            </x-slot:label>
                        </x-dropdown.item-checkbox>
                    @endforeach
                </x-slot:dropdownItems>
            </x-dropdown.index>

            <!-- Dropdown filter for speciality -->
            <x-dropdown.index :id="'dropdownSpecialityButton'" :dropdownId="'dropdownSpeciality'">
                <x-slot:icon>
                    <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                         stroke="currentColor" class="w-5 h-5">
                        <path stroke-linecap="round" stroke-linejoin="round"
                              d="M9.75 3.104v5.714a2.25 2.25 0 0 1-.659 1.591L5 14.5M9.75 3.104c-.251.023-.501.05-.75.082m.75-.082a24.301 24.301 0 0 1 4.5 0m0 0v5.714c0 .597.237 1.17.659 1.591L19.8 15.3M14.25 3.104c.251.023.501.05.75.082M19.8 15.3l-1.57.393A9.065 9.065 0 0 1 12 15a9.065 9.065 0 0 0-6.23-.693L5 14.5m14.8.8 1.402 1.402c1.232 1.232.65 3.318-1.067 3.611A48.309 48.309 0 0 1 12 21c-2.773 0-5.491-.235-8.135-.687-1.718-.293-2.3-2.379-1.067-3.61L5 14.5"/>
                    </svg>
                </x-slot:icon>
                <x-slot:title>
                    {{ __('Speciality') }}
                </x-slot:title>
                <x-slot:dropdownItems>
                    <x-dropdown.item-checkbox :inputId="'speciality-all'" :name="'speciality_codes[]'" :value="'all'"
                                              :checked="(empty($filters['speciality_codes']) || in_array('all', $filters['speciality_codes']))">
                        <x-slot:label>
                            {{ __('Select all') }}
                        </x-slot:label>
                    </x-dropdown.item-checkbox>
                    @php
                        // TODO : change the order of the specialities automatically
                        $specialityOrder = ['TC', 'IM', 'GI', 'GB', 'GU', 'GP', 'HuTech', 'ID-M', 'IdS', 'HIC', 'ISC', 'EMSSE', 'CH', 'MTSP', 'Major A', 'Major B', 'Major C', 'Autres'];
                    @endphp
                    @foreach($specialities->sortBy(function($speciality) use ($specialityOrder) {
                        return array_search($speciality->code, $specialityOrder);
                    }) as $speciality)
                        <x-dropdown.item-checkbox :inputId="'speciality-'.$speciality->code" :name="'speciality_codes[]'" :value="$speciality->code"
                                                  :checked="!empty($filters['speciality_codes']) && in_array($speciality->code, $filters['speciality_codes'])">
                            <x-slot:label>
                                {{ $speciality->code }}
                            </x-slot:label>
                        </x-dropdown.item-checkbox>
                @endforeach
                </x-slot:dropdownItems>
            </x-dropdown.index>

            <!-- Dropdown filter for season -->
            <x-dropdown.index :id="'dropdownSeasonButton'" :dropdownId="'dropdownSeason'">
                <x-slot:icon>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-calendar4-week flex-none" viewBox="0 0 16 16">
                        <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5M2 2a1 1 0 0 0-1 1v1h14V3a1 1 0 0 0-1-1zm13 3H1v9a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1z"/>
                        <path d="M11 7.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm-3 0a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm-2 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm-3 0a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5z"/>
                    </svg>
                </x-slot:icon>
                <x-slot:title>
                    {{ ucfirst(__('semester')) }}
                </x-slot:title>
                <x-slot:dropdownItems>
                    <x-dropdown.item-checkbox :inputId="'season-all'" :name="'season_codes[]'" :value="'all'"
                    :checked="empty($filters['season_codes']) || in_array('all', $filters['season_codes'])">
                        <x-slot:label>
                            {{ __('Select all') }}
                        </x-slot:label>
                    </x-dropdown.item-checkbox>
                    @php
                        $seasonOrder = ['P', 'A', 'E', 'H'];
                    @endphp
                    @foreach($seasons->sortBy(function($season) use ($seasonOrder) {
                        return array_search($season->code, $seasonOrder);
                    }) as $season)
                        <x-dropdown.item-checkbox :inputId="'season-'.$season->code->value" :name="'season_codes[]'" :value="$season->code->value"
                                                  :checked="!empty($filters['season_codes']) && in_array($season->code->value, $filters['season_codes'])">
                            <x-slot:label>
                                {{ __($season->code->label()) }}
                            </x-slot:label>
                        </x-dropdown.item-checkbox>
                    @endforeach
                </x-slot:dropdownItems>
            </x-dropdown.index>

            <!-- Dropdown filter for status -->
            <x-dropdown.index :id="'dropdownStatusButton'" :dropdownId="'dropdownStatus'">
                <x-slot:icon>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-archive" viewBox="0 0 16 16">
                        <path d="M0 2a1 1 0 0 1 1-1h14a1 1 0 0 1 1 1v2a1 1 0 0 1-1 1v7.5a2.5 2.5 0 0 1-2.5 2.5h-9A2.5 2.5 0 0 1 1 12.5V5a1 1 0 0 1-1-1zm2 3v7.5A1.5 1.5 0 0 0 3.5 14h9a1.5 1.5 0 0 0 1.5-1.5V5zm13-3H1v2h14zM5 7.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5"/>
                    </svg>
                </x-slot:icon>
                <x-slot:title>
                    {{ __('Status') }}
                </x-slot:title>
                <x-slot:dropdownItems>
                    <x-dropdown.item-checkbox :inputId="'status-all'" :name="'statuses[]'" :value="'all'"
                                              :checked="(empty($filters['statuses']) || in_array('all', $filters['statuses']))">
                        <x-slot:label>
                            {{ __('Select all') }}
                        </x-slot:label>
                    </x-dropdown.item-checkbox>
                    <x-dropdown.item-checkbox :inputId="'status-current'" :name="'statuses[]'" :value="'current'"
                                                :checked="(!empty($filters['statuses']) && in_array('current', $filters['statuses']))">
                        <x-slot:label>
                            {{ __('Current') }}
                        </x-slot:label>
                    </x-dropdown.item-checkbox>
                    <x-dropdown.item-checkbox :inputId="'status-archived'" :name="'statuses[]'" :value="'archived'"
                                                :checked="(!empty($filters['statuses']) && in_array('archived', $filters['statuses']))">
                        <x-slot:label>
                            {{ __('Archived') }}
                        </x-slot:label>
                    </x-dropdown.item-checkbox>
                </x-slot:dropdownItems>
            </x-dropdown.index>
        </div>

        <!-- Apply filters button -->
        <x-buttons.icon-text-purple :type="'submit'" :responsive="true" :outline="true">
            <x-slot:icon>
                <svg class="w-5 h-5" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="none" viewBox="0 0 24 24">
                    <path stroke="currentColor" stroke-linecap="round" stroke-width="1.5" d="M18.796 4H5.204a1 1 0 0 0-.753 1.659l5.302 6.058a1 1 0 0 1 .247.659v4.874a.5.5 0 0 0 .2.4l3 2.25a.5.5 0 0 0 .8-.4v-7.124a1 1 0 0 1 .247-.659l5.302-6.059c.566-.646.106-1.658-.753-1.658Z"/>
                </svg>
            </x-slot:icon>

            <x-slot:text>
                {{ __('Apply filters') }}
            </x-slot:text>
        </x-buttons.icon-text-purple>
    </form>

    <script>
        // Get the 'type-all' checkbox element
        const allTypes = document.getElementById('type-all');
        // Get all other type checkboxes except 'type-all'
        const otherTypes = Array.from(document.querySelectorAll('input[id^="type-"]')).filter(function (checkbox) {
            return checkbox.id !== 'type-all';
        });

        // Get the 'speciality-all' checkbox element
        const allSpecialities = document.getElementById('speciality-all');
        // Get all other speciality checkboxes except 'speciality-all'
        const otherSpecialities = Array.from(document.querySelectorAll('input[id^="speciality-"]')).filter(function (checkbox) {
            return checkbox.id !== 'speciality-all';
        });

        // Get the 'season-all' checkbox element
        const allSeasons = document.getElementById('season-all');
        // Get all other season checkboxes except 'season-all'
        const otherSeasons = Array.from(document.querySelectorAll('input[id^="season-"]')).filter(function (checkbox) {
            return checkbox.id !== 'season-all';
        });

        // Get the 'status-all' checkbox element
        const allStatuses = document.getElementById('status-all');
        // Get all other status checkboxes except 'status-all'
        const otherStatuses = Array.from(document.querySelectorAll('input[id^="status-"]')).filter(function (checkbox) {
            return checkbox.id !== 'status-all';
        });

        // Combine all 'all' checkboxes into an array
        const allCheckboxes = [allTypes, allSpecialities, allSeasons, allStatuses];
        // Combine all other checkboxes into an array
        const otherCheckboxes = otherTypes.concat(otherSpecialities, otherSeasons, otherStatuses);

        // Add event listener to each 'other' checkbox
        otherCheckboxes.forEach(function (checkbox) {
            checkbox.addEventListener('change', function () {
                // If a checkbox is checked, uncheck all 'all' checkboxes
                if (this.checked) {
                    allCheckboxes.forEach(function (allCheckbox) {
                        allCheckbox.checked = false;
                    });
                }
            });
        });

        // Add event listener to each 'all' checkbox
        allCheckboxes.forEach(function (allCheckbox) {
            allCheckbox.addEventListener('change', function () {
                // If a 'all' checkbox is checked, uncheck all 'other' checkboxes
                if (this.checked) {
                    otherCheckboxes.forEach(function (checkbox) {
                        checkbox.checked = false;
                    });
                }
            });
        });
    </script>

    @if (count($courses) > 0)
        <div class="flex flex-wrap justify-around items-center gap-3 rounded-md md:hidden">
            @foreach($courses->sortBy('code') as $course)
                <a href="{{ route('courses.show', $course->code) }}" title="{{ $course->title }}"
                   class="bg-grey-200 dark:bg-grey-800 text-grey-950 dark:text-grey-50 p-3 rounded-md w-full max-w-44 h-44 hover:scale-105 flex flex-col">
                    <span class="text-lg text-purple-400"><b>{{ $course->code }}</b></span>
                    <span class="truncate text-sm">{{ $course->title }}</span>
                    <div class="flex items-center gap-2">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-calculator" viewBox="0 0 16 16">
                            <path d="M12 1a1 1 0 0 1 1 1v12a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1zM4 0a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V2a2 2 0 0 0-2-2z"/>
                            <path d="M4 2.5a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-.5.5h-7a.5.5 0 0 1-.5-.5zm0 4a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm0 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm0 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm3-6a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm0 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm0 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm3-6a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm0 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5z"/>
                        </svg>
                        @if ($course->type == null)
                            <span>{{ __('Unknown') }}</span>
                        @else
                            <span>{{ $course->type->value }}</span>
                        @endif
                    </div>
                    <div class="flex items-center gap-2">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-files" viewBox="0 0 16 16">
                            <path d="M13 0H6a2 2 0 0 0-2 2 2 2 0 0 0-2 2v10a2 2 0 0 0 2 2h7a2 2 0 0 0 2-2 2 2 0 0 0 2-2V2a2 2 0 0 0-2-2m0 13V4a2 2 0 0 0-2-2H5a1 1 0 0 1 1-1h7a1 1 0 0 1 1 1v10a1 1 0 0 1-1 1M3 4a1 1 0 0 1 1-1h7a1 1 0 0 1 1 1v10a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1z"/>
                        </svg>
                        <span>{{ $course->documents->count() }}</span>
                    </div>
                    <div class="flex items-center gap-2">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-calendar4-week flex-none" viewBox="0 0 16 16">
                            <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5M2 2a1 1 0 0 0-1 1v1h14V3a1 1 0 0 0-1-1zm13 3H1v9a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1z"/>
                            <path d="M11 7.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm-3 0a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm-2 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm-3 0a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5z"/>
                        </svg>
                        <span>
                            @php
                                $semesters = $course->seasons->pluck('code');
                            @endphp
                            @if ($semesters->count() == 0)
                                {{ __('Unknown') }}
                            @elseif ($semesters->count() == 1)
                                {{ __($semesters->first()->label()) }}
                            @else
                                {{ $semesters->map(function($semester) { return __($semester->label()); })->implode(' - ') }}
                            @endif
                        </span>
                    </div>
                </a>
            @endforeach
        </div>
        <table
            class="hidden w-full text-sm text-left text-grey-950 dark:text-grey-50 bg-grey-100 dark:bg-grey-800 dark:border-grey-700 mb-4 md:block">
            <thead class="uppercase bg-grey-300 dark:bg-grey-700 dark:text-grey-200">
            <tr>
                <th scope="col" class="px-6 py-3">
                    {{ __('Code') }}
                </th>
                <th scope="col" class="px-6 py-3">
                    {{ __('Title') }}
                </th>
                <th scope="col" class="px-6 py-3">
                    {{ __('Type of course') }}
                </th>
                <th scope="col" class="px-6 py-3">
                    {{ __('Number of documents') }}
                </th>
                <th scope="col" class="px-6 py-3">
                    {{ Str::plural(__('Semester')) }}
                </th>
            </tr>
            </thead>

            <tbody>
            @foreach ($courses->sortBy('code') as $course)
                <tr class="hover:bg-grey-200 dark:hover:bg-grey-600 border-b cursor-pointer" onclick="window.location='{{route('courses.show', $course->code)}}'">
                    <th scope="row" class="px-6 py-4 font-bold whitespace-nowrap">
                        {{ $course->code }}
                    </th>
                    <td class="px-6 py-4">
                        {{ $course->title }}
                    </td>
                    <td class="px-6 py-4">
                        @if ($course->type == null)
                            {{ __('Unknown') }}
                        @else
                            {{ $course->type->value }}
                        @endif
                    </td>
                    <td class="px-6 py-4">
                        {{ $course->documents->count() }}
                    </td>
                    <td class="px-6 py-4">
                        @php
                            $semesters = $course->seasons->pluck('code');
                        @endphp
                        @if ($semesters->count() == 0)
                            {{ __('Unknown') }}
                        @elseif ($semesters->count() == 1)
                            {{ __($semesters->first()->label()) }}
                        @else
                            {{ $semesters->map(function($semester) { return __($semester->label()); })->implode(' - ') }}
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    @else
        <div
            class="rounded-md p-3 bg-grey-200 text-grey-950 dark:bg-grey-800 dark:text-grey-50 flex justify-start items-center">
            <span>
                {{__("No courses found. If you think this is not normal, please contact the")}}
                <x-link :href="'mailto:simde@assos.utc.fr'"> SiMDE</x-link>.
            </span>
        </div>
    @endif
@endsection
