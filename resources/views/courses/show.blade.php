@php
    use App\Enums\Format as FormatEnum;
    use App\Enums\Activity as ActivityEnum;
    use App\Models\Period;
@endphp
@extends('layouts.app')

@section('content')
    @if (isset($course))
        <div class="p-4 flex flex-col gap-y-5 rounded-md bg-grey-200 dark:bg-grey-800 text-grey-950 dark:text-grey-50">
            <div class="flex justify-between items-top gap-2">
                <h1 id="uv-title" class="font-extrabold text-2xl">
                    <span class="text-purple-400">{{ $course->code }}</span>
                    - {{ $course->title }}
                </h1>
                <div id="favorite-form-section">
                    @if(auth()->user()->courses->contains($course->id))
                        <form id="form-add-favourite-uv" action="{{ route('remove-favourite-course', $course->code) }}" method="POST">
                            @csrf
                            <button title="{{ __('Remove from favourites') }}" type="submit">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-star-fill text-purple-400" viewBox="0 0 16 16">
                                    <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
                                </svg>
                            </button>
                        </form>
                        @else
                        <form id="form-remove-favourite-uv" action="{{ route('add-favourite-course', $course->code) }}" method="POST">
                            @csrf
                            <button title="{{ __('Add to favourites') }}" type="submit">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="currentColor" class="bi bi-star text-purple-400" viewBox="0 0 16 16">
                                    <path d="M2.866 14.85c-.078.444.36.791.746.593l4.39-2.256 4.389 2.256c.386.198.824-.149.746-.592l-.83-4.73 3.522-3.356c.33-.314.16-.888-.282-.95l-4.898-.696L8.465.792a.513.513 0 0 0-.927 0L5.354 5.12l-4.898.696c-.441.062-.612.636-.283.95l3.523 3.356-.83 4.73zm4.905-2.767-3.686 1.894.694-3.957a.56.56 0 0 0-.163-.505L1.71 6.745l4.052-.576a.53.53 0 0 0 .393-.288L8 2.223l1.847 3.658a.53.53 0 0 0 .393.288l4.052.575-2.906 2.77a.56.56 0 0 0-.163.506l.694 3.957-3.686-1.894a.5.5 0 0 0-.461 0z"/>
                                </svg>
                            </button>
                        </form>
                    @endif
                </div>
            </div>

            @php
                $documentCount = $course->documents->count();
                $courseCode = $course->code;
                $userDocumentCount = $course->documents->where('user_id', auth()->id())->count();

                // Count of documents course
                if ($documentCount > 1) {
                    $description = __(
                        'Shwet currently has :documents_count documents which can be useful as part of :course_code',
                        [
                            'documents_count' => '<b>' . $documentCount . '</b>',
                            'course_code' => '<b">' . $courseCode . '</b>'
                        ]
                    );
                } elseif ($documentCount == 1) {
                    $description = __(
                        'Shwet currently has :documents_count document which can be useful as part of :course_code',
                        [
                            'documents_count' => '<b>' . $documentCount . '</b>',
                            'course_code' => '<b>'. $courseCode .'</b>',
                        ]
                    );
                } else {
                    $description = __(
                        'Shwet currently has no documents which can be useful as part of :course_code',
                        [
                            'course_code' => '<b>'.$courseCode.'</b>',
                        ]
                    );
                }

                // Count of documents user
                if ($userDocumentCount > 1) {
                    $description = $description . ', ' . __(
                        'knowing that :user_documents_count were added by you, well done!<br/>',
                        [
                            'user_documents_count' => '<b>' . $userDocumentCount . '</b>'
                        ]
                    );
                } elseif ($userDocumentCount == 1) {
                    $description = $description . ', ' . __('knowing that <b>1</b> was added by you, well done!<br/>');
                } else {
                    $description = $description . '.<br/>';
                }

                // Add to favourites courses
                if (!auth()->user()->courses->contains($course->id)) {
                    $description = $description . __(
                        'You can now add :course_code to your favorite UVs or ',
                        [
                            'course_code' => '<b>' . $courseCode . '</b>'
                        ]
                    );
                } else {
                    $description = $description . __(
                        'You can ',
                    );
                }

                // Still contribute by depositing a new document
                if ($userDocumentCount > 1) {
                    $description = $description . __('keep contributing by depositing a new document.');
                } else {
                    $description = $description . __('contribute by depositing a new document.');
                }
            @endphp

            <p>{!! $description !!}</p>
            
            <div class="flex justify-between">
                <form method="GET" action="{{ route('courses.show', $course->code) }}" class="flex justify-between gap-x-2">
                    <div class="flex gap-3">
                        <!-- Dropdown filter for season -->
                        <x-dropdown.index :id="'dropdownSeasonButton'" :dropdownId="'dropdownSeason'">
                            <x-slot:icon>
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-calendar4-week flex-none" viewBox="0 0 16 16">
                                    <path d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5M2 2a1 1 0 0 0-1 1v1h14V3a1 1 0 0 0-1-1zm13 3H1v9a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1z"/>
                                    <path d="M11 7.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm-3 0a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm-2 3a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5zm-3 0a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5v1a.5.5 0 0 1-.5.5h-1a.5.5 0 0 1-.5-.5z"/>
                                </svg>
                            </x-slot:icon>
                            <x-slot:title>
                                {{ ucfirst(__('semester')) }}
                            </x-slot:title>
                            <x-slot:dropdownItems>
                                <x-dropdown.item-checkbox :inputId="'season-all'" :name="'season_codes[]'" :value="'all'"
                                :checked="empty($filters['season_codes']) || in_array('all', $filters['season_codes'])">
                                    <x-slot:label>
                                        {{ __('Select all') }}
                                    </x-slot:label>
                                </x-dropdown.item-checkbox>
                                @php
                                    $seasonOrder = ['P', 'A', 'E', 'H'];
                                @endphp
                                @foreach($seasons->sortBy(function($season) use ($seasonOrder) {
                                    return array_search($season->code, $seasonOrder);
                                }) as $season)
                                    <x-dropdown.item-checkbox :inputId="'season-'.$season->code->value" :name="'season_codes[]'" :value="$season->code->value"
                                                            :checked="!empty($filters['season_codes']) && in_array($season->code->value, $filters['season_codes'])">
                                        <x-slot:label>
                                            {{ __($season->code->label()) }}
                                        </x-slot:label>
                                    </x-dropdown.item-checkbox>
                                @endforeach
                            </x-slot:dropdownItems>
                        </x-dropdown.index>
                    </div>

                    <!-- Apply filters button -->
                    <x-buttons.icon-text-purple :type="'submit'" :responsive="true" :outline="true">
                        <x-slot:icon>
                            <svg class="w-5 h-5" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" fill="none" viewBox="0 0 24 24">
                                <path stroke="currentColor" stroke-linecap="round" stroke-width="1.5" d="M18.796 4H5.204a1 1 0 0 0-.753 1.659l5.302 6.058a1 1 0 0 1 .247.659v4.874a.5.5 0 0 0 .2.4l3 2.25a.5.5 0 0 0 .8-.4v-7.124a1 1 0 0 1 .247-.659l5.302-6.059c.566-.646.106-1.658-.753-1.658Z"/>
                            </svg>
                        </x-slot:icon>

                        <x-slot:text>
                            {{ __('Apply filters') }}
                        </x-slot:text>
                    </x-buttons.icon-text-purple>
                </form>
                <x-buttons.icon-text-purple id="store-new-document" data-course="{{ $course->code }}" data-modal-target="modal-document" data-modal-show="modal-document">
                    <x-slot:icon>
                        <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-file-earmark-arrow-up" viewBox="0 0 16 16">
                            <path d="M8.5 11.5a.5.5 0 0 1-1 0V7.707L6.354 8.854a.5.5 0 1 1-.708-.708l2-2a.5.5 0 0 1 .708 0l2 2a.5.5 0 0 1-.708.708L8.5 7.707z"/>
                            <path d="M14 14V4.5L9.5 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2M9.5 3A1.5 1.5 0 0 0 11 4.5h2V14a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h5.5z"/>
                        </svg>
                    </x-slot:icon>
                    <x-slot:text>
                        {{__("Publish a document")}}
                    </x-slot:text>
                </x-buttons.icon-text-purple>
            </div>
        </div>
        @php
            $allDocumentsByActivity = $documents->groupBy('activity');
            $activities = ActivityEnum::cases();
        @endphp
        @foreach($activities as $activity)
            @php
                // Retrieve the documents for the current activity as Collection type
                $documentsByActivity = $allDocumentsByActivity->get($activity->value);

                $allDocumentsByActivityAreLinks = $documentsByActivity ? $documentsByActivity->every(function ($document) {
                    return $document->format->value == FormatEnum::LINK->value;
                }) : false;
            @endphp
            @if($documentsByActivity)
            <div id="accordion-open-{{ $activity }}" data-accordion="open">
                <div id="accordion-open-heading-{{ $activity }}">
                    <button type="button" class="flex items-center justify-between w-full p-4 rounded-t-md gap-3 bg-grey-200 dark:bg-grey-800" data-accordion-target="#accordion-open-body-{{ $activity }}" aria-expanded="false" aria-controls="accordion-open-body-{{ $activity }}">
                        <h2 class="text-purple-400 dark:text-purple-400">
                            <span class="text-xl font-bold">{{ __($activity->label()) }}</span>
                            @php
                                $numberOfDocumentsByActivity = $documentsByActivity->count();
                            @endphp
                            <span class="text-grey-500 dark:text-grey-400">{{ "(" . $numberOfDocumentsByActivity . ' ' . ($numberOfDocumentsByActivity > 1 ? Str::plural(__('document')) : __('document')) . ")" }}</span>
                        </h2>
                        <svg data-accordion-icon class="w-4 h-4 rotate-180 shrink-0 text-purple-400 dark:text-purple-400" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 10 6">
                            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5 5 1 1 5"/>
                        </svg>
                    </button>
                </div>

                <div id="accordion-open-body-{{ $activity }}" class="hidden" aria-labelledby="accordion-open-heading-{{ $activity }}">
                    <div class="p-5 rounded-b-md bg-grey-200 dark:bg-grey-800 text-grey-950 dark:text-grey-50">
                        <form action="{{ route('download-selected-documents') }}" method="POST">
                            @csrf
                            <input type="hidden" name="course_code" value="{{ $course->code }}">
                            <input type="hidden" name="activity" value="{{ $activity }}">
                            <table class="w-full mb-4 text-grey-950 dark:text-grey-50">
                                @if (!$allDocumentsByActivityAreLinks)
                                    <thead>
                                        <tr>
                                            <th class="text-left w-20" colspan="7">
                                                <label for="selected_documents[]" class="inline-flex items-center">
                                                    <input type="checkbox" id="file-{{ $activity->value }}-all" name="selected_documents[]" value="all" class="mr-5 w-4 h-4 text-purple-400 bg-grey-50 border-grey-300 rounded focus:ring-purple-400 focus:ring-2">
                                                    {{ __('Select all') }}
                                                </label>
                                            </th>
                                        </tr>
                                    </thead>
                                @endif
                                <tbody>
                                    @php
                                        $seasonOrder = ['H', 'P', 'E', 'A'];
                                    @endphp
                                    @foreach($documentsByActivity->groupBy('period.id')->sortByDesc(function ($group, $periodId) use ($seasonOrder) {
                                        $year = Period::find($periodId)->year;
                                        $season = Period::find($periodId)->season->code->value;
                                        return [$year, array_search($season, $seasonOrder)];
                                    }) as $periodId => $documentsByActivityAndPeriod)
                                        @php
                                            $period = Period::find($periodId);
                                        @endphp
                                        <tr>
                                            <th class="w-16 inline pt-4">
                                                {{ $period->getLabel() }}
                                            </th>
                                            <td class="pb-2">
                                                <ul class="grid grid-cols-1 gap-2">
                                                    @foreach($documentsByActivityAndPeriod as $document)
                                                        <x-document :document="$document" :showOwner="true" :selectable="true" :coursePage="true" />
                                                    @endforeach
                                                </ul>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>

                            @if (!$allDocumentsByActivityAreLinks)
                                <div class="flex justify-start">
                                    <x-buttons.icon-text-purple :type="'submit'">
                                        <x-slot:icon>
                                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" class="w-5 h-5">
                                                <path stroke-linecap="round" stroke-linejoin="round" d="M3 16.5v2.25A2.25 2.25 0 0 0 5.25 21h13.5A2.25 2.25 0 0 0 21 18.75V16.5M16.5 12 12 16.5m0 0L7.5 12m4.5 4.5V3" />
                                            </svg>
                                        </x-slot:icon>
                                        <x-slot:text>
                                            {{ __('Download selection') }}
                                        </x-slot:text>
                                    </x-buttons.icon-text-purple>
                                </div>
                            @endif
                        </form>
                    </div>
                </div>
            </div>
            @endif
        @endforeach
        <x-modals.document-edit/>
        <x-modals.document-delete/>
        <script>
            // Get the 'season-all' checkbox element
            const allSeasons = document.getElementById('season-all');
            // Get all other season checkboxes except 'season-all'
            const otherSeasons = Array.from(document.querySelectorAll('input[id^="season-"]')).filter(function (checkbox) {
                return checkbox.id !== 'season-all';
            });

            // Add event listener to 'other' season checkbox
            otherSeasons.forEach(function (checkbox) {
                checkbox.addEventListener('change', function () {
                    // If a checkbox is checked, uncheck all 'all' checkboxes
                    if (this.checked) {
                        allSeasons.checked = false;
                    }
                });
            });

            // Add event listener to 'all' season checkbox
            allSeasons.addEventListener('change', function () {
                // If a 'all' checkbox is checked, uncheck all 'other' checkboxes
                if (this.checked) {
                    otherSeasons.forEach(function (checkbox) {
                        checkbox.checked = false;
                    });
                }
            });

            // Attaching a click event listener to the button with the id "store-new-document"
            document.getElementById('store-new-document').addEventListener('click', function() {
                // Setting the value of the input with the id "course" to the value of the data-course attribute of the button
                var courseCode = $(this).data('course');
                $('#course').val(courseCode).trigger('change');
            });

            // Wait for the DOM to be fully loaded before executing the code
            document.addEventListener('DOMContentLoaded', function() {
                // Select all elements (checkbox inputs) whose ID starts with "file-"
                const activities = document.querySelectorAll('[id^="file-"]');

                // For each activity found on the page
                activities.forEach(function(activity) {
                    // Get the ID of the activity from its ID attribute
                    const activitySplit = activity.id.split('-');
                    const activityId = activitySplit.slice(1, -1).join('-');

                    // Select the "Select All" checkbox for this activity
                    const activityAllCheckbox = document.getElementById(`file-${activityId}-all`);

                    // Select all checkboxes for this activity (excluding the "Select All" checkbox)
                    const activityCheckboxes = Array
                        .from(document.querySelectorAll(`input[id^="file-${activityId}-"]`))
                        .filter(checkbox => checkbox.id !== `file-${activityId}-all`);

                    if(activityAllCheckbox) {
                        // Add an event listener for the "Select All" checkbox of this activity
                        activityAllCheckbox.addEventListener('change', function() {
                            // Check or uncheck all checkboxes for this activity
                            activityCheckboxes.forEach(checkbox => checkbox.checked = this.checked);
                        });

                        // Add an event listener for each checkbox of this activity (excluding the "Select All" checkbox)
                        activityCheckboxes.forEach(function(checkbox) {
                            checkbox.addEventListener('change', function() {
                                // If any of the checkboxes is unchecked, uncheck the "Select All" checkbox
                                if (!this.checked) {
                                    activityAllCheckbox.checked = false;
                                } else {
                                    // Check if all checkboxes for this activity are checked
                                    const allChecked = activityCheckboxes.every(checkbox => checkbox.checked);

                                    // If all checkboxes are checked, check the "Select All" checkbox
                                    if (allChecked) {
                                        activityAllCheckbox.checked = true;
                                    }
                                }
                            });
                        });
                    }
                });
            });
        </script>

        <!-- Comments modal -->
        <x-modals.comment/>
    @else
        @php
            @abort(404, 'Course not found');
        @endphp
    @endif
@endsection
