@php
    use App\Models\Course;
    use App\Models\Period;
@endphp

@extends('layouts.app')

@section('content')
    @php
        $documents = auth()->user()->documents()->get();
    @endphp

    <div class="p-4 flex flex-col gap-y-5 rounded-md bg-grey-200 dark:bg-grey-800 text-grey-950 dark:text-grey-50">
        <h1 id="uv-title" class="font-extrabold text-2xl text-purple-400">
            {{__("My documents")}}
        </h1>


        <p>
            {{__("You can find here all the documents you have uploaded on the platform.")}}<br>

            <span class="flex flex-row flex-wrap items-center gap-1 text-wrap">
                @if($documents->count() > 0)
                    @php
                        $documentLabel = $documents->count() > 1 ? "documents" : "document";
                    @endphp
                    {!! __("Congrats, you have uploaded <b>:count ".$documentLabel."</b> on Shwet!", ["count" => $documents->count()]) !!}
                    <svg width="16" height="16" viewBox="0 0 36 36" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" role="img" class="iconify iconify--twemoji" preserveAspectRatio="xMidYMid meet"><path fill="#DD2E44" d="M11.626 7.488a1.413 1.413 0 0 0-.268.395l-.008-.008L.134 33.141l.011.011c-.208.403.14 1.223.853 1.937c.713.713 1.533 1.061 1.936.853l.01.01L28.21 24.735l-.008-.009c.147-.07.282-.155.395-.269c1.562-1.562-.971-6.627-5.656-11.313c-4.687-4.686-9.752-7.218-11.315-5.656z"></path><path fill="#EA596E" d="M13 12L.416 32.506l-.282.635l.011.011c-.208.403.14 1.223.853 1.937c.232.232.473.408.709.557L17 17l-4-5z"></path><path fill="#A0041E" d="M23.012 13.066c4.67 4.672 7.263 9.652 5.789 11.124c-1.473 1.474-6.453-1.118-11.126-5.788c-4.671-4.672-7.263-9.654-5.79-11.127c1.474-1.473 6.454 1.119 11.127 5.791z"></path><path fill="#AA8DD8" d="M18.59 13.609a.99.99 0 0 1-.734.215c-.868-.094-1.598-.396-2.109-.873c-.541-.505-.808-1.183-.735-1.862c.128-1.192 1.324-2.286 3.363-2.066c.793.085 1.147-.17 1.159-.292c.014-.121-.277-.446-1.07-.532c-.868-.094-1.598-.396-2.11-.873c-.541-.505-.809-1.183-.735-1.862c.13-1.192 1.325-2.286 3.362-2.065c.578.062.883-.057 1.012-.134c.103-.063.144-.123.148-.158c.012-.121-.275-.446-1.07-.532a.998.998 0 0 1-.886-1.102a.997.997 0 0 1 1.101-.886c2.037.219 2.973 1.542 2.844 2.735c-.13 1.194-1.325 2.286-3.364 2.067c-.578-.063-.88.057-1.01.134c-.103.062-.145.123-.149.157c-.013.122.276.446 1.071.532c2.037.22 2.973 1.542 2.844 2.735c-.129 1.192-1.324 2.286-3.362 2.065c-.578-.062-.882.058-1.012.134c-.104.064-.144.124-.148.158c-.013.121.276.446 1.07.532a1 1 0 0 1 .52 1.773z"></path><path fill="#77B255" d="M30.661 22.857c1.973-.557 3.334.323 3.658 1.478c.324 1.154-.378 2.615-2.35 3.17c-.77.216-1.001.584-.97.701c.034.118.425.312 1.193.095c1.972-.555 3.333.325 3.657 1.479c.326 1.155-.378 2.614-2.351 3.17c-.769.216-1.001.585-.967.702c.033.117.423.311 1.192.095a1 1 0 1 1 .54 1.925c-1.971.555-3.333-.323-3.659-1.479c-.324-1.154.379-2.613 2.353-3.169c.77-.217 1.001-.584.967-.702c-.032-.117-.422-.312-1.19-.096c-1.974.556-3.334-.322-3.659-1.479c-.325-1.154.378-2.613 2.351-3.17c.768-.215.999-.585.967-.701c-.034-.118-.423-.312-1.192-.096a1 1 0 1 1-.54-1.923z"></path><path fill="#AA8DD8" d="M23.001 20.16a1.001 1.001 0 0 1-.626-1.781c.218-.175 5.418-4.259 12.767-3.208a1 1 0 1 1-.283 1.979c-6.493-.922-11.187 2.754-11.233 2.791a.999.999 0 0 1-.625.219z"></path><path fill="#77B255" d="M5.754 16a1 1 0 0 1-.958-1.287c1.133-3.773 2.16-9.794.898-11.364c-.141-.178-.354-.353-.842-.316c-.938.072-.849 2.051-.848 2.071a1 1 0 1 1-1.994.149c-.103-1.379.326-4.035 2.692-4.214c1.056-.08 1.933.287 2.552 1.057c2.371 2.951-.036 11.506-.542 13.192a1 1 0 0 1-.958.712z"></path><circle fill="#5C913B" cx="25.5" cy="9.5" r="1.5"></circle><circle fill="#9266CC" cx="2" cy="18" r="2"></circle><circle fill="#5C913B" cx="32.5" cy="19.5" r="1.5"></circle><circle fill="#5C913B" cx="23.5" cy="31.5" r="1.5"></circle><circle fill="#FFCC4D" cx="28" cy="4" r="2"></circle><circle fill="#FFCC4D" cx="32.5" cy="8.5" r="1.5"></circle><circle fill="#FFCC4D" cx="29.5" cy="12.5" r="1.5"></circle><circle fill="#FFCC4D" cx="7.5" cy="23.5" r="1.5"></circle></svg>
                @else
                    {{__("Woops, it seems like you have not uploaded any documents on Shwet yet.")}}
                    <svg width="16" height="16" viewBox="0 0 128 128" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" aria-hidden="true" role="img" class="iconify iconify--noto" preserveAspectRatio="xMidYMid meet">
                        <radialGradient id="IconifyId17ecdb2904d178eab7785" cx="63.6" cy="1992.9" r="56.96" gradientTransform="translate(0 -1930)" gradientUnits="userSpaceOnUse">
                        <stop offset=".5" stop-color="#fde030">
                        </stop>
                        <stop offset=".92" stop-color="#f7c02b">
                        </stop>
                        <stop offset="1" stop-color="#f4a223">
                        </stop>
                        </radialGradient>
                        <path d="M63.6 118.8c-27.9 0-58-17.5-58-55.9S35.7 7 63.6 7c15.5 0 29.8 5.1 40.4 14.4c11.5 10.2 17.6 24.6 17.6 41.5s-6.1 31.2-17.6 41.4c-10.6 9.3-25 14.5-40.4 14.5z" fill="url(#IconifyId17ecdb2904d178eab7785)">
                        </path>
                        <path d="M111.49 29.67c5.33 8.6 8.11 18.84 8.11 30.23c0 16.9-6.1 31.2-17.6 41.4c-10.6 9.3-25 14.5-40.4 14.5c-18.06 0-37-7.35-48.18-22.94c10.76 17.66 31 25.94 50.18 25.94c15.4 0 29.8-5.2 40.4-14.5c11.5-10.2 17.6-24.5 17.6-41.4c0-12.74-3.47-24.06-10.11-33.23z" fill="#eb8f00">
                        </path>
                        <g>
                        <g fill="#422b0d">
                        <path d="M64 87.2c10.8 0 17.8 7.9 19.7 11.6c.7 1.4.7 2.6.1 3.1c-.64.4-1.46.4-2.1 0c-.32-.13-.62-.3-.9-.5A29.3 29.3 0 0 0 64 95.9c-6.01.08-11.87 1.96-16.8 5.4c-.28.2-.58.37-.9.5c-.64.4-1.46.4-2.1 0c-.6-.6-.6-1.7.1-3.1c1.9-3.6 8.9-11.5 19.7-11.5z">
                        </path>
                        <path d="M74.4 67.7a3.34 3.34 0 0 0-.18 4.73c.2.22.43.41.68.57c2.92 2.17 6.2 3.8 9.7 4.8c3.53.88 7.18 1.15 10.8.8a3.415 3.415 0 0 0 3.32-3.5c-.01-.27-.05-.54-.12-.8a4.009 4.009 0 0 0-4.1-2.7c-2.68.26-5.38.1-8-.5c-2.58-.79-5.02-2.01-7.2-3.6a3.914 3.914 0 0 0-4.9.2z">
                        </path>
                        <path d="M53.6 67.7a3.34 3.34 0 0 1 .18 4.73c-.2.22-.43.41-.68.57c-2.92 2.17-6.2 3.8-9.7 4.8a31.93 31.93 0 0 1-10.8.8a3.308 3.308 0 0 1-3.1-4.3a4.009 4.009 0 0 1 4.1-2.7c2.68.26 5.38.1 8-.5c2.58-.79 5.02-2.01 7.2-3.6a3.773 3.773 0 0 1 4.8.2z">
                        </path>
                        </g>
                        </g>
                    </svg>
                @endif
            </span>
        </p>

        <div class="flex gap-3">
            <x-buttons.icon-text-purple data-modal-target="modal-document"
                                        data-modal-show="modal-document">
                <x-slot:icon>
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor"
                         class="bi bi-file-earmark-arrow-up" viewBox="0 0 16 16">
                        <path
                            d="M8.5 11.5a.5.5 0 0 1-1 0V7.707L6.354 8.854a.5.5 0 1 1-.708-.708l2-2a.5.5 0 0 1 .708 0l2 2a.5.5 0 0 1-.708.708L8.5 7.707z"/>
                        <path
                            d="M14 14V4.5L9.5 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2M9.5 3A1.5 1.5 0 0 0 11 4.5h2V14a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1h5.5z"/>
                    </svg>
                </x-slot:icon>
                <x-slot:text>
                    {{__("Publish a document")}}
                </x-slot:text>
            </x-buttons.icon-text-purple>

            <x-dropdown.index :id="'button-dropdown-filter-documents'" :dropdownId="'dropdown-filter-documents'">
                <x-slot:icon>
                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor" class="bi bi-sort-down" viewBox="0 0 16 16">
                        <path d="M3.5 2.5a.5.5 0 0 0-1 0v8.793l-1.146-1.147a.5.5 0 0 0-.708.708l2 1.999.007.007a.497.497 0 0 0 .7-.006l2-2a.5.5 0 0 0-.707-.708L3.5 11.293zm3.5 1a.5.5 0 0 1 .5-.5h7a.5.5 0 0 1 0 1h-7a.5.5 0 0 1-.5-.5M7.5 6a.5.5 0 0 0 0 1h5a.5.5 0 0 0 0-1zm0 3a.5.5 0 0 0 0 1h3a.5.5 0 0 0 0-1zm0 3a.5.5 0 0 0 0 1h1a.5.5 0 0 0 0-1z"/>
                    </svg>
                </x-slot:icon>
                <x-slot:title>
                    {{__("Sort by")}}
                </x-slot:title>
                <x-slot:dropdownItems>
                    <x-dropdown.item-link :href="route('my-documents', ['order' => 'period'])">
                        {{__("Semester")}}
                    </x-dropdown.item-link>
                    <x-dropdown.item-link :href="route('my-documents', ['order' => 'course'])">
                        {{__("UV")}}
                    </x-dropdown.item-link>
                </x-slot:dropdownItems>
            </x-dropdown.index>
        </div>
    </div>
    @php
        $seasonOrder = ['H', 'P', 'E', 'A'];

        $order = request('order', 'course');

        if($order != 'course' && $order != 'period') {
            // Reset $order to default value if invalid
            $order = 'course';
        }
    @endphp
    @if ($order == 'course')
        @foreach($documents->groupBy('course_id')->sortBy(function ($group, $courseId) {
            $code = Course::find($courseId)->code;
            return $code;
            }) as $courseId => $documentsByCourse)
            @php
                $course = $documentsByCourse->first()->course;
            @endphp
            @if($documentsByCourse)
                <div id="accordion-open-{{ $course->code }}" data-accordion="open">
                    <div id="accordion-open-heading-{{ $course->code }}">
                        <button type="button" class="flex items-center justify-between w-full p-4 rounded-t-md gap-3 bg-grey-200 dark:bg-grey-800" data-accordion-target="#accordion-open-body-{{ $course->code }}" aria-expanded="false" aria-controls="accordion-open-body-{{ $course->code }}">
                            <h2 class="text-purple-400 dark:text-purple-400">
                                <span class="text-xl font-bold">{{ $course->code }}</span>
                                @php
                                    $numberOfDocumentsByCourse = $documentsByCourse->count();
                                @endphp
                                <span class="text-grey-500 dark:text-grey-400">{{ "(" . $numberOfDocumentsByCourse . ' ' . ($numberOfDocumentsByCourse > 1 ? Str::plural(__('document')) : __('document')) . ")" }}</span>
                            </h2>
                            <svg data-accordion-icon class="w-4 h-4 rotate-180 shrink-0 text-purple-400 dark:text-purple-400" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 10 6">
                                <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5 5 1 1 5"/>
                            </svg>
                        </button>
                    </div>

                    <div id="accordion-open-body-{{ $course->code }}" class="hidden" aria-labelledby="accordion-open-heading-{{ $course->code }}">
                        <div class="p-5 rounded-b-md bg-grey-200 dark:bg-grey-800 text-grey-950 dark:text-grey-50">
                            <table class="w-full mb-4 text-grey-950 dark:text-grey-50">
                                <tbody>
                                @foreach($documentsByCourse->groupBy('period.id')->sortByDesc(function ($group, $periodId) use ($seasonOrder) {
                                    $year = Period::find($periodId)->year;
                                    $season = Period::find($periodId)->season->code->value;
                                    return [$year, array_search($season, $seasonOrder)];
                                }) as $periodId => $documentsByCourseAndPeriod)
                                    @php
                                        $period = Period::find($periodId);
                                    @endphp
                                        <tr>
                                            <th class="w-16 inline pt-4">
                                                {{ $period->season->code->value . $period->year }}
                                            </th>
                                            <td class="pb-2">
                                                <ul class="grid grid-cols-1 gap-2">
                                                    @foreach($documentsByCourseAndPeriod as $document)
                                                        <x-document :document="$document" :showDates="true" :showActivity="true" />
                                                    @endforeach
                                                </ul>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @endif
        @endforeach
    @elseif ($order == 'period')
        @foreach($documents->groupBy('period.id')->sortByDesc(function ($group, $periodId) use ($seasonOrder) {
            $year = Period::find($periodId)->year;
            $season = Period::find($periodId)->season->code->value;
            return [$year, array_search($season, $seasonOrder)];
        }) as $periodId => $documentsByPeriod)
            @php
                $period = Period::find($periodId);
            @endphp
            @if($documentsByPeriod)
                <div id="accordion-open-{{ $period->season->code->value . $period->year }}" data-accordion="open">
                    <div id="accordion-open-heading-{{ $period->season->code->value . $period->year }}">
                        <button type="button" class="flex items-center justify-between w-full p-4 rounded-t-md gap-3 bg-grey-200 dark:bg-grey-800" data-accordion-target="#accordion-open-body-{{ $period->season->code->value . $period->year }}" aria-expanded="false" aria-controls="accordion-open-body-{{ $period->season->code->value . $period->year }}">
                            <h2 class="text-purple-400 dark:text-purple-400">
                                <span class="text-xl font-bold">{{ $period->season->code->value . $period->year }}</span>
                                @php
                                    $numberOfDocumentsByPeriod = $documentsByPeriod->count();
                                @endphp
                                <span class="text-grey-500 dark:text-grey-400">{{ "(" . $numberOfDocumentsByPeriod . ' ' . ($numberOfDocumentsByPeriod > 1 ? Str::plural(__('document')) : __('document')) . ")" }}</span>
                            </h2>
                            <svg data-accordion-icon class="w-4 h-4 rotate-180 shrink-0 text-purple-400 dark:text-purple-400" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 10 6">
                                <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5 5 1 1 5"/>
                            </svg>
                        </button>
                    </div>

                    <div id="accordion-open-body-{{ $period->season->code->value . $period->year }}" class="hidden" aria-labelledby="accordion-open-heading-{{ $period->season->code->value . $period->year }}">
                        <div class="p-5 rounded-b-md bg-grey-200 dark:bg-grey-800 text-grey-950 dark:text-grey-50">
                            <table class="w-full mb-4 text-grey-950 dark:text-grey-50">
                                <tbody>
                                    @foreach($documentsByPeriod->groupBy('course_id')->sortBy(function ($group, $courseId) {
                                        $code = Course::find($courseId)->code;
                                        return $code;
                                        }) as $courseId => $documentsByPeriodAndCourse)
                                        @php
                                            $course = $documentsByPeriodAndCourse->first()->course;
                                        @endphp
                                        <tr>
                                            <th class="w-16 pb-2">
                                                <a href="{{ route('courses.show', $course->code) }}" class="hover:text-purple-400">{{ $course->code }}</a>
                                            </th>
                                            <td class="pb-2">
                                                <ul class="grid grid-cols-1 gap-2">
                                                    @foreach($documentsByPeriodAndCourse as $document)
                                                        <x-document :document="$document" :showDates="true" :showActivity="true" />
                                                    @endforeach
                                                </ul>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            @endif
        @endforeach
    @endif

    <x-modals.document-edit/>
    <x-modals.document-delete/>

@endsection
