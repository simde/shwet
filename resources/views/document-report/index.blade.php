@extends('layouts.app')

@section('content')
    @php
    use App\Enums\ReportStatus as ReportStatusEnum;

    $requestPath = request()->path();

    $filters = request()->all();

    if(!isset($filters['filter'])){
        if ($requestPath == 'admin/document-reports') {
            $filters['filter'][] = ReportStatusEnum::PENDING->value;
        } else {
            $filters['filter'] = ReportStatusEnum::values();
        }
    }

    $reports = $reports->filter(function($item) use ($filters) {
        return in_array($item->status, $filters['filter']);
    });
    @endphp
    <div class="p-4 flex flex-col gap-y-5 rounded-md bg-grey-200 dark:bg-grey-800 text-grey-950 dark:text-grey-50">
        <h1 class="font-extrabold text-2xl text-purple-400">
            {{ $requestPath == 'admin/document-reports' ? __("All reports") : __("My reports") }}
        </h1>
        <div class="flex flex-row items-center gap-5">
            <!-- Dropdown filter -->
            <form>
                <x-dropdown.index :id="'dropdownFilterButton'" :dropdownId="'dropdownFilter'">
                    <x-slot:icon>
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-funnel" viewBox="0 0 16 16">
                            <path d="M1.5 1.5A.5.5 0 0 1 2 1h12a.5.5 0 0 1 .5.5v2a.5.5 0 0 1-.128.334L10 8.692V13.5a.5.5 0 0 1-.342.474l-3 1A.5.5 0 0 1 6 14.5V8.692L1.628 3.834A.5.5 0 0 1 1.5 3.5zm1 .5v1.308l4.372 4.858A.5.5 0 0 1 7 8.5v5.306l2-.666V8.5a.5.5 0 0 1 .128-.334L13.5 3.308V2z"/>
                        </svg>
                    </x-slot:icon>
                    <x-slot:title>
                        {{ __('Filter') }}
                    </x-slot:title>
                    <x-slot:dropdownItems>
                        @foreach(ReportStatusEnum::cases() as $status)
                            <x-dropdown.item-checkbox :inputId="'status-'.$status->value" :name="'filter[]'" :value="$status->value"
                                                      :checked="(empty($filters['filter']) || in_array($status->value, $filters['filter']))">
                                <x-slot:label>
                                    {{ __($status->label()) }}
                                </x-slot:label>
                            </x-dropdown.item-checkbox>
                        @endforeach
                            <li>
                                <button type="submit" class="w-full p-2 bg-purple-400 text-white rounded-md hover:brightness-90">
                                    {{ __('Apply') }}
                                </button>
                            </li>
                    </x-slot:dropdownItems>
                </x-dropdown.index>
            </form>

            <div class="flex flex-row flex-wrap items-center">
                @foreach($filters['filter'] as $key => $value)
                    <x-status-badge :status="$value"/>
                @endforeach
            </div>
        </div>


        @if(isset($reports) && count($reports) > 0)
            <div class="flex gap-3 flex-wrap justify-center sm:justify-start">
                @foreach($reports as $report)
                   <x-document-report-card :report="$report"/>
                @endforeach
            </div>
            <x-modals.document-edit/>
            <x-modals.document-delete/>
        @else
            <p class="text-grey-500 dark:text-grey-300">
                {{__("No reports found")}}
            </p>
        @endif
    </div>
@endsection
