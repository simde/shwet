<div {{ $attributes->merge(['class' => 'slide w-full text-center p-1 text-sm text-grey-950 bg-grey-200 dark:text-grey-50 dark:bg-grey-800']) }}>
    <span class="inline-block">{{ $slot }}</span>
</div>
