@php
    use App\Enums\Activity as ActivityEnum;
    use App\Enums\Format as FormatEnum;
    use App\Enums\Season as SeasonEnum;
@endphp
@props([
    'document',
    'showOwner' => false,
    'showDates' => false,
    'selectable' => false,
    'showActivity' => false,
    'coursePage' => false
])

@php
    $showAdminDocument = $document->user_id == auth()->id() || auth()->user()->isAdmin();
@endphp

<li class="flex flex-row w-full items-center gap-2 border-2 border-grey-100 dark:border-grey-600 bg-grey-100 dark:bg-grey-600 rounded-md p-2 hover:border-purple-400 hover:dark:border-purple-400">
    @if($selectable)
        @if ($document->format->value == FormatEnum::FILE->value)
            <div class="w-5">
                <input type="checkbox" id="file-{{ $document->activity->value }}-{{ $document->id }}" name="selected_documents[]" value="{{ $document->path }}" class="w-4 h-4 text-purple-400 bg-grey-50 border-grey-500 rounded focus:ring-purple-400 focus:ring-2"/>
            </div>
        @else
            <div class="w-5"></div>
        @endif
    @endif
    <div class="w-full flex flex-col justify-between items-start gap-3 md:flex-row md:justify-between md:items-center">
        <div id="document-{{$document->id}}-info"
             class="flex flex-col gap-1 items-start">
            @php
                if($document->format->value == FormatEnum::LINK->value) {
                    $link=$document->path;
                }
                else {
                    $link=$document->course->code . '/' . $document->path;

                    // If not already in the course page, open the document in the right place
                    if(!$coursePage) {
                        $link = 'courses/' . $link;
                    }
                }
            @endphp
            <div class="flex flex-col md:flex-row md:flex-wrap md:items-center gap-1">
                <div class="flex flex-row flex-wrap items-center gap-1 text-md">
                    @if ($document->format->value == FormatEnum::LINK->value)
                        <button data-popover-target="popover-format-link-{{$document->id}}" data-popover-placement="bottom-start">
                            <svg xmlns="http://www.w3.org/2000/svg" width="22" height="22" fill="currentColor"
                                 class="bi bi-link-45deg fill-purple-400" viewBox="0 0 16 16">
                                <path
                                    d="M4.715 6.542 3.343 7.914a3 3 0 1 0 4.243 4.243l1.828-1.829A3 3 0 0 0 8.586 5.5L8 6.086a1 1 0 0 0-.154.199 2 2 0 0 1 .861 3.337L6.88 11.45a2 2 0 1 1-2.83-2.83l.793-.792a4 4 0 0 1-.128-1.287z"/>
                                <path
                                    d="M6.586 4.672A3 3 0 0 0 7.414 9.5l.775-.776a2 2 0 0 1-.896-3.346L9.12 3.55a2 2 0 1 1 2.83 2.83l-.793.792c.112.42.155.855.128 1.287l1.372-1.372a3 3 0 1 0-4.243-4.243z"/>
                            </svg>
                        </button>
                        <div data-popover id="popover-format-link-{{$document->id}}" role="tooltip"
                             class="absolute z-10 invisible inline-block text-sm text-grey-700 transition-opacity duration-300 bg-white border border-grey-200 rounded-md shadow-sm opacity-0 max-w-sm lg:max-w-3xl dark:bg-grey-800 dark:border-grey-600 dark:text-grey-200">
                            <div class="p-3">
                                <h3 class="font-bold">{{ __($document->format->label()) }}</h3>
                                <p class="truncate"><x-link :href="$document->path">{{ $document->path }}</x-link></p>
                            </div>
                            <div data-popper-arrow></div>
                        </div>
                    @elseif ($document->format->value == FormatEnum::FILE->value)
                        <button data-popover-target="popover-format-file-{{$document->id}}" data-popover-placement="bottom-start" type="button">
                            <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" fill="currentColor"
                                 class="bi bi-file-earmark-text fill-purple-400" viewBox="0 0 16 16">
                                <path
                                    d="M5.5 7a.5.5 0 0 0 0 1h5a.5.5 0 0 0 0-1zM5 9.5a.5.5 0 0 1 .5-.5h5a.5.5 0 0 1 0 1h-5a.5.5 0 0 1-.5-.5m0 2a.5.5 0 0 1 .5-.5h2a.5.5 0 0 1 0 1h-2a.5.5 0 0 1-.5-.5"/>
                                <path
                                    d="M9.5 0H4a2 2 0 0 0-2 2v12a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V4.5zm0 1v2A1.5 1.5 0 0 0 11 4.5h2V14a1 1 0 0 1-1 1H4a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1z"/>
                            </svg>
                        </button>
                        <div data-popover id="popover-format-file-{{$document->id}}" role="tooltip"
                             class="absolute z-10 invisible inline-block text-sm text-grey-700 transition-opacity duration-300 bg-white border border-grey-200 rounded-md shadow-sm opacity-0 max-w-sm lg:max-w-3xl dark:bg-grey-800 dark:border-grey-600 dark:text-grey-200">
                            <div class="p-3">
                                <h3 class="font-bold">{{ __($document->format->label()) }}
                                    @if($document->fileExtension)(<i>{{$document->fileExtension->suffix}}</i>)@endif</h3>
                                <p>
                                    <x-link :href="$link">{{ $document->path }}</x-link>
                                </p>
                            </div>
                            <div data-popper-arrow></div>
                        </div>

                        <button data-popover-target="popover-license-{{$document->id}}" data-popover-placement="bottom-start" type="button">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-award text-purple-400" viewBox="0 0 16 16">
                                <path d="M9.669.864 8 0 6.331.864l-1.858.282-.842 1.68-1.337 1.32L2.6 6l-.306 1.854 1.337 1.32.842 1.68 1.858.282L8 12l1.669-.864 1.858-.282.842-1.68 1.337-1.32L13.4 6l.306-1.854-1.337-1.32-.842-1.68zm1.196 1.193.684 1.365 1.086 1.072L12.387 6l.248 1.506-1.086 1.072-.684 1.365-1.51.229L8 10.874l-1.355-.702-1.51-.229-.684-1.365-1.086-1.072L3.614 6l-.25-1.506 1.087-1.072.684-1.365 1.51-.229L8 1.126l1.356.702z"/>
                                <path d="M4 11.794V16l4-1 4 1v-4.206l-2.018.306L8 13.126 6.018 12.1z"/>
                            </svg>
                            <span class="sr-only">Show license</span>
                        </button>
                        <div data-popover id="popover-license-{{$document->id}}" role="tooltip"
                            class="absolute z-10 invisible inline-block text-sm text-grey-700 transition-opacity duration-300 bg-white border border-grey-200 rounded-md shadow-sm opacity-0 max-w-sm lg:max-w-3xl dark:bg-grey-800 dark:border-grey-600 dark:text-grey-200">
                            <div class="p-3">
                                <h3 class="font-bold">{{$document->license->label}} - {{ __($document->license->title) }}</h3>
                                <p>{{ __($document->license->description) }}</p>
                            </div>
                            <div data-popper-arrow></div>
                        </div>
                    @endif

                    @if ($showActivity)
                        <span class="text-md"><b>{{ __($document->activity->label()) }}</b> |</span>
                    @endif
                    <span class="text-md">{{ __($document->type->label()) }}</span>
                </div>
                @if ($document->title)
                    <span class="hidden md:inline">|</span>
                    <p class="text-sm md:truncate md:max-w-40 lg:max-w-sm"><i>{{ $document->title }}</i></p>
                @endif
                @if ($showOwner || $document->anonymous)
                    <div class="flex flex-row items-center justify-start">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                             class="bi bi-person-up md:hidden" viewBox="0 0 16 16">
                            <path
                                d="M12.5 16a3.5 3.5 0 1 0 0-7 3.5 3.5 0 0 0 0 7m.354-5.854 1.5 1.5a.5.5 0 0 1-.708.708L13 11.707V14.5a.5.5 0 0 1-1 0v-2.793l-.646.647a.5.5 0 0 1-.708-.708l1.5-1.5a.5.5 0 0 1 .708 0M11 5a3 3 0 1 1-6 0 3 3 0 0 1 6 0M8 7a2 2 0 1 0 0-4 2 2 0 0 0 0 4"/>
                            <path
                                d="M8.256 14a4.5 4.5 0 0 1-.229-1.004H3c.001-.246.154-.986.832-1.664C4.484 10.68 5.711 10 8 10q.39 0 .74.025c.226-.341.496-.65.804-.918Q8.844 9.002 8 9c-5 0-6 3-6 4s1 1 1 1z"/>
                        </svg>
                        <span
                            class="text-sm ms-2 md:ms-0 md:text-grey-500 md:dark:text-grey-300">
                            @if (!$document->anonymous)
                                {{ substr($document->user->firstname, 0, 1).". ".$document->user->lastname }}
                            @else
                                {{ __("Anonymous") }}
                            @endif
                        </span>
                    </div>
                @endif
            </div>
            @if($showDates)
                <div class="flex flex-row items-center justify-start ">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                         class="bi bi-calendar-check" viewBox="0 0 16 16">
                        <path
                            d="M10.854 7.146a.5.5 0 0 1 0 .708l-3 3a.5.5 0 0 1-.708 0l-1.5-1.5a.5.5 0 1 1 .708-.708L7.5 9.793l2.646-2.647a.5.5 0 0 1 .708 0"/>
                        <path
                            d="M3.5 0a.5.5 0 0 1 .5.5V1h8V.5a.5.5 0 0 1 1 0V1h1a2 2 0 0 1 2 2v11a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V3a2 2 0 0 1 2-2h1V.5a.5.5 0 0 1 .5-.5M1 4v10a1 1 0 0 0 1 1h12a1 1 0 0 0 1-1V4z"/>
                    </svg>
                    <span class="text-sm ms-2">
                        {{ __("Uploaded ").$document->created_at->diffForHumans()."." }}
                    </span>
                </div>
                <div class="flex flex-row items-center justify-start ">
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                         class="bi bi-floppy"
                         viewBox="0 0 16 16">
                        <path d="M11 2H9v3h2z"/>
                        <path
                            d="M1.5 0h11.586a1.5 1.5 0 0 1 1.06.44l1.415 1.414A1.5 1.5 0 0 1 16 2.914V14.5a1.5 1.5 0 0 1-1.5 1.5h-13A1.5 1.5 0 0 1 0 14.5v-13A1.5 1.5 0 0 1 1.5 0M1 1.5v13a.5.5 0 0 0 .5.5H2v-4.5A1.5 1.5 0 0 1 3.5 9h9a1.5 1.5 0 0 1 1.5 1.5V15h.5a.5.5 0 0 0 .5-.5V2.914a.5.5 0 0 0-.146-.353l-1.415-1.415A.5.5 0 0 0 13.086 1H13v4.5A1.5 1.5 0 0 1 11.5 7h-7A1.5 1.5 0 0 1 3 5.5V1H1.5a.5.5 0 0 0-.5.5m3 4a.5.5 0 0 0 .5.5h7a.5.5 0 0 0 .5-.5V1H4zM3 15h10v-4.5a.5.5 0 0 0-.5-.5h-9a.5.5 0 0 0-.5.5z"/>
                    </svg>
                    <span class="text-sm ms-2">
                        {{ __("Last updated ").$document->updated_at->diffForHumans()."." }}
                    </span>
                </div>
            @endif
        </div>
        <div id="document-{{$document->id}}-actions"
             class="w-full flex flex-row flex-wrap md:flex-nowrap gap-3 justify-start items-center md:justify-end md:w-auto">
            @if($document->format->value == FormatEnum::LINK->value)
                <x-buttons.icon-purple :title="__('Copy the link')"
                                       onclick="navigator.clipboard.writeText('{{$document->path}}');"
                                       data-popover-target="popover-link-copied-{{$document->id}}"
                                       data-popover-trigger="click">
                    <x-slot:icon>
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                             stroke="currentColor" class="w-5 h-5">
                            <path stroke-linecap="round" stroke-linejoin="round"
                                  d="M15.666 3.888A2.25 2.25 0 0 0 13.5 2.25h-3c-1.03 0-1.9.693-2.166 1.638m7.332 0c.055.194.084.4.084.612v0a.75.75 0 0 1-.75.75H9a.75.75 0 0 1-.75-.75v0c0-.212.03-.418.084-.612m7.332 0c.646.049 1.288.11 1.927.184 1.1.128 1.907 1.077 1.907 2.185V19.5a2.25 2.25 0 0 1-2.25 2.25H6.75A2.25 2.25 0 0 1 4.5 19.5V6.257c0-1.108.806-2.057 1.907-2.185a48.208 48.208 0 0 1 1.927-.184"/>
                        </svg>
                    </x-slot:icon>
                </x-buttons.icon-purple>
                <div data-popover id="popover-link-copied-{{$document->id}}" role="tooltip"
                     class="absolute z-10 invisible inline-block text-sm border border-grey-200 rounded-md shadow-sm opacity-0 px-3 py-2 bg-grey-100 dark:border-grey-600 dark:bg-grey-700">
                    <h3 class="font-semibold text-grey-900 dark:text-white">{{__("Link copied!")}}</h3>
                </div>
            @else
                <x-buttons.icon-purple :title="__('Download').' '.$document->path" :link="true" href="{{$link}}"
                                       download="{{ $document->path }}">
                    <x-slot:icon>
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                             stroke="currentColor" class="w-5 h-5">
                            <path stroke-linecap="round" stroke-linejoin="round"
                                  d="M3 16.5v2.25A2.25 2.25 0 0 0 5.25 21h13.5A2.25 2.25 0 0 0 21 18.75V16.5M16.5 12 12 16.5m0 0L7.5 12m4.5 4.5V3"/>
                        </svg>
                    </x-slot:icon>
                </x-buttons.icon-purple>
            @endif
            <x-buttons.icon-purple :title="__('Open').' '.$document->path" :link="true" href='{{$link}}'
                                   target='_blank'>
                <x-slot:icon>
                    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor"
                         class="bi bi-box-arrow-up-right" viewBox="0 0 16 16">
                        <path fill-rule="evenodd"
                              d="M8.636 3.5a.5.5 0 0 0-.5-.5H1.5A1.5 1.5 0 0 0 0 4.5v10A1.5 1.5 0 0 0 1.5 16h10a1.5 1.5 0 0 0 1.5-1.5V7.864a.5.5 0 0 0-1 0V14.5a.5.5 0 0 1-.5.5h-10a.5.5 0 0 1-.5-.5v-10a.5.5 0 0 1 .5-.5h6.636a.5.5 0 0 0 .5-.5"/>
                        <path fill-rule="evenodd"
                              d="M16 .5a.5.5 0 0 0-.5-.5h-5a.5.5 0 0 0 0 1h3.793L6.146 9.146a.5.5 0 1 0 .708.708L15 1.707V5.5a.5.5 0 0 0 1 0z"/>
                    </svg>
                </x-slot:icon>
            </x-buttons.icon-purple>
            @if($showAdminDocument)
                @if($document->format->value == FormatEnum::FILE->value)
                    <x-buttons.icon-purple :title="__('Edit')"
                                        data-modal-target="modal-edit-document"
                                        data-modal-toggle="modal-edit-document"
                                        onclick="setEditDocumentProperties('{{$document->id}}', '{{$document->course->code}}',
                                        '{{ $document->period->id . ',' .$document->period->getLabel() }}', '{{$document->activity}}', '{{$document->type}}', '{{$document->format}}',
                                        '{{$document->title}}', '{{$document->path}}', '{{$document->license->id}}', '{{$document->anonymous}}')">
                        <x-slot:icon>
                            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor"
                                class="bi bi-pencil-square" viewBox="0 0 16 16">
                                <path
                                    d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                <path fill-rule="evenodd"
                                    d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5z"/>
                            </svg>
                        </x-slot:icon>
                    </x-buttons.icon-purple>
                @else
                    <x-buttons.icon-purple :title="__('Edit')"
                                        data-modal-target="modal-edit-document"
                                        data-modal-toggle="modal-edit-document"
                                        onclick="setEditDocumentProperties('{{$document->id}}', '{{$document->course->code}}',
                                        '{{ $document->period->id . ',' .$document->period->getLabel() }}', '{{$document->activity}}', '{{$document->type}}', '{{$document->format}}',
                                        '{{$document->title}}', '{{$document->path}}', 'null', '{{$document->anonymous}}')">
                        <x-slot:icon>
                            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor"
                                class="bi bi-pencil-square" viewBox="0 0 16 16">
                                <path
                                    d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                <path fill-rule="evenodd"
                                    d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5z"/>
                            </svg>
                        </x-slot:icon>
                    </x-buttons.icon-purple>
                @endif
                <x-buttons.icon-danger :title="__('Delete')"
                                       data-modal-target="modal-confirm-deletion-doc"
                                       data-modal-toggle="modal-confirm-deletion-doc"
                                       onclick="setDeleteDocumentId('{{$document->id}}')">
                    <x-slot:icon>
                        <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor"
                             class="bi bi-trash"
                             viewBox="0 0 16 16">
                            <path
                                d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5m2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5m3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0z"/>
                            <path
                                d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4zM2.5 3h11V2h-11z"/>
                        </svg>
                    </x-slot:icon>
                </x-buttons.icon-danger>
            @endif
            <x-buttons.icon-danger :title="__('Report document')"
                                   data-modal-target="modal-add-doc-report"
                                   data-modal-toggle="modal-add-doc-report"
                                   onclick="setReportDocumentStoreModalProperties('{{$document->id}}', '{{$document->course->code}}', '{{$document->path}}', '{{$document->format}}')">
                <x-slot:icon>
                    <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor"
                         class="bi bi-exclamation-diamond" viewBox="0 0 16 16">
                        <path
                            d="M6.95.435c.58-.58 1.52-.58 2.1 0l6.515 6.516c.58.58.58 1.519 0 2.098L9.05 15.565c-.58.58-1.519.58-2.098 0L.435 9.05a1.48 1.48 0 0 1 0-2.098zm1.4.7a.495.495 0 0 0-.7 0L1.134 7.65a.495.495 0 0 0 0 .7l6.516 6.516a.495.495 0 0 0 .7 0l6.516-6.516a.495.495 0 0 0 0-.7L8.35 1.134z"/>
                        <path
                            d="M7.002 11a1 1 0 1 1 2 0 1 1 0 0 1-2 0M7.1 4.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0z"/>
                    </svg>
                </x-slot:icon>
            </x-buttons.icon-danger>
        </div>
    </div>
</li>
