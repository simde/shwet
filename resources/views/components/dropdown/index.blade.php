@props(['id', 'dropdownId'])

<button id="{{$id}}" data-dropdown-toggle="{{$dropdownId}}"
        class="inline-flex items-center gap-2 text-grey-950 bg-purple-400 focus:outline-none hover:brightness-90 rounded-lg text-sm font-medium px-3 py-2"
        type="button" title="{{$title}}">
    {{ $icon }}
    <span class="hidden sm:inline">{{ $title }}</span>
    <svg class="w-2.5 h-2.5" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none"
         viewBox="0 0 10 6">
        <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
              d="m1 1 4 4 4-4"/>
    </svg>
</button>
<div id="{{$dropdownId}}"
     class="z-10 hidden bg-white divide-y divide-grey-100 rounded-md shadow dark:bg-grey-700 dark:divide-grey-600">
    <ul class="max-h-56 p-1.5 overflow-y-auto space-y-1 text-sm text-grey-700 dark:text-grey-200">
        {{$dropdownItems}}
    </ul>
</div>
