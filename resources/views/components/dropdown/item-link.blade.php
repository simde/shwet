@props(['href' => '#'])
<li>
    <a href="{{$href}}"
       class="flex justify-start items-center p-2 rounded-md hover:bg-grey-100 dark:hover:bg-grey-600 w-full text-sm font-medium text-grey-900 dark:text-grey-50"
        {{$attributes}}>
        {{ $slot }}
    </a>
</li>
