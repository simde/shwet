@props(['inputId', 'name', 'value', 'checked' => false])

<li>
    <div class="flex items-center p-2 rounded hover:bg-grey-100 dark:hover:bg-grey-600">
        <input id="{{$inputId}}" type="checkbox" name="{{$name}}" value="{{$value}}"
               class="w-4 h-4 text-purple-400 bg-grey-50 border-grey-300 rounded focus:ring-purple-400 focus:ring-2"
            {{ $checked ? 'checked' : '' }}/>
        <label for="{{$inputId}}"
               class="w-full ms-2 text-sm font-medium text-grey-900 dark:text-grey-50 rounded">{{ $label }}</label>
    </div>
</li>
