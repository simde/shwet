@props([
    'outline' => false,
    'type' => 'button',
    'responsive' => false,
])


@php
    $classes = "focus:ring-2 focus:outline-none focus:outline-none font-medium rounded-lg text-sm px-2.5 py-2 text-center inline-flex items-center";
    $classes .= $outline ?
        " text-red-400 border-2 border-red-400 hover:bg-red-400 hover:text-grey-950 focus:ring-red-500 dark:focus:ring-red-300" :
        " text-gray-950 bg-red-500 hover:brightness-90 focus:ring-red-500 dark:bg-red-400 dark:focus:ring-red-300";
@endphp

<button type="{{$type}}" {{ $attributes->class($classes) }} {{$attributes}} title="{{$text}}">
    {{$icon}}
    <span class="{{ $responsive ? 'hidden sm:inline' : '' }} ms-2">{{$text}}</span>
</button>
