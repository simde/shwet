@props([
    'link' => false,
    'outline' => false,
    'type' => 'button',
    'responsive' => false,
])


@php
    $classes = "focus:ring-2 focus:outline-none focus:outline-none font-medium rounded-lg text-sm px-2.5 py-2 text-center inline-flex items-center";
    $classes .= $outline ?
        " text-purple-400 border border-2 border-purple-400 hover:bg-purple-400 hover:text-grey-950 focus:ring-purple-500 dark:focus:ring-purple-300" :
        " text-grey-950 bg-purple-400 hover:brightness-90 focus:ring-purple-500 dark:bg-purple-400 dark:focus:ring-purple-300";
@endphp



@if(!$link)
    <button type="{{$type}}" {{ $attributes->class($classes) }} {{$attributes}} title="{{$text}}">
        <span class="{{ $responsive ? 'hidden sm:inline' : '' }}">{{$text}}</span>
    </button>
@else
    <a class="{{$classes}}" {{ $attributes->class($classes) }} title="{{$text}}">
        {{$text}}
    </a>
@endif
