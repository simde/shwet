@php
    use App\Enums\ReportStatus as ReportStatusEnum;
@endphp
@props(['status'])
<div>
    @switch ($status)
        @case(ReportStatusEnum::PENDING->value)
            <span class="bg-yellow-200 text-yellow-800 text-sm font-medium me-2 px-2.5 py-1 rounded-full dark:bg-yellow-900 dark:text-yellow-300">
                {{__(ReportStatusEnum::PENDING->label())}}
            </span>
            @break
        @case(ReportStatusEnum::PROCESSED->value)
            <span class="bg-green-200 text-green-800 text-sm font-medium me-2 px-2.5 py-1 rounded-full dark:bg-green-900 dark:text-green-100">
                {{__(ReportStatusEnum::PROCESSED->label())}}
            </span>
            @break
        @case (ReportStatusEnum::REJECTED->value)
            <span class="bg-red-300 text-red-900 text-sm font-medium me-2 px-2.5 py-1 rounded-full dark:bg-red-800 dark:text-red-200">
                {{__(ReportStatusEnum::REJECTED->label())}}
            </span>
            @break
        @default
            <span class="bg-purple-200 text-purple-800 text-sm font-medium me-2 px-2.5 py-1 rounded-full dark:bg-purple-900 dark:text-purple-300">No status</span>
    @endswitch
</div>
