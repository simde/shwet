{{--Only displayed if the user has favorite courses--}}
@if(!empty(auth()->user()->courses))
<div class="flex flex-row justify-start items-center gap-4 overflow-x-auto pb-1">
    @foreach(auth()->user()->courses as $course)
        <x-favourite-courses.item :code="$course->code"/>
    @endforeach
</div>
@endif
