@props(['code'])

@php
    $active = request()->is('courses'. "/$code");
    //$classes = 'inline-flex justify-center items-center p-2 bg-grey-200 rounded-md hover:bg-purple-400 dark:bg-grey-800 dark:hover:bg-purple-400 dark:hover:text-grey-950';
    $classes = 'inline-flex justify-center items-center gap-1 p-2 rounded-md';

    $classes .= ($active ?? false) ? " bg-purple-400 dark:bg-purple-400" : " bg-grey-200 dark:bg-grey-800";
@endphp

<div {{ $attributes->class($classes) }} {{$attributes}}>
    <a href="{{ route('courses.show', [$code]) }}" class="flex justify-center items-center {{$active ? '' : 'hover:text-purple-400 text-grey-950 dark:text-grey-50 dark:hover:text-purple-400'}}">
        <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" fill="currentColor" class="bi bi-star-fill" viewBox="0 0 16 16">
            <path d="M3.612 15.443c-.386.198-.824-.149-.746-.592l.83-4.73L.173 6.765c-.329-.314-.158-.888.283-.95l4.898-.696L7.538.792c.197-.39.73-.39.927 0l2.184 4.327 4.898.696c.441.062.612.636.282.95l-3.522 3.356.83 4.73c.078.443-.36.79-.746.592L8 13.187l-4.389 2.256z"/>
        </svg>
        <span class="ms-1 font-bold text-sm">{{$code}}</span>
    </a>
    <form action="{{ route('remove-favourite-course', [$code]) }}" method="POST">
        @csrf
        <button  type="submit"
                 class="inline-flex items-center justify-center h-6 w-6 rounded-lg {{$active ? 'hover:text-grey-950 hover:bg-grey-200 dark:hover:text-grey-50 dark:hover:bg-grey-700' : 'text-grey-400 hover:text-grey-950 hover:bg-grey-50 dark:text-grey-400 dark:hover:text-grey-50 dark:bg-grey-800 dark:hover:bg-grey-600' }}"
                 aria-label="Close" title="{{__('Remove from favourites')}}">
            <span class="sr-only">Remove</span>
            <svg class="w-2.5 h-2.5" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 14 14">
                <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                      d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6"/>
            </svg>
        </button>
    </form>
</div>

