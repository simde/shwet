@props(['dataDropdownToggle'])

<div class="flex items-center">
    <button type="button" data-dropdown-toggle="{{$dataDropdownToggle}}"
            class="inline-flex items-center lg:justify-center w-full px-3 py-2 text-grey-950 dark:text-grey-50 hover:bg-grey-200 lg:hover:bg-grey-50 lg:dark:text-grey-950 rounded-lg cursor-pointer lg:mt-0 mt-2 dark:hover:bg-grey-800 lg:dark:hover:bg-grey-800 dark:hover:text-grey-50">
        {{$icon}}
        <span class="ms-1">{{$title}}</span>
        <svg class="ms-1.5 w-2.5 h-2.5" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 10 6">
            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m1 1 4 4 4-4"></path>
        </svg>
    </button>
    <!-- Dropdown -->
    <div
        class="z-40 hidden w-1/2 lg:w-auto my-4 text-base list-none bg-grey-50 divide-y divide-grey-200 rounded-lg shadow dark:bg-grey-800 max-h-56 overflow-y-auto"
        id="{{$dataDropdownToggle}}">
        {{$itemGroups}}
    </div>
</div>
