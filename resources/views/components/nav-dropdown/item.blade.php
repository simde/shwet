@props(['href' => "#", 'active' => false])

@php
    $classes = ($active ?? false)
                ? 'block px-4 py-2 text-sm text-purple-400 hover:bg-grey-100 dark:hover:bg-grey-600 dark:text-purple-400 dark:hover:text-grey-50'
                : 'block px-4 py-2 text-sm text-grey-800 hover:bg-grey-100 dark:hover:bg-grey-600 dark:text-grey-200 dark:hover:text-grey-50';
@endphp

<li>
    <a href="{{$href}}" {{ $attributes->class($classes) }} {{$attributes}}>
        {{ $slot }}
    </a>
</li>
