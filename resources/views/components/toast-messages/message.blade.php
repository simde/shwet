@props(['message', 'index'])

@php
    use App\Enums\ToastMessage as ToastMessageEnum;

    $color = match ($message['type']) {
        ToastMessageEnum::SUCCESS => 'green',
        ToastMessageEnum::ERROR => 'red',
        ToastMessageEnum::WARNING => 'orange',
        ToastMessageEnum::INFO => 'purple',
        default => 'purple',
    };
@endphp
<div id="toast-message-{{$index}}"
     class="flex items-center justify-between w-3/4 sm:w-full p-4 text-grey-600 bg-white rounded-lg shadow-xl dark:text-grey-200 dark:bg-grey-900 border-2 border-{{$color}}-500 dark:border-{{$color}}-500 toast-{{$message['type']}}"
     role="alert">
    @switch($message['type'])
        @case(ToastMessageEnum::SUCCESS)
            <div
                class="inline-flex items-center justify-center flex-shrink-0 w-8 h-8 text-green-500 bg-green-100 rounded-lg dark:bg-green-800 dark:text-green-200">
                <svg class="w-5 h-5" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="currentColor"
                     viewBox="0 0 20 20">
                    <path
                        d="M10 .5a9.5 9.5 0 1 0 9.5 9.5A9.51 9.51 0 0 0 10 .5Zm3.707 8.207-4 4a1 1 0 0 1-1.414 0l-2-2a1 1 0 0 1 1.414-1.414L9 10.586l3.293-3.293a1 1 0 0 1 1.414 1.414Z"/>
                </svg>
                <span class="sr-only">Check icon {{ToastMessageEnum::SUCCESS}}</span>
            </div>
            @break
        @case(ToastMessageEnum::ERROR)
            <div
                class="inline-flex items-center justify-center flex-shrink-0 w-8 h-8 text-red-500 bg-red-100 rounded-lg dark:bg-red-800 dark:text-red-200">
                <svg class="w-5 h-5" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="currentColor"
                     viewBox="0 0 20 20">
                    <path
                        d="M10 .5a9.5 9.5 0 1 0 9.5 9.5A9.51 9.51 0 0 0 10 .5Zm3.707 11.793a1 1 0 1 1-1.414 1.414L10 11.414l-2.293 2.293a1 1 0 0 1-1.414-1.414L8.586 10 6.293 7.707a1 1 0 0 1 1.414-1.414L10 8.586l2.293-2.293a1 1 0 0 1 1.414 1.414L11.414 10l2.293 2.293Z"/>
                </svg>
                <span class="sr-only">Error icon</span>
            </div>
            @break
        @case(ToastMessageEnum::WARNING)
            <div
                class="inline-flex items-center justify-center flex-shrink-0 w-8 h-8 text-orange-500 bg-orange-100 rounded-lg dark:bg-orange-700 dark:text-orange-200">
                <svg class="w-5 h-5" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="currentColor"
                     viewBox="0 0 20 20">
                    <path
                        d="M10 .5a9.5 9.5 0 1 0 9.5 9.5A9.51 9.51 0 0 0 10 .5ZM10 15a1 1 0 1 1 0-2 1 1 0 0 1 0 2Zm1-4a1 1 0 0 1-2 0V6a1 1 0 0 1 2 0v5Z"/>
                </svg>
                <span class="sr-only">Warning icon</span>
            </div>
            @break
        @case(ToastMessageEnum::INFO)
            <div
                class="inline-flex items-center justify-center flex-shrink-0 w-8 h-8 text-purple-400 bg-purple-100 rounded-lg dark:bg-purple-700 dark:text-purple-200">
                <svg class="w-5 h-5" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                     fill="currentColor" viewBox="0 0 24 24">
                    <path fill-rule="evenodd"
                          d="M2 12C2 6.477 6.477 2 12 2s10 4.477 10 10-4.477 10-10 10S2 17.523 2 12Zm9.408-5.5a1 1 0 1 0 0 2h.01a1 1 0 1 0 0-2h-.01ZM10 10a1 1 0 1 0 0 2h1v3h-1a1 1 0 1 0 0 2h4a1 1 0 1 0 0-2h-1v-4a1 1 0 0 0-1-1h-2Z"
                          clip-rule="evenodd"/>
                </svg>
                <span class="sr-only">Info icon</span>
            </div>
            @break
        @default
            <div
                class="inline-flex items-center justify-center flex-shrink-0 w-8 h-8 text-purple-500 bg-purple-100 rounded-lg dark:bg-purple-700 dark:text-purple-200">
                <svg class="w-6 h-6" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="20" height="20"
                     fill="currentColor" viewBox="0 0 24 24">
                    <path fill-rule="evenodd"
                          d="M3 6a2 2 0 0 1 2-2h14a2 2 0 0 1 2 2v9a2 2 0 0 1-2 2h-6.616l-2.88 2.592C8.537 20.461 7 19.776 7 18.477V17H5a2 2 0 0 1-2-2V6Zm4 2a1 1 0 0 0 0 2h5a1 1 0 1 0 0-2H7Zm8 0a1 1 0 1 0 0 2h2a1 1 0 1 0 0-2h-2Zm-8 3a1 1 0 1 0 0 2h2a1 1 0 1 0 0-2H7Zm5 0a1 1 0 1 0 0 2h5a1 1 0 1 0 0-2h-5Z"
                          clip-rule="evenodd"/>
                </svg>
                <span class="sr-only">Message icon</span>
            </div>
    @endswitch
    <div class="ms-3 text-sm font-normal me-2 text-{{$color}}-500 w-full">{{ __($message['message']) }}</div>
    <button type="button"
            class="bg-white text-grey-400 hover:text-grey-900 rounded-lg focus:ring-2 focus:ring-grey-300 p-1.5 hover:bg-grey-100 inline-flex items-center justify-center h-8 w-8 dark:text-grey-500 dark:hover:text-grey-50 dark:bg-grey-800 dark:hover:bg-grey-700"
            data-dismiss-target="#toast-message-{{$index}}" aria-label="Close" title="{{__("Close")}}">
        <span class="sr-only">Close</span>
        <svg class="w-3 h-3 m-1" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 14 14">
            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                  d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6"/>
        </svg>
    </button>
</div>


