@if (session()->has('messages') && !empty(session('messages')))
    <div id="toast-messages"
         class="z-20 fixed block bottom-2 w-full sm:max-w-sm sm:top-20 sm:right-5 sm:bottom-auto">
        <div id="toast-messages-list" class="flex flex-col gap-y-4 items-center">
            @foreach (array_reverse(session('messages')) as $message)
                <x-toast-messages.message :message="$message" :index="$loop->index"/>
            @endforeach
        </div>
    </div>
    <script>
        const removeToasts = (className) => {
            Array.from(document.getElementsByClassName(className)).forEach((element) => {
                setTimeout(() => {
                    element.remove();
                }, 5000);
            });
        };

        removeToasts('toast-success');
        removeToasts('toast-info');
    </script>
@endif
