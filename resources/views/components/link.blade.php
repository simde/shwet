@props(['href'])

<a href="{{$href}}" class="text-purple-400 font-medium hover:underline hover:underline-offset-2" target="_blank" {{$attributes}}>{{$slot}}</a>
