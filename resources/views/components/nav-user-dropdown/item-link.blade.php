@props(['href' => '#', 'id', 'target' => ''])

<li>
    <a href="{{$href}}" id="{{$id}}" target="{{$target}}"
       class="block px-4 py-2 text-sm text-grey-700 hover:bg-grey-100 dark:hover:bg-grey-600 dark:text-grey-200 dark:hover:text-white">
        <div class="inline-flex items-center">
            {{$icon}}
            <span class="ms-2">{{$title}}</span>
        </div>
    </a>
</li>
