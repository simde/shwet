<li>
    <div
        class="flex items-center px-4 py-2 text-sm text-grey-700 hover:bg-grey-100 dark:hover:bg-grey-600 dark:text-grey-200 dark:hover:text-white">
        {{$slot}}
    </div>
</li>
