@props(['id'])
<div class="px-5">
    <div id="accordion-help-{{$id}}" data-accordion="collapse" data-active-classes="text-grey-900 dark:text-grey-50" data-inactive-classes="text-grey-500 dark:text-grey-200">
        <h2 id="accordion-help-heading-{{$id}}">
            <button type="button" class="flex items-center justify-between w-full py-2 font-medium rtl:text-right text-grey-500 border-b border-grey-300 dark:border-grey-600 dark:text-grey-300 gap-3" data-accordion-target="#accordion-help-body-{{$id}}" aria-expanded="false" aria-controls="accordion-help-body-{{$id}}">
                <span class="flex items-center">
                    <svg class="w-5 h-5 me-2 shrink-0" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-8-3a1 1 0 00-.867.5 1 1 0 11-1.731-1A3 3 0 0113 8a3.001 3.001 0 01-2 2.83V11a1 1 0 11-2 0v-1a1 1 0 011-1 1 1 0 100-2zm0 8a1 1 0 100-2 1 1 0 000 2z" clip-rule="evenodd">

                        </path>
                    </svg>
                    {{__("How to publish a document?")}}
                </span>
                <svg data-accordion-icon class="w-3 h-3 rotate-180 shrink-0" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 10 6">
                    <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 5 5 1 1 5"/>
                </svg>
            </button>
        </h2>
        <div id="accordion-help-body-{{$id}}" class="hidden" aria-labelledby="accordion-help-heading-{{$id}}">
            <div class="py-2 text-sm">
                <ul>
                    <li>
                        <i>{{ __("The maximum file size that can be added is 10MB.") }}</i>
                    </li>
                    <li>
                        <b class="text-purple-400">UV :</b> {{__("The code of the course you want to publish the document for.")}}
                    </li>
                    <li>
                        <b class="text-purple-400">{{ucfirst(__("semester"))}} :</b> {{__("The semester during which you got or made this document.")}}
                    </li>
                    <li>
                        <b class="text-purple-400">{{__("Type of activity")}} :</b> {{__("The kind of activity your document is linked to (e.g. exam, project, lab report, etc.).")}}
                    </li>
                    <li>
                        <b class="text-purple-400">{{__("Type of document")}} :</b> {{__("Whether your document is a correction, a subject or made up by yourself (Personal).")}}
                    </li>
                    <li>
                        <b class="text-purple-400">{{__("Format")}} :</b> {{__("Whether your document is a file (PDF, Powerpoint, Word, ...) or a link to an online document (Google Doc, ...).")}}
                    </li>
                    <li>
                        <b class="text-purple-400">{{__("Title")}} :</b> {{__("(OPTIONAL) Give your document a name if you think it might be necessary.")}}
                    </li>
                    @if($id == 'add')
                    <li>
                        <b class="text-purple-400">{{__("License")}} :</b> {{__("CC0 (corresponding to the public domain) by default, indicates the document property and conditions of its usage. (Only applies to files, not links)")}}
                    </li>
                    @endif
                    <li>
                        <b class="text-purple-400">{{__("Anonymous")}} :</b> {{__("If checked, nobody will know who published the document. Else, your name will be displayed.")}}
                    </li>
                </ul>
            </div>
        </div>
    </div>

</div>

