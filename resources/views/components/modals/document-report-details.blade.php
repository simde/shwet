@props(['report'])

@php
    use App\Enums\ReportStatus as ReportStatusEnum;
    $document = $report->document;
@endphp

<div id="modal-doc-report-{{$report->id}}" tabindex="-1"
     class="hidden overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0 z-50 justify-center items-center w-full md:inset-0 h-[calc(100%-1rem)] max-h-full">
    <div class="relative p-4 w-full max-w-xl max-h-full">
        <form autocomplete="off" action="{{ route('document-reports.update', ['id' => $report->id]) }}" method="POST">
            @csrf
            @method('PATCH')

            <div class="relative bg-grey-100 rounded-lg shadow dark:bg-grey-800">
                <div
                    class="flex items-center justify-between px-5 py-4 border-b rounded-t border-grey-200 dark:border-grey-600">
                    <div class="w-full flex flex-col justify-start items-center sm:flex-row gap-3">
                        <h3 class="text-lg font-semibold text-grey-900 dark:text-white">
                            {{ __('Report details') }}
                        </h3>

                        @if(auth()->user()->isAdmin())
                            <select
                                id="select-status-{{$report->id}}"
                                name="status"
                                class="rounded-md focus:outline-none focus:ring-purple-400 focus:ring-2 dark:bg-grey-600 focus:border-purple-400">
                                <option
                                    value="{{ReportStatusEnum::PENDING->value}}" {{$report->status==ReportStatusEnum::PENDING->value ? "selected": ""}}>
                                    {{__(ReportStatusEnum::PENDING->label())}}
                                </option>
                                <option
                                    value="{{ReportStatusEnum::PROCESSED->value}}" {{$report->status==ReportStatusEnum::PROCESSED->value ? 'selected=selected': ""}}>
                                    {{__(ReportStatusEnum::PROCESSED->label())}}
                                </option>
                                <option
                                    value="{{ReportStatusEnum::REJECTED->value}}" {{$report->status==ReportStatusEnum::REJECTED->value ? "selected": ""}}>
                                    {{__(ReportStatusEnum::REJECTED->label())}}
                                </option>
                            </select>
                        @else
                            <x-status-badge :status="$report->status"/>
                        @endif
                    </div>


                    <!-- Close button -->
                    <button type="button"
                            class="text-grey-400 bg-transparent hover:bg-grey-200 hover:text-grey-900 rounded-md text-sm w-8 h-8 ms-auto inline-flex justify-center items-center dark:hover:bg-grey-600 dark:hover:text-white"
                            data-modal-hide="modal-doc-report-{{$report->id}}">
                        <svg class="w-3 h-3" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none"
                             viewBox="0 0 14 14">
                            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                  d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6"/>
                        </svg>
                        <span class="sr-only">{{ __('Close modal') }}</span>
                    </button>
                </div>
                <div class="p-4 md:p-5 flex flex-col gap-5">
                    <div>
                        <div>
                            <span class="text-grey-500 dark:text-gray-400">[{{$report->commented_at}}]</span>
                            <span
                                class="text-nowrap"> {{auth()->id() === $report->user_id ? __("You reported") : $report->user->getFullName() . __(" reported")}} :</span>
                        </div>
                        @if($report->user->id !== auth()->id() || $report->status !== ReportStatusEnum::PENDING->value)
                            <div class="mt-1 w-full bg-grey-200 dark:bg-grey-600 rounded-md p-3"
                                 id="report-comment">{{$report->comment}}</div>
                        @else
                            <textarea id="report-comment-{{$report->id}}"
                                      name="comment"
                                      class="mt-1 w-full bg-grey-200 dark:bg-grey-600 rounded-md p-2 placeholder:text-grey-500 dark:placeholder:text-grey-400 focus:outline-none focus:ring-2 focus:ring-purple-400 focus:border-transparent disabled:text-grey-500 disabled:dark:text-grey-400"
                                      placeholder="{{__("Type here your report")}}" maxlength="800"
                                      onkeyup="checkLength('report-comment-{{$report->id}}', 800, 'report-comment-length-{{$report->id}}')">{{$report->comment}}</textarea>
                            <div class="flex justify-end items-top">
                                <span class="text-sm text-grey-500" id="report-comment-length-{{$report->id}}">{{strlen($report->comment)}}/800</span>
                            </div>
                        @endif
                    </div>
                    <div class="flex flex-col gap-1">
                        @if($document)
                            <div>
                                <span> {{__("About document")}} :</span>
                            </div>
                            <div class="w-full bg-grey-200 dark:bg-grey-600 rounded-md p-3" id="report-document">
                                @if($document->isFile())
                                    <x-link
                                        :href="'/courses/'.$document->course->code.'/'.$document->path">{{$document->path}}</x-link>
                                @elseif($document->isLink())
                                    <x-link :href="$document->path">{{$document->path}}</x-link>
                                @endif
                            </div>
                            @if(auth()->user()->isAdmin())
                                <div class="mt-1 flex flex-row gap-2 flex-wrap">
                                    <x-buttons.icon-text-purple :outline="true"
                                                                data-modal-target="modal-edit-document"
                                                                data-modal-toggle="modal-edit-document"
                                                                onclick="setEditDocumentProperties('{{$document->id}}','{{$document->course->code}}',
                                           '{{ $document->period->id . ',' .$document->period->getLabel() }}','{{$document->activity}}','{{$document->type}}','{{$document->format}}',
                                           '{{$document->title}}','{{$document->path}}', '{{$document->anonymous}}')">
                                        <x-slot:icon>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18"
                                                 fill="currentColor"
                                                 class="bi bi-pencil-square" viewBox="0 0 16 16">
                                                <path
                                                    d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                                <path fill-rule="evenodd"
                                                      d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5z"/>
                                            </svg>
                                        </x-slot:icon>
                                        <x-slot:text>{{__('Edit')}}</x-slot:text>
                                    </x-buttons.icon-text-purple>
                                    <x-buttons.icon-text-danger data-modal-target="modal-confirm-deletion-doc"
                                                                data-modal-toggle="modal-confirm-deletion-doc"
                                                                onclick="setDeleteDocumentId('{{$document->id}}')"
                                                                :outline="true">
                                        <x-slot:icon>
                                            <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18"
                                                 fill="currentColor" class="bi bi-trash"
                                                 viewBox="0 0 16 16">
                                                <path
                                                    d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5m2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5m3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0z"/>
                                                <path
                                                    d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1zM4.118 4 4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4zM2.5 3h11V2h-11z"/>
                                            </svg>
                                        </x-slot:icon>
                                        <x-slot:text>
                                            {{__("Delete")}}
                                        </x-slot:text>
                                    </x-buttons.icon-text-danger>
                                </div>
                            @endif
                        @else
                            <div class="bg-grey-100 dark:bg-grey-600 rounded-md border-2 border-red-400 dark:border-red-500 p-3">
                                <span class="flex flex-row items-center gap-1">
                                    <b class="text-red-500 dark:text-red-400">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-exclamation-triangle" viewBox="0 0 16 16">
                                          <path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.15.15 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.2.2 0 0 1-.054.06.1.1 0 0 1-.066.017H1.146a.1.1 0 0 1-.066-.017.2.2 0 0 1-.054-.06.18.18 0 0 1 .002-.183L7.884 2.073a.15.15 0 0 1 .054-.057m1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767z"/>
                                          <path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0M7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0z"/>
                                        </svg>
                                    </b>
                                    {{__("The document doesn't exist anymore, it may have been deleted.")}}
                                </span>
                            </div>
                        @endif
                    </div>
                    @if(auth()->user()->isAdmin())
                        <div class="mt-3">
                            <div>
                                <span> {{__("Answer to ").$report->user->getFullName()}} :</span>
                            </div>
                            <textarea
                                id="report-answer-{{$report->id}}"
                                name="response"
                                class="mt-1 w-full bg-grey-200 dark:bg-grey-600 rounded-md p-2 placeholder:text-grey-500 dark:placeholder:text-grey-400 focus:outline-none focus:ring-2 focus:ring-purple-400 focus:border-transparent"
                                placeholder="{{__("Type here your answer")}}"
                                onkeyup="checkLength('report-answer-{{$report->id}}', 800, 'report-answer-length-{{$report->id}}')">{{$report->response}}</textarea>
                            <div class="flex justify-end items-top">
                                <span class="text-sm text-grey-500" id="report-answer-length-{{$report->id}}">{{strlen($report->response)}}/800</span>
                            </div>
                        </div>
                    @else
                        @if($report->response)
                            <div>
                                <div>
                                <span class="text-grey-500 dark:text-gray-400">[{{$report->responded_at}}]</span>
                                <span
                                    class="text-nowrap"> {{ __("Admin answered")}} :</span>
                            </div>
                            <div class="mt-1 w-full bg-grey-200 dark:bg-grey-600 rounded-md p-3"
                                 id="report-comment">{{$report->comment}}</div>
                            </div>
                        @endif
                    @endif
                    <div class="flex justify-center">
                        <x-buttons.text-purple :type="'submit'">
                            <x-slot:text>{{__('Save changes')}}</x-slot:text>
                        </x-buttons.text-purple>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<script>
    document.getElementById("select-status-{{$report->id}}").addEventListener("change", function () {
        if(this.value === '{{ReportStatusEnum::REJECTED->value}}'){
            document.getElementById("report-answer-{{$report->id}}").required = true;
            let commentTextarea = document.getElementById("report-comment-{{$report->id}}");
            commentTextarea ? commentTextarea.disabled = true : null;
        }
        else {
            document.getElementById("report-answer-{{$report->id}}").required = false;
        }
    });
</script>
