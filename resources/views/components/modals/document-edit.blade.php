@php
    use App\Models\Course;
    use App\Models\License;
    use App\Models\Period;
    use App\Models\FileExtension;
    use App\Enums\DocumentType as DocumentTypeEnum;
    use App\Enums\Activity as ActivityEnum;
    use App\Enums\Format as FormatEnum;
@endphp
<div id="modal-edit-document" tabindex="-1" aria-hidden="true" class="text-grey-950 dark:text-grey-50 hidden overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0 z-50 justify-center items-center w-full md:inset-0 h-[calc(100%-1rem)] max-h-full">
    <div class="relative p-4 max-h-full">
        <div class="relative bg-white rounded-md shadow dark:bg-grey-700">
            <div class="flex items-center justify-between p-4 md:p-5 border-b rounded-t dark:border-grey-600">
                <h3 class="text-lg font-semibold text-grey-900 dark:text-white">
                    {{ __('Edit the document') }}
                </h3>

                <!-- Close button -->
                <button type="button" class="text-grey-400 bg-transparent hover:bg-grey-200 hover:text-grey-900 rounded-md text-sm w-8 h-8 ms-auto inline-flex justify-center items-center dark:hover:bg-grey-600 dark:hover:text-white" data-modal-hide="modal-edit-document">
                    <svg class="w-3 h-3" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 14 14">
                        <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6"/>
                    </svg>
                    <span class="sr-only">{{ __('Close modal') }}</span>
                </button>
            </div>

            <!-- Form -->
            <form id="document-form-edit" class="flex flex-wrap items-center justify-around gap-8 p-4 max-w-md" method="POST"
                  action="{{ route('documents.edit') }}" enctype="multipart/form-data">

                @csrf

                <input type="hidden" name="id" id="id-edit" value="" />

                @php
                    $seasonOrder = ['H', 'P', 'E', 'A'];
                    $courses = Course::all();
                    $periods = Period::all();
                    $types = DocumentTypeEnum::cases();
                    $activities = ActivityEnum::allowedActivities();
                    $formats = FormatEnum::cases();
                    $allowedExtensions = FileExtension::onlyActive()->pluck('suffix')->toArray();
                    $licenses = License::onlyActive()->get();
                @endphp

                <select id="course-edit" name="course"
                        class="w-48 text-grey-950 dark:text-grey-50 bg-grey-200 dark:bg-grey-800 border border-grey-300 dark:border-grey-600 rounded-md focus:ring-2 focus:ring-purple-400 focus:border-purple-400 select2"
                        required>
                    <option value="" class="hidden" class="w-4 h-4" selected disabled>{{ __('UV') }}</option>
                    @foreach($courses->sortBy('code') as $course)
                        <option value="{{ $course->code }}">{{ $course->code }}</option>
                    @endforeach
                </select>
                <select id="period-edit" name="period"
                        class="w-48 text-grey-950 dark:text-grey-50 bg-grey-200 dark:bg-grey-800 border border-grey-300 dark:border-grey-600 rounded-md focus:ring-2 focus:ring-purple-400 focus:border-purple-400 select2"
                        required>
                    <option value="" class="hidden" selected disabled>{{ ucfirst(__('semester')) }}</option>
                    @foreach($periods->sortByDesc(function ($period) use ($seasonOrder) {
                        $year = $period->year;
                        $season = $period->season->code->value;
                        return [$year, array_search($season, $seasonOrder)];
                    }) as $period)
                        <option value="{{ $period->id . ',' . $period->getLabel() }}">{{ $period->getLabel() }}</option>
                    @endforeach
                </select>
                <select id="activity-edit" name="activity"
                        class="w-48 text-grey-950 dark:text-grey-50 bg-grey-200 dark:bg-grey-800 border border-grey-300 dark:border-grey-600 rounded-md focus:ring-2 focus:ring-purple-400 focus:border-purple-400 invalid:border-red-500 invalid:border-2"
                        required>
                    <option value="" class="hidden" selected disabled>{{ __('Type of activity') }}</option>
                    @foreach($activities as $activity)
                        <option value="{{ $activity->value }}">{{ __($activity->label()) }}</option>
                    @endforeach
                </select>
                <select id="type-edit" name="type"
                        class="w-48 text-grey-950 dark:text-grey-50 bg-grey-200 dark:bg-grey-800 border border-grey-300 dark:border-grey-600 rounded-md focus:ring-2 focus:ring-purple-400 focus:border-purple-400"
                        required>
                    <option value="" class="hidden" selected disabled>{{ __('Type of document') }}</option>
                    @foreach($types as $type)
                        <option value="{{ $type->value }}">{{ __($type->label()) }}</option>
                    @endforeach
                </select>
                <select id="format-edit" name="format"
                        class="w-48 text-grey-950 dark:text-grey-50 bg-grey-200 dark:bg-grey-800 border border-grey-300 dark:border-grey-600 rounded-md focus:ring-2 focus:ring-purple-400 focus:border-purple-400"
                        required>
                    <option value="" class="hidden" selected disabled>{{ __('Format') }}</option>
                    @foreach($formats as $format)
                        <option value="{{ $format->value }}">{{ __($format->label()) }}</option>
                    @endforeach
                </select>
                <div class="w-48 relative">
                    <input type="text" id="title-edit" name="title"
                           class="w-full pr-8 text-grey-950 dark:text-grey-50 bg-grey-200 dark:bg-grey-800 border border-grey-300 dark:border-grey-600 rounded-md focus:ring-2 focus:ring-purple-400 focus:border-purple-400 dark:placeholder-gray-400"
                           placeholder="{{ __('Title') }}" />
                    <span 
                        title="{{ __('Please only add a title if the information composing it has not been entered above (example: title of a report)!') }}"
                        class="w-6 h-6 absolute right-2 top-1/2 transform -translate-y-1/2">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                             stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round"
                                  d="M9.879 7.519c1.171-1.025 3.071-1.025 4.242 0 1.172 1.025 1.172 2.687 0 3.712-.203.179-.43.326-.67.442-.745.361-1.45.999-1.45 1.827v.75M21 12a9 9 0 1 1-18 0 9 9 0 0 1 18 0Zm-9 5.25h.008v.008H12v-.008Z" />
                        </svg>
                    </span>
                </div>
                <div id="file-edit-div">
                    <label id="file-label-edit" for="file-edit" class="text-sm mb-3 ms-1">{{__("Replace file with:")}}</label>
                    <input type="file" id="file-edit" name="file" accept="{{ '.' . implode(',.', $allowedExtensions) }}"
                           class="relative text-grey-950 dark:text-grey-50 bg-grey-200 dark:bg-grey-500 rounded border-none w-full" />
                </div>
                <x-error-message id="error-file-extension-edit" class="w-full">
                    {{ __('The selected file extension is not allowed!') }}
                </x-error-message>
                <input type="url" id="link-edit" name="link"
                       class="w-full text-grey-950 dark:text-grey-50 bg-grey-200 dark:bg-grey-800 border border-grey-300 dark:border-grey-600 rounded-md focus:ring-2 focus:ring-purple-400 focus:border-purple-400 dark:placeholder-gray-400"
                       placeholder="{{ __('Link\'s URL') }}" required />
                <select id="license-edit" name="license"
                        class="w-48 text-grey-950 dark:text-grey-50 bg-grey-200 dark:bg-grey-800 border border-grey-300 dark:border-grey-600 rounded-md focus:ring-2 focus:ring-purple-400 focus:border-purple-400" 
                        required>
                        <option value="" class="hidden" selected disabled>{{ __('License') }}</option>
                    @foreach($licenses as $license)
                        @if($license->label === "CC0")
                            <option value="{{ $license->id }}">
                                [{{ $license->label }}] {{ __($license->title) }} {{ __('(default)') }}
                            </option>
                        @else
                            <option value="{{ $license->id }}">
                                [{{$license->label }}] {{  __($license->title) }}
                            </option>
                        @endif
                    @endforeach
                </select>
                <div class="w-48 text-grey-950 dark:text-grey-50 rounded border-none text-center">
                    <label for="anonymous-edit" class="text-sm font-medium text-grey-900 dark:text-white">
                        <input type="checkbox" id="anonymous-edit" name="anonymous" value="true"
                               class="w-4 h-4 text-purple-400 bg-grey-50 border-grey-300 rounded focus:ring-purple-400 focus:ring-2" />
                        <span class="ml-2 text-sm text-grey-900 dark:text-white">{{ __('Submit as anonymous') }}</span>
                    </label>
                </div>

                <button id="document-submit-button-edit" type="submit" class="w-48 relative text-grey-950 bg-purple-400 hover:brightness-90 border-none focus:outline-none font-medium rounded-md text-sm px-5 py-2.5 text-center disabled:brightness-90 disabled:cursor-not-allowed">
                    {{ __('Save changes') }}
                </button>

                <div class="w-full">
                    <x-help-form-document :id="'edit'"/>
                </div>
            </form>

        </div>
    </div>

    <script>
        // Get elements
        const formatSelectEdit = document.getElementById('format-edit');
        const linkInputEdit = document.getElementById('link-edit');
        const fileInputEdit = document.getElementById('file-edit');
        const fileEditDiv = document.getElementById('file-edit-div');
        const errorFileExtensionEdit = document.getElementById('error-file-extension-edit');
        const submitButtonEdit = document.getElementById('document-submit-button-edit');
        const courseInputEdit = document.getElementById('course-edit');
        const periodInputEdit = document.getElementById('period-edit');
        const activityInputEdit = document.getElementById('activity-edit');
        const typeInputEdit = document.getElementById('type-edit');
        const formatInputEdit = document.getElementById('format-edit');
        const titleInputEdit = document.getElementById('title-edit');
        const licenseInputEdit = document.getElementById('license-edit');

        // Wait for the DOM content to be loaded before executing the script
        document.addEventListener('DOMContentLoaded', function() {
            // Initialize Select2 for course and period select elements
            $('.select2').select2();

            // Change the Select2 arrow
            const select2Inputs = document.getElementsByClassName('select2-container');
            Array.from(select2Inputs).forEach(function(input) {
                // Create SVG element
                const svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
                svg.setAttribute("viewBox", "0 0 24 24");
                svg.setAttribute("fill", "none");
                svg.setAttribute("stroke-width", "3.5");
                svg.setAttribute("stroke", "currentColor");
                svg.setAttribute("class", "w-4 h-4 text-grey-600 dark:text-grey-200");

                const path = document.createElementNS("http://www.w3.org/2000/svg", "path");
                path.setAttribute("stroke-linecap", "round");
                path.setAttribute("stroke-linejoin", "round");
                path.setAttribute("d", "m19.5 8.25-7.5 7.5-7.5-7.5");

                svg.appendChild(path);

                // Replace <b role="presentation"> with SVG
                const bElement = input.querySelector('b[role="presentation"]');
                if (bElement) {
                    input.firstElementChild.firstElementChild.lastElementChild.replaceChild(svg, bElement);
                }
            });

            function hideLinkEditInput() {
                linkInputEdit.classList.add('hidden');
                linkInputEdit.required = false;
                licenseInputEdit.classList.add('hidden');
                licenseInputEdit.required = false;
            }

            function showLinkEditInput() {
                linkInputEdit.classList.remove('hidden');
                linkInputEdit.required = true;
            }

            function hideFileEditInput() {
                fileEditDiv.classList.add('hidden');
                fileInputEdit.required = false;
                licenseInputEdit.classList.add('hidden');
                licenseInputEdit.required = false;
                errorFileExtensionEdit.classList.add('hidden');
            }

            function showFileEditInput() {
                fileEditDiv.classList.remove('hidden');
                licenseInputEdit.classList.remove('hidden');
                licenseInputEdit.required = true;
                errorFileExtensionEdit.classList.remove('hidden');
            }

            // Hide the input elements initially
            if(formatSelectEdit.value === '{{ FormatEnum::LINK->value }}'){
                hideFileEditInput();
                showLinkEditInput();
            }
            else if(formatSelectEdit.value === '{{ FormatEnum::FILE->value }}'){
                hideLinkEditInput();
                showFileEditInput();
            }
            else {
                hideLinkEditInput();
                hideFileEditInput();
            }


            // Listen for changes on the select element
            formatSelectEdit.addEventListener('change', function() {
                // Show or hide the input elements based on the selected value
                if (this.value === '{{ FormatEnum::LINK->value }}') {
                    // If 'lien' is selected, show the link input and hide the document input
                    showLinkEditInput();
                    hideFileEditInput();
                } else if (this.value === '{{ FormatEnum::FILE->value }}') {
                    // If 'fichier' is selected, show the document input and hide the link input
                    hideLinkEditInput();
                    showFileEditInput();
                    validateFileExtensionEdit();
                } else {
                    // If neither 'lien' nor 'fichier' is selected, hide both inputs
                    hideLinkEditInput();
                    hideFileEditInput();
                }
            });
        });

        //retrieve all inputs from modal-edit-document div
        const inputs = document.getElementById('modal-edit-document').querySelectorAll('input, select');

        // add event listener to all inputs depending on the type of input
        inputs.forEach(input => {
            if (input.tagName === 'SELECT') {
                input.addEventListener('change', handleSubmitEnability);
            } else {
                input.addEventListener('input', handleSubmitEnability);
            }
        });

        function handleSubmitEnability() {
            if(!courseInputEdit.value || !periodInputEdit.value
                || !activityInputEdit.value
                || !typeInputEdit.value
                || !formatInputEdit.value) {
                submitButtonEdit.setAttribute('disabled', 'disabled');
            }
            else {
                submitButtonEdit.removeAttribute('disabled');
            }
        }

        // Convert PHP array to JavaScript array
        const allowedExtensionsEdit = @json($allowedExtensions);

        function validateFileExtensionEdit() {
            // Get the selected file
            const file = fileInputEdit.files[0];

            // Check if file is selected
            if (file) {
                // Get the file extension
                const fileName = file.name;
                const fileExtension = fileName.split('.').pop().toLowerCase();

                // Check if file extension is allowed
                if (!allowedExtensionsEdit.includes(fileExtension)) {
                    errorFileExtensionEdit.classList.remove('hidden');
                    submitButtonEdit.setAttribute('disabled', 'disabled');
                } else {
                    errorFileExtensionEdit.classList.add('hidden');
                    submitButtonEdit.removeAttribute('disabled');
                }
            } else {
                // If no file is selected, hide the error message and enable the submit button
                errorFileExtensionEdit.classList.add('hidden');
                submitButtonEdit.removeAttribute('disabled');
            }
        }

        // Listen for changes in the file input
        fileInputEdit.addEventListener('change', validateFileExtensionEdit);
    </script>
</div>
