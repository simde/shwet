<div id="modal-confirm-deletion-doc" tabindex="-1" class="hidden overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0 z-50 justify-center items-center w-full md:inset-0 h-[calc(100%-1rem)] max-h-full">
    <div class="relative p-4 w-full max-w-md max-h-full">
        <div class="relative bg-white rounded-lg shadow dark:bg-grey-700">
            <button type="button" class="absolute top-3 end-2.5 text-grey-400 bg-transparent hover:bg-grey-200 hover:text-grey-900 rounded-lg text-sm w-8 h-8 ms-auto inline-flex justify-center items-center dark:hover:bg-grey-600 dark:hover:text-white" data-modal-hide="modal-confirm-deletion-doc">
                <svg class="w-3 h-3" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 14 14">
                    <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6"/>
                </svg>
                <span class="sr-only">Close modal</span>
            </button>
            <div class="p-4 md:p-5 text-center">
                <svg class="mx-auto mb-4 text-grey-400 w-12 h-12 dark:text-grey-200" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 20 20">
                    <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 11V6m0 8h.01M19 10a9 9 0 1 1-18 0 9 9 0 0 1 18 0Z"/>
                </svg>
                <h3 class="mb-5 text-lg font-normal text-grey-500 dark:text-grey-300">{{__("Are you sure you want to delete this document?")}}</h3>
                <form action="{{ route('documents.delete') }}" method="POST">
                    @csrf
                    <input type="hidden" name="id" id="delete-document-id-input">
                    <button data-modal-hide="modal-confirm-deletion-doc" type="submit" class="text-white bg-red-600 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 dark:focus:ring-red-800 font-medium rounded-lg text-sm inline-flex items-center px-5 py-2.5 text-center">
                        {{__("Yes, I'm sure")}}
                    </button>
                    <button data-modal-hide="modal-confirm-deletion-doc" type="button" class="py-2.5 px-5 ms-3 text-sm font-medium text-grey-900 focus:outline-none bg-white rounded-lg border border-grey-200 hover:bg-grey-100 hover:text-purple-400 focus:z-10 focus:ring-4 focus:ring-grey-100 dark:focus:ring-grey-700 dark:bg-grey-800 dark:text-grey-400 dark:border-grey-600 dark:hover:text-white dark:hover:bg-grey-700">
                        {{__("No, cancel")}}
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>
