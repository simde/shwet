<div id="modal-add-doc-report" tabindex="-1"
     class="hidden overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0 z-50 justify-center items-center w-full md:inset-0 h-[calc(100%-1rem)] max-h-full text-grey-950 dark:text-grey-50">
    <div class="relative p-4 w-full max-w-xl max-h-full">
        <form autocomplete="off" action="{{ route('document-reports.store') }}" method="POST">
            @csrf
            <input type="hidden" id="add-doc-report-doc-id" name="document_id" value="">

            <div class="relative bg-grey-100 rounded-lg shadow dark:bg-grey-800">
                <div
                    class="flex items-center justify-between px-5 py-4 border-b rounded-t border-grey-200 dark:border-grey-600">
                    <h3 class="text-lg font-semibold text-grey-900 dark:text-white">
                        {{ __('Make a report') }}
                    </h3>

                    <!-- Close button -->
                    <button type="button"
                            class="text-grey-400 bg-transparent hover:bg-grey-200 hover:text-grey-900 rounded-md text-sm w-8 h-8 ms-auto inline-flex justify-center items-center dark:hover:bg-grey-600 dark:hover:text-white"
                            data-modal-hide="modal-add-doc-report">
                        <svg class="w-3 h-3" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none"
                            viewBox="0 0 14 14">
                            <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                                d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6"/>
                        </svg>
                        <span class="sr-only">{{ __('Close modal') }}</span>
                    </button>
                </div>
                <div class="p-4 md:p-5 flex flex-col gap-5">
                    <div>
                        <div>
                            <span>{{__("Please explain below what is wrong with the document :")}}</span>
                        </div>
                        <textarea id="add-report-comment"
                                name="comment"
                                rows="5"
                                class="mt-1 w-full bg-grey-200 dark:bg-grey-600 rounded-md p-2 placeholder:text-grey-500 dark:placeholder:text-grey-400 focus:outline-none focus:ring-2 focus:ring-purple-400 focus:border-transparent"
                                placeholder="{{__("I have an issue with the document because...")}}" maxlength="800"
                                onkeyup="checkLength('add-report-comment', 800, 'report-add-comment-length')"
                                required></textarea>
                        <div class="flex justify-end items-top">
                            <span class="text-sm text-grey-500" id="report-add-comment-length">0/800</span>
                        </div>
                    </div>
                    <div class="flex flex-col gap-1">
                        <div>
                            <span> {{__("About document")}} :</span>
                        </div>
                        <div class="w-full bg-grey-200 dark:bg-grey-600 rounded-md p-3" id="report-document">
                            <x-link
                                :href="'#'"
                                id="report-document-link">
                            </x-link>
                        </div>
                    </div>
                    <div class="flex justify-center">
                        <x-buttons.icon-text-purple :type="'submit'">
                            <x-slot:icon>
                                <svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" fill="currentColor" class="bi bi-send" viewBox="0 0 16 16">
                                    <path d="M15.854.146a.5.5 0 0 1 .11.54l-5.819 14.547a.75.75 0 0 1-1.329.124l-3.178-4.995L.643 7.184a.75.75 0 0 1 .124-1.33L15.314.037a.5.5 0 0 1 .54.11ZM6.636 10.07l2.761 4.338L14.13 2.576zm6.787-8.201L1.591 6.602l4.339 2.76z"/>
                                </svg>
                            </x-slot:icon>
                            <x-slot:text>{{__('Send report')}}</x-slot:text>
                        </x-buttons.icon-text-purple>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
