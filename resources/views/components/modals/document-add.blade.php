@php
    use App\Models\Course;
    use App\Models\License;
    use App\Models\Period;
    use App\Models\FileExtension;
    use App\Enums\DocumentType as DocumentTypeEnum;
    use App\Enums\Activity as ActivityEnum;
    use App\Enums\Format as FormatEnum;
@endphp
<div id="modal-document" tabindex="-1" aria-hidden="true"
     class="text-grey-950 dark:text-grey-50 hidden overflow-y-auto overflow-x-hidden fixed top-0 right-0 left-0 z-50 justify-center items-center w-full md:inset-0 h-[calc(100%-1rem)] max-h-full">
    <div class="relative p-4 max-h-full">
        <div class="relative bg-white rounded-md shadow dark:bg-grey-700">
            <div class="flex items-center justify-between p-4 md:p-5 border-b rounded-t dark:border-grey-600">
                <h3 class="text-lg font-semibold text-grey-900 dark:text-white">
                    {{ __('Publish a document') }}
                </h3>

                <!-- Close button -->
                <button type="button"
                        class="text-grey-400 bg-transparent hover:bg-grey-200 hover:text-grey-900 rounded-md text-sm w-8 h-8 ms-auto inline-flex justify-center items-center dark:hover:bg-grey-600 dark:hover:text-white"
                        data-modal-hide="modal-document">
                    <svg class="w-3 h-3" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none"
                         viewBox="0 0 14 14">
                        <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                              d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6"/>
                    </svg>
                    <span class="sr-only">{{ __('Close modal') }}</span>
                </button>
            </div>

            <!-- Form -->
            <form id="document-form" class="flex flex-wrap items-center justify-around gap-8 p-4 max-w-md" method="POST"
                  action="{{ route('documents.store') }}" enctype="multipart/form-data">

                @csrf
                @php
                    $seasonOrder = ['H', 'P', 'E', 'A'];
                    $courses = Course::all();
                    $periods = Period::all();
                    $types = DocumentTypeEnum::cases();
                    $activities = ActivityEnum::allowedActivities();
                    $formats = FormatEnum::cases();
                    $allowedExtensions = FileExtension::onlyActive()->pluck('suffix')->toArray();
                    $licenses = License::onlyActive()->get();
                @endphp

                <select id="course" name="course"
                        class="w-48 text-grey-950 dark:text-grey-50 bg-grey-200 dark:bg-grey-800 border border-grey-300 dark:border-grey-600 rounded-md focus:ring-2 focus:ring-purple-400 focus:border-purple-400 select2"
                        required>
                    <option value="" class="hidden" class="w-4 h-4" selected disabled>{{ __('UV') }}</option>
                    @foreach($courses->sortBy('code') as $course)
                        <option value="{{ $course->code }}">{{ $course->code }}</option>
                    @endforeach
                </select>
                <select id="period" name="period"
                        class="w-48 text-grey-950 dark:text-grey-50 bg-grey-200 dark:bg-grey-800 border border-grey-300 dark:border-grey-600 rounded-md focus:ring-2 focus:ring-purple-400 focus:border-purple-400 select2"
                        required>
                    <option value="" class="hidden" selected disabled>{{ ucfirst(__('semester')) }}</option>
                    @foreach($periods->sortByDesc(function ($period) use ($seasonOrder) {
                        $year = $period->year;
                        $season = $period->season->code->value;
                        return [$year, array_search($season, $seasonOrder)];
                    }) as $period)
                        <option value="{{ $period->id . ',' . $period->getLabel() }}">{{ $period->getLabel() }}</option>
                    @endforeach
                </select>
                <select id="activity" name="activity"
                        class="w-48 text-grey-950 dark:text-grey-50 bg-grey-200 dark:bg-grey-800 border border-grey-300 dark:border-grey-600 rounded-md focus:ring-2 focus:ring-purple-400 focus:border-purple-400"
                        required>
                    <option value="" class="hidden" selected disabled>{{ __('Type of activity') }}</option>
                    @foreach($activities as $activity)
                        <option value="{{ $activity->value }}">{{ __($activity->label()) }}</option>
                    @endforeach
                </select>
                <select id="type" name="type"
                        class="w-48 text-grey-950 dark:text-grey-50 bg-grey-200 dark:bg-grey-800 border border-grey-300 dark:border-grey-600 rounded-md focus:ring-2 focus:ring-purple-400 focus:border-purple-400"
                        required>
                    <option value="" class="hidden" selected disabled>{{ __('Type of document') }}</option>
                    @foreach($types as $type)
                        <option value="{{ $type->value }}">{{ __($type->label()) }}</option>
                    @endforeach
                </select>
                <select id="format" name="format"
                        class="w-48 text-grey-950 dark:text-grey-50 bg-grey-200 dark:bg-grey-800 border border-grey-300 dark:border-grey-600 rounded-md focus:ring-2 focus:ring-purple-400 focus:border-purple-400"
                        required>
                    <option value="" class="hidden" selected disabled>{{ __('Format') }}</option>
                    @foreach($formats as $format)
                        <option value="{{ $format->value }}">{{ __($format->label()) }}</option>
                    @endforeach
                </select>
                <div class="w-48 relative">
                    <input type="text" id="title" name="title"
                           class="w-full pr-8 text-grey-950 dark:text-grey-50 bg-grey-200 dark:bg-grey-800 border border-grey-300 dark:border-grey-600 rounded-md focus:ring-2 focus:ring-purple-400 focus:border-purple-400 dark:placeholder-grey-400"
                           placeholder="{{ __('Title') }}"/>
                    <span
                        title="{{ __('Please only add a title if the information composing it has not been entered above (example: title of a report)!') }}"
                        class="w-6 h-6 absolute right-2 top-1/2 transform -translate-y-1/2">
                        <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke-width="1.5"
                             stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round"
                                  d="M9.879 7.519c1.171-1.025 3.071-1.025 4.242 0 1.172 1.025 1.172 2.687 0 3.712-.203.179-.43.326-.67.442-.745.361-1.45.999-1.45 1.827v.75M21 12a9 9 0 1 1-18 0 9 9 0 0 1 18 0Zm-9 5.25h.008v.008H12v-.008Z"/>
                        </svg>
                    </span>
                </div>
                <input type="file" id="file" name="file" accept="{{ '.' . implode(',.', $allowedExtensions) }}"
                       class="relative text-grey-950 dark:text-grey-50 bg-grey-200 dark:bg-grey-500 rounded border-none w-full"
                       required/>
                <x-error-message id="error-file-extension" class="w-full">
                    {{ __('The selected file extension is not allowed!') }}
                </x-error-message>
                <input type="url" id="link" name="link"
                       class="w-full text-grey-950 dark:text-grey-50 bg-grey-200 dark:bg-grey-800 border border-grey-300 dark:border-grey-600 rounded-md focus:ring-2 focus:ring-purple-400 focus:border-purple-400 dark:placeholder-gray-400"
                       placeholder="{{ __('Link\'s URL') }}" required/>
                <select id="license" name="license"
                        class="w-48 text-grey-950 dark:text-grey-50 bg-grey-200 dark:bg-grey-800 border border-grey-300 dark:border-grey-600 rounded-md focus:ring-2 focus:ring-purple-400 focus:border-purple-400"
                        required>
                    <option value="" class="hidden" selected disabled>{{ __('License') }}</option>
                    @foreach($licenses as $license)
                        @if($license->label === "CC0")
                            <option value="{{ $license->id }}">
                                [{{ $license->label }}] {{ __($license->title) }} {{ __('(default)') }}
                            </option>
                        @else
                            <option value="{{ $license->id }}">
                                [{{$license->label }}] {{  __($license->title) }}
                            </option>
                        @endif
                    @endforeach
                </select>
                <div class="w-48 text-grey-950 dark:text-grey-50 rounded border-none text-center">
                    <label for="anonymous" class="text-sm font-medium text-grey-900 dark:text-white">
                        <input type="checkbox" id="anonymous" name="anonymous" value="true"
                               class="w-4 h-4 text-purple-400 bg-grey-50 border-grey-300 rounded focus:ring-purple-400 focus:ring-2"
                               checked/><span
                            class="ml-2 text-sm text-grey-900 dark:text-white">{{ __('Submit as anonymous') }}</span>
                    </label>
                </div>
                <div class="w-full text-center">
                    <button id="document-submit-button" type="submit"
                            class="w-48 relative text-grey-950 bg-purple-400 hover:brightness-90 border-none focus:outline-none font-medium rounded-md text-sm px-5 py-2.5 text-center disabled:brightness-90 disabled:cursor-not-allowed">
                        {{ __('Publish the document') }}
                    </button>
                </div>

                <div class="w-full">
                    <x-help-form-document :id="'add'"/>
                </div>
            </form>

        </div>
    </div>

    <script>
        // Get elements
        const formatSelect = document.getElementById('format');
        const linkInput = document.getElementById('link');
        const fileInput = document.getElementById('file');
        const errorFileExtension = document.getElementById('error-file-extension');
        const submitButton = document.getElementById('document-submit-button');
        const licenseInput = document.getElementById('license');

        // Convert PHP array to JavaScript array
        const allowedExtensions = @json($allowedExtensions);

        // Wait for the DOM content to be loaded before executing the script
        document.addEventListener('DOMContentLoaded', function () {
            // Initialize Select2 for course and period select elements
            $('.select2').select2();

            // Change the Select2 arrow
            const select2Inputs = document.getElementsByClassName('select2-container');
            Array.from(select2Inputs).forEach(function (input) {
                // Create SVG element
                const svg = document.createElementNS("http://www.w3.org/2000/svg", "svg");
                svg.setAttribute("viewBox", "0 0 24 24");
                svg.setAttribute("fill", "none");
                svg.setAttribute("stroke-width", "3.5");
                svg.setAttribute("stroke", "currentColor");
                svg.setAttribute("class", "w-4 h-4 text-grey-600 dark:text-grey-200");

                const path = document.createElementNS("http://www.w3.org/2000/svg", "path");
                path.setAttribute("stroke-linecap", "round");
                path.setAttribute("stroke-linejoin", "round");
                path.setAttribute("d", "m19.5 8.25-7.5 7.5-7.5-7.5");

                svg.appendChild(path);

                // Replace <b role="presentation"> with SVG
                const bElement = input.querySelector('b[role="presentation"]');
                if (bElement) {
                    input.firstElementChild.firstElementChild.lastElementChild.replaceChild(svg, bElement);
                }
            });

            function hideLinkInput() {
                linkInput.classList.add('hidden');
                linkInput.required = false;
            }

            function showLinkInput() {
                linkInput.classList.remove('hidden');
                linkInput.required = true;
            }

            function hideFileInput() {
                licenseInput.classList.add('hidden');
                fileInput.classList.add('hidden');
                fileInput.required = false;
                licenseInput.required = false;
                errorFileExtension.classList.add('hidden');
            }

            function showFileInput() {
                fileInput.classList.remove('hidden');
                licenseInput.classList.remove('hidden');
                fileInput.required = true;
                licenseInput.required = true;
                errorFileExtension.classList.remove('hidden');
            }

            // Hide the input elements initially
            if (formatSelect.value === '{{ FormatEnum::LINK->value }}') {
                hideFileInput();
            } else if (formatSelect.value === '{{ FormatEnum::FILE->value }}') {
                hideLinkInput();
            } else {
                hideLinkInput();
                hideFileInput();
            }


            // Listen for changes on the select element
            formatSelect.addEventListener('change', function () {
                // Show or hide the input elements based on the selected value
                if (this.value === '{{ FormatEnum::LINK->value }}') {
                    // If 'lien' is selected, show the link input and hide the document input
                    showLinkInput();
                    hideFileInput();
                } else if (this.value === '{{ FormatEnum::FILE->value }}') {
                    // If 'fichier' is selected, show the document input and hide the link input
                    hideLinkInput();
                    showFileInput();
                    validateFileExtension();
                } else {
                    // If neither 'lien' nor 'fichier' is selected, hide both inputs
                    hideLinkInput();
                    hideFileInput();
                }
            });
        });

        // Function to validate file extension
        function validateFileExtension() {
            // Get the selected file
            const file = fileInput.files[0];

            // Check if file is selected
            if (file) {
                // Get the file extension
                const fileName = file.name;
                const fileExtension = fileName.split('.').pop().toLowerCase();

                // Check if file extension is allowed
                if (!allowedExtensions.includes(fileExtension)) {
                    errorFileExtension.classList.remove('hidden');
                    submitButton.setAttribute('disabled', 'disabled');
                } else {
                    errorFileExtension.classList.add('hidden');
                    submitButton.removeAttribute('disabled');
                }
            } else {
                // If no file is selected, hide the error message and enable the submit button
                errorFileExtension.classList.add('hidden');
                submitButton.removeAttribute('disabled');
            }
        }

        // Listen for changes in the file input
        fileInput.addEventListener('change', validateFileExtension);
    </script>
</div>
