@props(['active'])

@php
    $classes = ($active ?? false)
                ? 'inline-flex items-center lg:justify-center py-2 px-3 mt-2 w-full text-grey-950 rounded-lg lg:w-auto hover:bg-grey-200 lg:mt-0 lg:hover:bg-grey-100 dark:text-grey-50 lg:dark:hover:bg-grey-800 lg:text-center lg:py-2 lg:px-3 dark:hover:bg-grey-800 dark:hover:text-grey-50 dark:border-grey-700 lg:text-grey-50 lg:hover:text-grey-950 dark:lg:text-grey-50'
                : 'inline-flex items-center lg:justify-center py-2 px-3 mt-2 w-full text-grey-950 rounded-lg lg:w-auto hover:bg-grey-200 lg:mt-0 lg:hover:bg-grey-100 dark:text-grey-50 lg:dark:text-grey-950 lg:dark:hover:bg-grey-800 lg:text-center lg:py-2 lg:px-3 dark:hover:bg-grey-800 dark:hover:text-grey-50 dark:border-grey-700';
@endphp

<a {{ $attributes->class($classes) }}>
    {{$icon}}
    <span class="ms-1">
        {{$title}}
    </span>
</a>
