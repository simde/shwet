@extends('layouts.app')

@section('content')
    <div class="p-4 flex flex-col gap-y-5 rounded-md bg-grey-200 dark:bg-grey-800 text-grey-950 dark:text-grey-50">
        <div>
            <h1 class="uppercase font-extrabold text-2xl">{{ __('Sitemap') }}</h1>
            <span class="text-sm">
                <i>{{ __("Last updated on June 23, 2024.") }}</i>
            </span>
        </div>
        <p>{{ __("Sitemap objective is to provide a structured list of all pages available on the site.") }}</p>

        <div>
            <ul class="list-disc list-inside">
                <li><x-link :href="route('welcome')">{{ __("Welcome page") }}</x-link></li>
                <li><x-link :href="route('courses.index')">{{ __("All courses") }}</x-link></li>
                <li>{{__("User pages")}}
                    <ul class="list-disc list-inside ms-5">
                        <li>
                            <x-link :href="route('my-documents')">{{ __("My documents") }}</x-link>
                        </li>
                        <li>
                            <x-link :href="route('document-reports.index')">{{ __("My reports") }}</x-link>
                        </li>
                    </ul>
                </li>
                @if(auth()->user()->isAdmin())
                    <li>{{__("Administrator pages")}}
                        <ul class="list-disc list-inside ms-5">
                            <li>
                                <x-link :href="route('admin.document-reports.index')">{{ __("Document reports") }}</x-link>
                            </li>
                        </ul>
                    </li>
                @endif
                <li>
                    <x-link :href="route('legal-notices')">{{ __("Legal notices") }}</x-link>
                </li>
                <li>
                    <x-link :href="route('privacy-policies')">{{ __("Privacy policy") }}</x-link>
                </li>
                <li>
                    <x-link :href="route('sitemap')">{{ __("Sitemap") }}</x-link>
                </li>
            </ul>
        </div>
    </div>
@endsection
