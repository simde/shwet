<h1>Nouveau signalement sur Shwet !</h1>
<p>Un nouveau signalement a été effectué sur le site par <i>{{ $details['reportAuthor']->getFullName() }}
    </i> [id: <code>{{ $details['reportAuthor']->id }}</code>] le {{ $details['reportCreatedAt'] }}.</p>
<p>Hop hop hop, il faut aller s'en occuper !</p>
<p>Ça se passe par ici : <a href="{{ route('admin.document-reports.index') }}">Page des signalements</a>.</p>
<p>
    <span>Message du signalement :</span>
    <code>{{ $details['message'] }}</code>
</p>
<p>À très vite !</p>
