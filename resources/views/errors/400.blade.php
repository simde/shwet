@extends('layouts.app')

@section('content')
    <h1>{{ $exception->getStatusCode() . ' : ' . __($exception->getMessage()) }}</h1>
@endsection
