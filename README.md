# 🦉 Shwet - README

Welcome on the code repository of the online platform Shwet. We are happy to know that you are interested in this tool and curious about its source code. The following descriptions are part of the README.md file, located at the root of the project.

## Table of contents

This document covers the following topics:

- [🦉 Shwet - README](#-shwet---readme)
  - [Table of contents](#table-of-contents)
  - [1. Meet Shwet](#1-meet-shwet)
  - [2. Authors and Contribute](#2-authors-and-contribute)
  - [3. Functionalities](#3-functionalities)
  - [4. Use case diagram](#4-use-case-diagram)
  - [5. Database schemas](#5-database-schemas)
    - [UML diagram](#uml-diagram)
    - [MLD diagram](#mld-diagram)
  - [6. Technologies](#6-technologies)
  - [7. Install and run](#7-install-and-run)
  - [8. License](#8-license)
  - [9. Contact us](#9-contact-us)

## 1. Meet Shwet

Shwet is a web application developed by the Service informatique de la Maison des Étudiants (SiMDE), an UTC association that manages the entire application suite intended for the associative life at UTC. Its objective is to share resources related to the courses offered at UTC. These resources can be of different nature (subject, correction or personal), on various supports (file or link to another resource), and in different file extensions (.pdf, .docx, .pptx, .zip, etc.).
Let us introduce the official new logo of Shwet (unfortunately, only French-speaking people will understand this amazing pun):
<p align="center" width="100%">
    <img width="100px" src="/public/images/logos/ShwetBde.png">
</p>

## 2. Authors and Contribute

Shwet is a project developed by the SiMDE team, the UTC students club in charge of the development and maintenance of the digital services for students and students clubs.  
Up to now, the current version of this project was developed the following members:

* Quentin BOYER
* Eliot DEWULF

With the help of Léo Mullier
It is based on a previous version developed during the HackathonUTC event in 2015 by:

* Samuel FAVRICHON
* Mewen MICHEL
* Nicolas SZEWE

If you are a UTC student and you want to contribute to this project, you can contact us using the email address <simde@assos.utc.fr>. We will be happy to welcome you in our team and to help you to start your first contribution.

## 3. Functionalities

| Description                                     | Actor(s)            | State | Description                                                                                                                                                                                                                                                                                                             |
|-------------------------------------------------|---------------------|-------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Authenticate (login, logout)                    | All UTC CAS members | Done  | The user must authenticate via the SiMDE OAuth portal using their CAS to access Shwet.                                                                                                                                                                                                                                  |
| Search for a course                             | All UTC CAS members | Done  | The user can enter a course code in the search form to try to find a specific course. They can also perform an advanced search using filters in the course catalog with more information about the course.                                                                                                              |
| View the documents of a course                  | All UTC CAS members | Done  | Once on a course's page, the user can open a document to view it, or download it. They can also download all documents of a section of a course at once.                                                                                                                                                                |
| Publish  a document                             | All UTC CAS members | Done  | The user can access a form to upload a document (file or link) specifying at least the period, the course, the activity (final, midterm, lecture, tutorial, practical, report, other), the type (subject, correction, personal) and optionally a title. The document can be show anonymously or not to other users.     |
| Edit one of my documents                        | All UTC CAS members | Done  | The user can access a form to edit one of its documents (file or link) specifying at least the period, the UV, the activity (final, midterm, lecture, tutorial, practical, report, other), the type (subject, correction, personal) and optionally a title. The document can be show anonymously or not to other users. |
| Delete one of my documents                      | All UTC CAS members | Done  | The user can delete one its documents.                                                                                                                                                                                                                                                                                  |
| Change the interface language (French, English) | All UTC CAS members | Done  | The user should be able to select the site language between French and English for better accessibility.                                                                                                                                                                                                                |
| Change the interface theme (Light, Dark)        | All UTC CAS members | Done  | The user should be able to select the site theme between light and dark for better accessibility.                                                                                                                                                                                                                       |
| Report the content of a document                | All UTC CAS members | Done  | If a document seems incorrect, incomplete, or unreadable, a user should be able to report it to the administrators and other users.                                                                                                                                                                                     |
| Add/Remove a course from favorites              | All UTC CAS members | Done  | The user has a list of favorite courses that are highlighted. They can add and remove any course from their favorites.                                                                                                                                                                                                  |
| Hide a document                                 | Administrator       | Done  | The administrator can hide a document that is inappropriate or offensive.                                                                                                                                                                                                                                               |
| Comment on a document                           | All UTC CAS members | To do | The user can add a comment about a specific document if they need clarification on its content or if they wish to clarify an obscure point.                                                                                                                                                                             |
| Report the content of a comment                 | All UTC CAS members | To do | If a comment is inappropriate or offensive, the user can report it to the administrators who will then decide whether it should be hidden.                                                                                                                                                                              |
| View statistics                                 | All UTC CAS members | To do | The user can view statistics on the documents published on Shwet and the documents they themselves have published.                                                                                                                                                                                                      |
| Improve publish document form                   | All UTC CAS members | To do | Improve the form to publish documents with a progress bar, reusing parameters of previous add.                                                                                                                                                                                                                          |
| Hide a comment                                  | Administrator       | To do | The administrator can hide a comment that is inappropriate or offensive.                                                                                                                                                                                                                                                |
| Administration page of all documents            | Administrator       | To do | Create a page with filters and sorting parameters for all attributes of documents to manage documents and their reports and comments.                                                                                                                                                                                   |

## 4. Use case diagram

![Use Case Diagram](/docs/use_case_diagram.png)

This use case diagram have been realised with LucidCharts ([online version](https://lucid.app/lucidchart/6562e9bc-6fd2-47aa-96d1-cdddd1b00f6a/edit?existing=1&token=ad4a2f6caac630b0c223e00016c5328fc94c78e135517367582812380bc53ddc-eml%3Deliotdewulf91%2540gmail.com%26ts%3D1708887157%26uid%3D169039291&docId=6562e9bc-6fd2-47aa-96d1-cdddd1b00f6a&shared=true&invitationId=inv_053b0d5e-8357-41ea-8d7a-2b8d327bcc1a&page=.Q4MUjXso07N))

## 5. Database schemas
### UML diagram
![UML diagram](/docs/database/uml_diagram.png)

### MLD diagram
![MLD diagram](/docs/database/mld_diagram.png)

## 6. Technologies

* Backend : Laravel v10.48.11 (PHP v8.3.13)
* Frontend : Blade templates with Tailwind CSS and JS scripts

## 7. Install and run

Here are the steps to install and run the project locally on your computer. First, you need to have PHP, Composer and Node installed. Then, clone the repository on your computer using the following command lines in your favorite terminal:

``` bash
cd [your directory]
git clone https://gitlab.utc.fr/simde/shwet.git
cd shwet
npm install
composer install
```

+ In a terminal, run the command `npm run dev` to compile CSS and JS files and enable live reload.
+ In another terminal, run the command `php artisan serve` to start the PHP server (<http://localhost:8000>).
+ Contact the SiMDE members to get the configuration file (_.env_).
+ Add an SSL __cacert__ certificate ([link to stackoverflow doc](https://stackoverflow.com/questions/41772340/how-do-i-add-a-certificate-authority-to-php-so-the-file-function-trusts-certif)) to the PHP folder to be able to exchange data with the simde oauth.
+ Install the crawler locally to be able to retrieve UV and branch data.
+ Please, note that you need to have a MySQL database to store the data. Once your database is installed, run the command `php artisan migrate:fresh --seed` to create and populate the tables.

## 8. License

Shwet is licensed under the [Creative Commons Attribution-NonCommercial 4.0 International License](https://creativecommons.org/licenses/by-nc/4.0/). This means you are free to share and adapt the code for non-commercial purposes, provided you give appropriate credit to the original author (SiMDE) and indicate if changes were made. However, you may not use the code for commercial purposes without explicit permission from the author.

## 9. Contact us

For any missing information or support request, don't hesitate to contact the SiMDE team using the email address <simde@assos.utc.fr>. We will be happy to answer you as soon as possible.
