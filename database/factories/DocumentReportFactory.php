<?php

namespace Database\Factories;

use App\Enums\ReportStatus as ReportStatusEnum;
use App\Models\Document;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class DocumentReportFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'comment' => $this->faker->sentence,
            'commented_at' => $this->faker->dateTime,
            'status' => $this->faker->randomElement(ReportStatusEnum::values()),
            'document_id' => Document::all()->random()->id,
            'user_id' => User::all()->random()->id,
        ];
    }
}
