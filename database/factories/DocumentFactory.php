<?php

namespace Database\Factories;

use App\Models\Course;
use App\Models\User;
use App\Models\Period;
use App\Models\FileExtension;
use App\Models\License;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Enums\DocumentType as DocumentTypeEnum;
use App\Enums\Activity as ActivityEnum;
use App\Enums\Format as FormatEnum;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Document>
 */
class DocumentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $type = fake()->randomElement(DocumentTypeEnum::cases())->value;
        $activity = fake()->randomElement(ActivityEnum::allowedActivities())->value;
        $format = fake()->randomElement(FormatEnum::cases())->value;
        $course = Course::all()->random();
        $period = Period::all()->random();
        $fileExtension = FileExtension::all()->random();
        $license = License::all()->random();
        if ($format == FormatEnum::LINK->value) {
            $path = fake()->url;
        } elseif ($format == FormatEnum::FILE->value) {
            $path = strtolower($course->code) . "-{$type}-{$activity}-" . strtolower($period->season->code->value) . $period->year . ".{$fileExtension->suffix}";
        }

        return [
            'title' => fake()->sentence(3),
            'path' => $path,
            'type' => $type,
            'activity' => $activity,
            'format' => $format,
            'anonymous' => fake()->boolean($chanceOfGettingTrue = 70),
            'course_id' => $course->id,
            'user_id' => User::all()->random()->id,
            'period_id' => $period->id,
            'file_extension_id' => $fileExtension->id,
            'license_id' => $license->id,
        ];
    }
}
