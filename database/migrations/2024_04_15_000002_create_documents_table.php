<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Enums\DocumentType as DocumentTypeEnum;
use App\Enums\Activity as ActivityEnum;
use App\Enums\Format as FormatEnum;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->id()->comment('Unique ID of each document');
            $table->string('title', 200)->nullable()->comment('Title of each document');
            $table->string('path')->comment('Path of each document');
            $table->enum('type', DocumentTypeEnum::values())->comment('Type of each document');
            $table->enum('activity', ActivityEnum::values())->comment('Activity of each document');
            $table->enum('format', FormatEnum::values())->comment('Format of each document');
            $table->boolean('anonymous')->default(true)->comment('Boolean to know if the document is anonymous or not');
            $table->integer('views')->default(0)->comment('Number of views of each document');
            $table->integer('reports')->default(0)->comment('Number of reports of each document');
            $table->timestamps();
            $table->timestamp('accepted_at')->nullable()->comment('Date when it is an accepted document');
            $table->timestamp('deleted_at')->nullable()->comment('Date when it is a deleted document');
            $table->foreignId('course_id')->constrained()->comment('ID of the course to which the document belongs');
            $table->foreignUuid('user_id')->constrained()->comment('ID of the user who uploaded the document');
            $table->foreignId('period_id')->constrained()->comment('ID of the period to which the document belongs');
            $table->foreignId('file_extension_id')->nullable()->constrained()->comment('ID of the file extension of the document');
            $table->foreignId('license_id')->nullable()->constrained()->comment('ID of the license of the document (null for links)');
        });
        DB::statement("ALTER TABLE documents COMMENT = 'Table containing all documents';");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('documents');
    }
};
