<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('file_extensions', function (Blueprint $table) {
            $table->id()->comment('Unique ID of each file extension');
            $table->string('suffix', 20)->unique()->comment('Unique suffix of each file extension');
            $table->timestamp('deleted_at')->nullable()->comment('Date when it is a deleted file extension');
        });
        DB::statement("ALTER TABLE file_extensions COMMENT = 'Table containing all file extensions';");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('file_extensions');
    }
};
