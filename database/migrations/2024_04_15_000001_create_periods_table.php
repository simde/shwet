<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('periods', function (Blueprint $table) {
            $table->id()->comment('Unique ID of each period');
            $table->unsignedInteger('year')->check('year >= 1972 AND year <= YEAR(CURDATE())')->comment('Year of each period');
            $table->foreignId('season_id')->constrained()->comment('Unique ID of the associated season');
        });
        DB::statement("ALTER TABLE periods COMMENT = 'Table containing all periods';");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('periods');
    }
};
