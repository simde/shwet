<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('licenses', function (Blueprint $table) {
            $table->id()->comment('Unique ID of each license')->autoIncrement();
            $table->string('label', 20)->unique()->comment('Label of the license');
            $table->string('title', 70)->comment('Title of the license');
            $table->string('description', 255)->comment('Description of the license');
            $table->string('url', 255)->nullable()->comment('URL of the license');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable()->comment('Date when it is a deleted license');
        });
        DB::statement("ALTER TABLE licenses COMMENT = 'Table containing all licenses';");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('licenses');
    }
};
