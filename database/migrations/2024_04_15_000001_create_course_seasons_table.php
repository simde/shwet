<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('course_seasons', function (Blueprint $table) {
            $table->foreignId('course_id')->constrained();
            $table->foreignId('season_id')->constrained();
            $table->primary(['course_id', 'season_id']);
        });
        DB::statement("ALTER TABLE course_seasons COMMENT = 'Table containing all associated seasons for all courses';");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('course_seasons');
    }
};
