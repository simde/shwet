<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Enums\CourseType as CourseTypeEnum;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->id()->comment('Unique ID of each course');
            $table->string('code', 5)->unique()->comment('Unique code of each course');
            $table->string('title', 200)->nullable()->comment('Title of each course (can be null because the old data\'s seeder does not have always this information)');
            $table->enum('type', CourseTypeEnum::values())->nullable()->comment('Type of each course');
            $table->timestamp('archived_at')->nullable()->comment('Date when it is an archived course'); 
        });
        DB::statement("ALTER TABLE courses COMMENT = 'Table containing all courses';");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('courses');
    }
};
