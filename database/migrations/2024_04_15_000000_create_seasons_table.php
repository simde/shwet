<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Enums\Season as SeasonEnum;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('seasons', function (Blueprint $table) {
            $table->id()->comment('Unique ID of each season');
            $table->enum('code', SeasonEnum::values())->unique()->comment('Unique code of each season');
        });
        DB::statement("ALTER TABLE seasons COMMENT = 'Table containing all seasons';");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('seasons');
    }
};
