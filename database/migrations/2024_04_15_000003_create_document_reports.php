<?php

use App\Enums\ReportStatus as ReportStatusEnum;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('document_reports', function (Blueprint $table) {
            $table->id()->comment('Unique ID of each document report');
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
            $table->string('comment', 800)->comment('Reason the document report');
            $table->timestamp('commented_at')->comment('Date and time the document was commented on by the user');
            $table->string('response', 800)->nullable()->comment('Admin response to the document report');
            $table->timestamp('responded_at')->nullable()->comment('Date when the document report was responded to by an admin');
            $table->enum('status', ReportStatusEnum::values())->default(ReportStatusEnum::PENDING->value)->comment('Status of each document report');
            $table->foreignId('document_id')->constrained()->comment('ID of the document reported');
            $table->foreignUuid('user_id')->constrained()->comment('ID of the user who reported the document');
        });
        DB::statement("ALTER TABLE document_reports COMMENT = 'Table containing all document reports';");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('document_reports');
    }
};
