<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('favourite_courses', function (Blueprint $table) {
            $table->foreignId('course_id')->constrained()->comment('Unique ID of the associated course');
            $table->foreignUuid('user_id')->constrained()->comment('Unique ID of the associated user');
            $table->primary(['course_id', 'user_id']);
        });
        DB::statement("ALTER TABLE favourite_courses COMMENT = 'Table containing all favourite courses of users';");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('favourite_courses');
    }
};
