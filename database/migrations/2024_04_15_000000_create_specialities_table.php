<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('specialities', function (Blueprint $table) {
            $table->id()->comment('Unique ID of each speciality');
            $table->string('code', 10)->unique()->comment('Unique code of each speciality');
            $table->string('label', 200)->unique()->comment('Unique label of each speciality');
        });
        DB::statement("ALTER TABLE specialities COMMENT = 'Table containing all specialities';");
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('specialities');
    }
};
