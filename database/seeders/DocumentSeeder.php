<?php

namespace Database\Seeders;

use App\Models\Document;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Storage;
use App\Enums\Format as FormatEnum;

class DocumentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Document::factory(100)->create()->each(function ($document) {
            if ($document->format->value == FormatEnum::FILE->value) {
                $newPath = $document->id . '-' . $document->path;
                $document->path = $newPath;
                $document->save();
                Storage::disk('public')->put('documents/' . $newPath, '');
            }
        });
    }
}
