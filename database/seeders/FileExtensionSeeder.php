<?php

namespace Database\Seeders;

use App\Models\FileExtension;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class FileExtensionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $fileExtensions = [
            "5",
            "7z",
            "apkg",
            "asm",
            "c",
            "doc",
            "docx",
            "gif",
            "h",
            "htm",
            "html",
            "ico",
            "ipynb",
            "jpeg",
            "jpg",
            "lisp",
            "m",
            "md",
            "mp3",
            "mp4",
            "odg",
            "odp",
            "ods",
            "odt",
            "pages",
            "pas",
            "pdf",
            "php",
            "png",
            "pps",
            "ppt",
            "pptm",
            "pptx",
            "py",
            "stx",
            "tex",
            "txt",
            "rar",
            "webp",
            "xls",
            "xlsx",
            "zip"
        ];

        foreach ($fileExtensions as $fileExtension) {
            FileExtension::create(['suffix' => $fileExtension]);
        }
    }
}
