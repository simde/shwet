<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Http\Controllers\CourseController;
use App\Http\Controllers\DocumentController;
use App\Models\Season;
use App\Models\Period;
use App\Models\User;
use App\Models\License;
use Illuminate\Support\Facades\File;
use Exception;

class OldDataSeeder extends Seeder
{
    public function run()
    {
        // Path to the old data directory, defaulting to 'database/old_data/' if not set in .env
        $oldData = config('filesystems.storage_path.old_data', 'database/seeders/old_data/');

        // Check if the old data directory exists
        if (is_dir($oldData)) {
            // Define the paths to the old UVs JSON file and old documents directory
            $oldUVs = $oldData . 'old_uvs.json';
            $oldDocuments = $oldData . 'old_documents/';

            // Check if the old UVs JSON file exists
            if (file_exists($oldUVs)) {
                // Decode the JSON data from the file
                $jsonData = json_decode(file_get_contents($oldUVs), true);

                $courseController = new CourseController();

                // Loop through each course in the JSON data
                foreach ($jsonData as $code => $courseData) {
                    // If 'Type' is empty, set it to NULL
                    if ($courseData['Type'] == "") {
                        $courseData['Type'] = NULL;
                    }
                    // Create a new UV (course) record using the CourseController
                    $courseController->createUV($code, $courseData);
                }

                // Delete the old UVs JSON file after processing
                unlink($oldUVs);
            }

            // Check if the old documents directory exists
            if (is_dir($oldDocuments)) {
                // Get all files in the old documents directory
                $files = File::files($oldDocuments);

                $documentController = new DocumentController();

                // Get the ID of the first user in the database
                $userId = User::first()->id;

                // Loop through each file in the directory
                foreach ($files as $file) {
                    // Get the filename and extension
                    $filename = pathinfo($file, PATHINFO_FILENAME);
                    $extension = pathinfo($file, PATHINFO_EXTENSION);

                    // Split the filename into parts using '-' as the delimiter
                    $parts = explode('-', $filename);

                    // If the filename contains 5 parts, assign them to respective variables
                    if (count($parts) == 5) {
                        [$id, $courseCode, $type, $activity, $period] = $parts;
                    } elseif (count($parts) == 6) {
                        // If the filename contains 6 parts, handle it differently
                        [$id, $courseCode, $type, $activityPart1, $activityPart2, $period] = $parts;
                        $activity = $activityPart1 . '-' . $activityPart2;
                    }

                    // Get the season ID from the first character of the period
                    $seasonId = Season::where('code', substr($period, 0, 1))->first()->id;
                    // Get the period ID using the year and season ID
                    $periodId = Period::where('year', substr($period, 1))
                        ->where('season_id', $seasonId)
                        ->first()
                        ->id;
                    // Get the license ID for the CC0 license
                    $licenseId = License::where('label', 'CC0')->first()->id;

                    // Create a new document record using the DocumentController
                    $documentController->createDocument(
                        $link = null,
                        $type,
                        $activity,
                        $format = 'file',
                        $anonymous = 1,
                        strtoupper($courseCode),
                        $userId,
                        $periodId,
                        $licenseId,
                        $file,
                        $id,
                        $title = null,
                    );

                    // Delete the file after processing
                    unlink($file);
                }

                // Delete the old documents directory after all files are processed
                rmdir($oldDocuments);
            }

            // Delete the old data directory after all its contents are processed
            rmdir($oldData);
        } else {
            throw new Exception(__("Old data cannot be seeded !"));
        }
    }
}
