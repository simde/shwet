<?php

namespace Database\Seeders;

use App\Models\Period;
use App\Models\Season;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PeriodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $currentYear = date('Y');

        $seasons = Season::all();

        $seasonIds = $seasons->filter(function ($season) {
            return $season->code->value === 'A' || $season->code->value === 'P';
        })->pluck('id');

        for ($year = 1972; $year <= $currentYear; $year++) {
            foreach ($seasonIds as $seasonId) {
                Period::create([
                    'year' => $year,
                    'season_id' => $seasonId,
                ]);
            }
        }
    }
}
