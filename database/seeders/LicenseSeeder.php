<?php

namespace Database\Seeders;

use App\Models\License;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class LicenseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $licenses = [
            [
                'label' => 'CC0',
                'title' => 'Creative Commons Zero',
                'description' => 'Allows authors to waive all rights and place the work in the public domain without attribution requirement.',
                'url' => 'https://creativecommons.org/publicdomain/zero/1.0/'
            ],
            [
                'label' => 'CC-BY',
                'title' => 'Creative Commons Attribution',
                'description' => 'Allows redistribution and modification as long as the author is credited.',
                'url' => 'https://creativecommons.org/licenses/by/4.0/'
            ],
            [
                'label' => 'CC-BY-SA',
                'title' => 'Creative Commons Attribution-ShareAlike',
                'description' => 'Allows redistribution and modification as long as the author is credited and the same license is applied.',
                'url' => 'https://creativecommons.org/licenses/by-sa/4.0/'
            ],
            [
                'label' => 'CC-BY-NC',
                'title' => 'Creative Commons Attribution-NonCommercial',
                'description' => 'Allows redistribution and modification for non-commercial purposes as long as the author is credited.',
                'url' => 'https://creativecommons.org/licenses/by-nc/4.0/'
            ],
            [
                'label' => 'CC-BY-NC-SA',
                'title' => 'Creative Commons Attribution-NonCommercial-ShareAlike',
                'description' => 'Allows redistribution and modification for non-commercial purposes as long as the author is credited and the same license is applied.',
                'url' => 'https://creativecommons.org/licenses/by-nc-sa/4.0/'
            ],
            [
                'label' => 'CC-BY-ND',
                'title' => 'Creative Commons Attribution-NoDerivatives',
                'description' => 'Allows redistribution as long as the author is credited and the work is not modified.',
                'url' => 'https://creativecommons.org/licenses/by-nd/4.0/'
            ],
            [
                'label' => 'CC-BY-NC-ND',
                'title' => 'Creative Commons Attribution-NonCommercial-NoDerivatives',
                'description' => 'Allows redistribution for non-commercial purposes as long as the author is credited and the work is not modified.',
                'url' => 'https://creativecommons.org/licenses/by-nc-nd/4.0/'
            ],
            [
                'label' => 'GNU GPL',
                'title' => 'GNU General Public License',
                'description' => 'License for free software, requires that modifications are also licensed under GPL.',
                'url' => 'https://www.gnu.org/licenses/gpl-3.0.html'
            ],
            [
                'label' => 'MIT',
                'title' => 'MIT License',
                'description' => 'Permissive license that allows use, modification, and distribution without restrictions.',
                'url' => 'https://opensource.org/licenses/mit'
            ],
            [
                'label' => 'EUPL',
                'title' => 'European Union Public License',
                'description' => 'Open source license supported by the European Commission, compatible with other open source licenses.',
                'url' => 'https://eupl.eu'
            ],
            [
                'label' => 'Open License',
                'title' => 'Open License',
                'description' => 'Allows free reuse of public data provided the source is credited.',
                'url' => 'https://www.etalab.gouv.fr/licence-ouverte-open-licence'
            ]
        ];

        foreach ($licenses as $license) {
            License::create($license);
        }
    }
}
