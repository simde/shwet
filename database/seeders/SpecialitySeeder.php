<?php

namespace Database\Seeders;

use App\Models\Speciality;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SpecialitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $specialities = [
            ["code" => "IM", "label" => "Ingénierie Mécanique"],
            ["code" => "GI", "label" => "Génie Informatique"],
            ["code" => "GB", "label" => "Génie Biologique"],
            ["code" => "GP", "label" => "Génie des Procédés"],
            ["code" => "GU", "label" => "Génie Urbain"],
            ["code" => "HuTech", "label" => "Humanités et Technologie"],
            ["code" => "TC", "label" => "Tronc Commun"],
            ["code" => "ID-M", "label" => "Bachelor Ingénierie Digitale et Management"],
            ["code" => "MTSP", "label" => "Maintenance et technologie : systèmes pluritechniques"],
            ["code" => "Major A", "label" => "Knowledge, innovation and the digital transition"],
            ["code" => "Major B", "label" => "Macroeconomics and finance for the socioeconomic transition"],
            ["code" => "Major C", "label" => "Development, sustainable development and ecological transition policies"],
            ["code" => "CH", "label" => "Chimie"],
            ["code" => "HIC", "label" => "Humanités et industries créatives"],
            ["code" => "ISC", "label" => "Ingénierie des systèmes complexes"],
            ["code" => "IdS", "label" => "Ingénierie de la santé"],
            ["code" => "Autres", "label" => "Autres spécialités"],
        ];

        foreach ($specialities as $speciality) {
            Speciality::create($speciality);
        }
    }
}
