<?php

namespace Database\Seeders;

use App\Models\Course;
use App\Models\User;
use Illuminate\Database\Seeder;

class FavoriteCoursesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $users = User::all();

        // attach random courses to users
        foreach ($users as $user) {
            $user->courses()->attach(
                Course::inRandomOrder()->take(rand(1,4))->pluck('id')
            );
        }
    }
}
