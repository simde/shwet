<?php

namespace Database\Seeders;

use App\Models\Course;
use App\Models\Speciality;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CourseSpecialitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $courses = Course::all();

        // attach random specialities to courses
        foreach ($courses as $course) {
            $course->specialities()->attach(
                Speciality::inRandomOrder()->take(rand(1,4))->pluck('id')
            );
        }
    }
}
