<?php

namespace Database\Seeders;

use App\Models\Season;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Enums\Season as SeasonEnum;

class SeasonSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $seasons = SeasonEnum::cases();

        foreach ($seasons as $season) {
            Season::create([
                'code' => $season->value,
            ]);
        }
    }
}
