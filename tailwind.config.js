import defaultTheme from 'tailwindcss/defaultTheme';
import forms from '@tailwindcss/forms';

/** @type {import('tailwindcss').Config} */
export default {
    content: [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
        "./node_modules/flowbite/**/*.js",
    ],

    theme: {
        extend: {
            colors: {
                'purple': {
                    400: '#AC94FA',
                    500: '#9061F9',
                },
                'grey': {
                    50: '#F6F7F9',
                    100: '#EDEEF1',
                    200: '#D7DBE0',
                    300: '#B4BBC5',
                    400: '#8B96A5',
                    500: '#6C798B',
                    600: '#576272',
                    700: '#474F5D',
                    800: '#3D444F',
                    900: '#363B44',
                    950: '#24272D',
                },
            },
            fontFamily: {
                sans: ['Figtree', ...defaultTheme.fontFamily.sans],
                'annie-use-your-telescope': 'Annie Use Your Telescope',
            },
            spacing: {
                '28': '6.5rem',
                '38': '9.5rem',
            },
        },
    },

    plugins: [
        forms,
        require('flowbite/plugin'),
    ],

    darkMode: 'class',
};
