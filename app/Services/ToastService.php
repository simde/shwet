<?php

namespace App\Services;

use App\Helpers\Flash;
use App\Models\ToastMessage;
use App\Enums\ToastMessage as ToastMessageEnum;

class ToastService
{
    /**
     * Add a toast message to the session.
     *
     * @param string $message
     * @param ToastMessageEnum $type
     */
    public static function addMessage(string $message, ToastMessageEnum $type)
    {
        $toastMessage = new ToastMessage(['message' => $message, 'type' => $type]);
        Flash::push('messages', $toastMessage);
    }
}
