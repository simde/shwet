<?php

namespace App\Enums;

use App\Traits\EnumMethods;

enum CourseType: string
{
    use EnumMethods;
    
    case CS = 'CS';

    case TM = 'TM';

    case TSH = 'TSH';

    case ST = 'ST';
}