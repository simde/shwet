<?php

namespace App\Enums;

use App\Attributes\Label;
use App\Traits\AttributableEnum;
use App\Traits\EnumMethods;

enum Role: string
{
    use EnumMethods, AttributableEnum;

    #[Label('User')]
    case USER = 'user';

    #[Label('Administrator')]
    case ADMINISTRATOR = 'administrator';

    #[Label('Moderator')]
    case MODERATOR = 'moderator';
}