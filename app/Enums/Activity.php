<?php

namespace App\Enums;

use App\Attributes\Label;
use App\Traits\AttributableEnum;
use App\Traits\EnumMethods;

enum Activity: string
{
    use EnumMethods, AttributableEnum;

    #[Label('Midterm')]
    case MIDTERM = 'midterm';

    #[Label('Final')]
    case FINAL = 'final';

    #[Label('Lecture')]
    case LECTURE = 'lecture';

    #[Label('Tutorial')]
    case TUTORIAL = 'tutorial';

    #[Label('Practical')]
    case PRACTICAL = 'practical';

    #[Label('Report')]
    case REPORT = 'report';

    #[Label('Revision sheet')]
    case REVISION_SHEET = 'revision-sheet';

    #[Label('Other')]
    case OTHER = 'other';

    #[Label('Drive')]
    case DRIVE = 'drive';

    /**
     * The method returned only allowed activities.
     * 
     */
    public static function allowedActivities(): array
    {
        return array_values(array_filter(self::cases(), function ($case) {
            return $case !== self::DRIVE;
        }));
    }

    /**
     * The method returned only allowed activities values.
     * 
     */
    public static function allowedActivitiesValues(): array
    {
        return array_map(function ($case) {
            return $case->value;
        }, self::allowedActivities());
    }
}