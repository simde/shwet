<?php

namespace App\Enums;

use App\Attributes\Label;
use App\Traits\AttributableEnum;
use App\Traits\EnumMethods;

enum Format: string
{
    use EnumMethods, AttributableEnum;

    #[Label('File')]
    case FILE = 'file';

    #[Label('Link')]
    case LINK = 'link';
}