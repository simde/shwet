<?php

namespace App\Enums;

use App\Attributes\Label;
use App\Traits\AttributableEnum;
use App\Traits\EnumMethods;

enum Season: string
{
    use EnumMethods, AttributableEnum;

    #[Label('Spring')]
    case SPRING = 'P';

    #[Label('Summer')]
    case SUMMER = 'E';

    #[Label('Autumn')]
    case AUTUMN = 'A';

    #[Label('Winter')]
    case WINTER = 'H';
}