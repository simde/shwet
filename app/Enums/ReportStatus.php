<?php

namespace App\Enums;

use App\Attributes\Label;
use App\Traits\AttributableEnum;
use App\Traits\EnumMethods;

enum ReportStatus: string
{
    use EnumMethods, AttributableEnum;

    // waiting for admin to review
    #[Label('Pending')]
    case PENDING = 'pending';

    // precessed by the admin
    #[Label('Processed')]
    case PROCESSED = 'processed';

    // rejected by the admin
    #[Label('Rejected')]
    case REJECTED = 'rejected';
}
