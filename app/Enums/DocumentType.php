<?php

namespace App\Enums;

use App\Attributes\Label;
use App\Traits\AttributableEnum;
use App\Traits\EnumMethods;

enum DocumentType: string
{
    use EnumMethods, AttributableEnum;

    #[Label('Subject')]
    case SUBJECT = 'subject';

    #[Label('Correction')]
    case CORRECTION = 'correction';

    #[Label('Personal')]
    case PERSONAL = 'personal';

    #[Label('Other')]
    case OTHER = 'other';
}