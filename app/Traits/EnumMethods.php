<?php

namespace App\Traits;

trait EnumMethods
{
    /**
     * The method returned all values.
     * 
     */
    public static function values(): array
    {
        return array_map(function ($case) {
            return $case->value;
        }, self::cases());
    }
}