<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Session;

class Flash
{
    /**
     * Add a flash value to a session.
     *
     * @param $key
     * @param $value
     * @return void
     */
    public static function push($key, $value): void
    {
        $values = Session::get($key, []);
        $values[] = $value;
        Session::flash($key, $values);
    }
}
