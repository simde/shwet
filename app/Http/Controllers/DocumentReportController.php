<?php

namespace App\Http\Controllers;

use App\Enums\ReportStatus as ReportStatusEnum;
use App\Enums\ToastMessage as ToastMessageEnum;
use App\Mail\MailNotification;
use App\Models\DocumentReport;
use App\Services\ToastService;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Mail;

class DocumentReportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return View
     */
    public function index(Request $request): View
    {
        $reports = null;

        // If user is admin, show all document reports
        if ($request->route('admin') && auth()->user()->isAdmin()) {
            // Get reports ordered by the most recent first
            $reports = DocumentReport::orderBy('created_at', 'desc')->get();
        }
        else {
            // Get reports ordered by the most recent first
            $reports = DocumentReport::where('user_id', auth()->id())->orderBy('created_at', 'desc')->get();
        }
        return view('document-report.index')->with('reports', $reports);
    }

    public function store(Request $request): RedirectResponse
    {
        $report = new DocumentReport();
        $report->user_id = auth()->id();
        $report->document_id = $request->document_id;

        // Check if the comment is valid
        try {
            $this->checkCommentValidity($request->comment);
        } catch (Exception $e) {
            ToastService::addMessage(__('Failed to send the report : ').$e->getMessage(), ToastMessageEnum::ERROR);
            return redirect()->back();
        }

        $report->setComment($request->comment);
        $report->save();

        ToastService::addMessage(__('Report successfully submitted'), ToastMessageEnum::SUCCESS);

        // send mail to SiMDE
        $details = [
            'reportAuthor' => auth()->user(),
            'reportCreatedAt' => $report->created_at,
            'message' => $request->comment
        ];

        $recipient = config('mail.simde.address');
        Mail::to($recipient)->send(new MailNotification($details));

        info('mail sent to '.$recipient.' with details: '.json_encode($details));

        return redirect()->back();
    }

    /**
     * Save the document report changes
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     */
    public function update(Request $request, $id): RedirectResponse
    {
        $report = DocumentReport::find($id);

        if (auth()->user()->isAdmin()) {
            $response = $request->response;
            $status = $request->status;

            $this->updateAdmin($report, $response, $status);
        }
        if (auth()->id() === $report->user_id) {
            $this->updateOwner($report, $request->comment);
        }

        return redirect()->back();
    }

    /**
     * Update the report status and response for admin only
     * @param $report
     * @param $reponse
     * @param $status
     * @return void
     */
    private function updateAdmin($report, $response, $status): void {
        // Check if the status value exists in the enum
        if (!in_array($status, ReportStatusEnum::values())) {
            ToastService::addMessage(__('Invalid report status'), ToastMessageEnum::ERROR);
            return;
        }

        // Check if the response is not too long
        if (strlen($response) > 800) {
            ToastService::addMessage(__('Response is too long (max 800 characters)'), ToastMessageEnum::ERROR);
            return;
        }

        // Check if the response is required for rejected reports
        if ($status === ReportStatusEnum::REJECTED && !$response) {
            ToastService::addMessage(__('Response is required for rejected reports'), ToastMessageEnum::ERROR);
            return;
        }

        $report->setResponse($response);
        $report->status = $status;

        $report->save();

        ToastService::addMessage(__('Report response and status successfully updated'), ToastMessageEnum::SUCCESS);
    }


    /**
     * Update the report comment for the owner only
     * @param $report
     * @param $comment
     * @return void
     */
    private function updateOwner($report, $comment): void
    {
        if($report->status !== ReportStatusEnum::PENDING->value) {
            ToastService::addMessage(__('Cannot update comment for non-pending reports'), ToastMessageEnum::ERROR);
            return;
        }

        // Check if the comment is valid
        try {
            $this->checkCommentValidity($comment);
        } catch (Exception $e) {
            ToastService::addMessage(__('Failed to update the report : ').$e->getMessage(), ToastMessageEnum::ERROR);
            return;
        }

        $report->setComment($comment);
        $report->save();

        ToastService::addMessage(__('Report comment successfully updated'), ToastMessageEnum::SUCCESS);
    }

    /**
     * @throws Exception
     */
    private function checkCommentValidity($comment): void {
        if (strlen($comment) > 800) {
            throw new Exception(__('Comment is too long (max 800 characters)'));
        }
        if (empty($comment)) {
            throw new Exception(__('Comment must not be empty'));
        }
    }
}
