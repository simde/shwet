<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Services\ToastService;
use App\Enums\ToastMessage as ToastMessageEnum;

class FavouriteCourseController extends Controller
{
    /**
     * Remove a specific UV from connected user's favourites list
     * @param $code
     * @return \Illuminate\Http\RedirectResponse
     */
        public function removeFavouriteCourse ($code): \Illuminate\Http\RedirectResponse
        {

            // Retrieve the course
            $course = Course::where('code', $code)->first();

            // Remove the course from the user's favourite courses
            auth()->user()->courses()->detach($course->id);

            ToastService::addMessage(__(":code has successfully been removed from your favourites.", ['code' => $code]), ToastMessageEnum::SUCCESS);

            // Redirect to the previous page
            return redirect()->back();

        }

    /**
     * Add a specific UV to connected user's favourites list
     * @param $code
     * @return \Illuminate\Http\RedirectResponse
     */
        public function addFavouriteCourse ($code): \Illuminate\Http\RedirectResponse
        {

            // Retrieve the course
            $course = Course::where('code', $code)->first();

            // Add the course to the user's favourite courses
            auth()->user()->courses()->attach($course->id);

            ToastService::addMessage(__(":code has successfully been added to your favourites.", ['code' => $code]), ToastMessageEnum::SUCCESS);

            // Redirect to the previous page
            return redirect()->back();

        }
}
