<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Enums\Role as RoleEnum;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Create or update a user in the database
     *
     * @param $userDetails
     * @param $currentAssociations
     * @return User
     */
    public function createOrUpdateUser($userDetails, $currentAssociations)
    {
        $role = RoleEnum::USER->value;

        $userRoles = json_decode(file_get_contents(storage_path(config('filesystems.storage_path.user_roles'))), true);

        if ($currentAssociations != null) {
            foreach ($currentAssociations as $association) {
                foreach ($userRoles as $userRole) {
                    if ($association['login'] === $userRole['association'] && $association['user_role']['type'] === $userRole['role']) {
                        $role = RoleEnum::ADMINISTRATOR->value;
                        break 2;
                    }
                }
            }
        }

        $user = User::updateOrCreate(
            ['id' => $userDetails['uuid']],
            [
                'firstname' => $userDetails['firstName'],
                'lastname' => $userDetails['lastName'],
                'email' => $userDetails['email'],
                'role' => $role,
            ]
        );

        return $user;
    }

    /**
     * Update the dark theme preference of the authenticated user
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function updateDarkThemePreference(Request $request)
    {
        // Retrieve the dark theme preference from the request
        $jsonData = $request->json()->all();
        $darkTheme = $jsonData['darkTheme'];

        // Find the user by ID
        $user = User::find(Auth::user()->id);
        $user->dark_theme = $darkTheme;
        $user->save();

        return response('', 200);
    }

    /**
     * Update language preference of the authenticated user
     *
     * @param $language
     */
    public static function updateLanguagePreference($language)
    {
        // Find the user by ID
        $user = User::find(Auth::user()->id);
        $user->language = $language;
        $user->save();

        return response('', 200);
    }
}
