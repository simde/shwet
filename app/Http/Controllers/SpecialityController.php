<?php

namespace App\Http\Controllers;

use App\Services\ToastService;
use App\Enums\ToastMessage as ToastMessageEnum;
use Illuminate\Http\Request;
use App\Models\Speciality;

class SpecialityController extends Controller
{
    /**
     * Initialize specialities by saving and updating data fetched from the API to the database.
     *
     * @param  \App\Http\Controllers\UTCrawlController  $utcrawlController
     * @param  boolean  $isIndivualCall
     * @return \Illuminate\Http\Response
     */
    public function manageSpecialities(UTCrawlController $utcrawlController, $isIndivualCall = true)
    {
        // Fetch diplomas data from the API
        $diplomasData = json_decode($utcrawlController->fetchDataFrom('/diplomas'), true);

        // Insert default speciality into the SPECIALITIES table
        $speciality = Speciality::firstOrCreate(['code' => "Autres"], ['label' => "Autres spécialités"]);

        // Insert specialities into the SPECIALITIES table
        foreach ($diplomasData as $diploma) {
            foreach ($diploma as $specialityCode => $specialityLabel) {
                // Find or create speciality based on code and label
                $speciality = Speciality::firstOrCreate(['code' => $specialityCode], ['label' => $specialityLabel]);
            }
        }

        if ($isIndivualCall) {
            ToastService::addMessage('Specialities data managed successfully in the database.', ToastMessageEnum::SUCCESS);

            return response()->json(['message' => 'Specialities data managed successfully in the database.'], 200);
        } else {
            return true;
        }
    }
}
