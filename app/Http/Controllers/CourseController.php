<?php

namespace App\Http\Controllers;

use App\Services\ToastService;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Course;
use App\Models\Speciality;
use App\Models\Season;
use App\Models\Document;
use App\Models\ToastMessage;
use App\Enums\ToastMessage as ToastMessageEnum;
use App\Enums\CourseType as CourseTypeEnum;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\View\View
     */
    public function index(Request $request): \Illuminate\View\View
    {
        // Retrieve courses based on filters
        $courses = Course::query()->with('seasons')
                         ->filterByType($request->input('types'))
                         ->filterBySpeciality($request->input('speciality_codes'))
                         ->filterBySeason($request->input('season_codes'))
                         ->filterByStatus($request->input('statuses'))
                         ->get();

        // Retrieve distinct course types
        $types = CourseTypeEnum::cases();

        // Retrieve all specialities
        $specialities = Speciality::all();

        // Retrieve all seasons
        $seasons = Season::uv()->get();

        // Retrieve filters
        $filters = $request->all();

        return view('courses.index', compact('courses', 'types', 'specialities', 'seasons', 'filters'));
    }

    /**
     * Display the specified resource.
     *
     * @param  str  $code
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\View\View
     */
    public function show($code, Request $request): \Illuminate\View\View
    {
        // Find the course with the given code if it exists
        $course = Course::where('code', $code)->firstOrFail();

        // Retrieve and filter documents based on request parameters
        $documents = Document::where('course_id', $course->id)
                             ->with(['period', 'period.season'])
                             ->filterBySeason($request->input('season_codes'))
                             ->get();

        // Retrieve all seasons
        $seasons = Season::uv()->get();

        // Retrieve filters
        $filters = $request->all();

        // Return the course as a response
        return view('courses.show', compact('course', 'documents', 'seasons', 'filters'));
    }

    /**
     * Manage UVs by saving and updating data fetched from the UTCrawl API to the database.
     *
     * @param  \App\Http\Controllers\UTCrawlController  $utcrawlController
     * @param  \App\Http\Controllers\SpecialityController  $specialityController
     * @return \Illuminate\Http\Response
     */
    public function manageUVs(UTCrawlController $utcrawlController, SpecialityController $specialityController): \Illuminate\Http\Response
    {
        // Increase execution time
        ini_set('max_execution_time', 600);

        // Fetch latest Diplomas data from the UTCrawl API
        $successManageSpecialities = $specialityController->manageSpecialities($utcrawlController, false);

        if (!$successManageSpecialities) {
            ToastService::addMessage('Failed to manage specialities data in the database.', ToastMessageEnum::ERROR);
            return response()->json(['message' => 'Failed to manage specialities data in the database.'], 500);
        }

        // Fetch latest UVs data from the UTCrawl API
        $latestUVs = json_decode($utcrawlController->fetchDataFrom('/uvs'), true);

        // Recover existing UVs data from the database
        $existingUVs = Course::with('specialities')->get();

        // Process changes in the UVs and update the database accordingly
        foreach ($existingUVs as $existingUV) {
            if (!isset($latestUVs[$existingUV['code']])) {
                // UV is considered archived
                $this->archiveUV($existingUV['code']);
            } else {
                // UV already exists, check for modifications
                if ($this->isModified($existingUV, $latestUVs[$existingUV['code']])) {
                    $this->updateUV($existingUV['code'], $latestUVs[$existingUV['code']]);
                }

                // UV is considered active
                if ($this->isArchived($existingUV['code'])) {
                    $this->activeUV($existingUV['code']);
                }

                // Remove UV from latest data to track new UVs
                unset($latestUVs[$existingUV['code']]);
            }
        }

        // Any remaining UVs in latest data are considered newly
        foreach ($latestUVs as $uvCode => $uvData) {
            $this->createUV($uvCode, $uvData);
        }

        ToastService::addMessage('Specialities and UVs data managed successfully in the storage.', ToastMessageEnum::SUCCESS);

        return response()->json(['message' => 'Specialities and UVs data managed successfully in the storage.'], 200);
    }

    /**
     * Create a new UV in the database.
     *
     * @param string $uvCode The code of the UV.
     * @param array $uvData The data associated with the UV.
     *                      Contains 'Titre' (title), 'Type' (type), 'Spécialités' (specialities), and 'Semestres' (seasons).
     */
    public function createUV($uvCode, $uvData)
    {
        // Create or retrieve the course based on the UV code
        $course = Course::firstOrCreate([
            'code' => $uvCode,
            'title' => $uvData['Titre'],
            'type' => $uvData['Type'],
        ]);

        // Attach specialities and seasons to the course
        $this->attachSpecialities($course, $uvData['Spécialités']);
        $this->attachSeasons($course, $uvData['Semestres']);
    }

    /**
     * Update an existing UV in the database.
     *
     * @param string $uvCode The code of the UV to update.
     * @param array $uvData The updated data associated with the UV.
     *                      Contains 'Titre' (title), 'Type' (type), 'Spécialités' (specialities), and 'Semestres' (seasons).
     */
    private function updateUV($uvCode, $uvData)
    {
        // Find the course based on the UV code
        $course = Course::where('code', $uvCode)->first();
        $course->title = $uvData['Titre'];
        $course->type = $uvData['Type'];
        $course->save();

        // Attach specialities and seasons to the course if necessary
        $this->attachSpecialities($course, $uvData['Spécialités']);
        $this->attachSeasons($course, $uvData['Semestres']);
    }

    /**
     * Archive a UV in the database.
     *
     * @param string $uvCode The code of the UV to archive.
     */
    private function archiveUV($uvCode)
    {
        // Find the course based on the UV code and archive it
        $course = Course::where('code', $uvCode)->first();
        if ($course) {
            $course->archived_at = Carbon::now();
            $course->save();
        }
    }

    /**
     * Activate an archived UV in the database.
     *
     * @param string $uvCode The code of the UV to activate.
     */
    private function activeUV($uvCode)
    {
        // Find the course based on the UV code and archive it
        $course = Course::where('code', $uvCode)->first();
        if ($course) {
            $course->archived_at = null;
            $course->save();
        }
    }

    /**
     * Check if a UV is archived.
     *
     * @param string $uvCode The code of the UV to check.
     * @return bool Returns true if the UV is archived, otherwise false.
     */
    private function isArchived($uvCode): bool
    {
        // Find the course based on the UV code and check if it is archived
        $course = Course::where('code', $uvCode)->first();
        if ($course) {
            return $course->archived_at !== null;
        }
    }

    /**
     * Check if UV data has been modified.
     *
     * @param array $existingData The existing data associated with the UV.
     * @param array $newData The new data associated with the UV.
     * @return bool Returns true if UV data has been modified, otherwise false.
     */
    private function isModified($existingData, $newData)
    {
        return $existingData['title'] !== $newData['Titre'] || 
           $existingData['type'] !== $newData['Type'] || 
           !$this->arraysAreEqual($existingData['specialities']->pluck('code')->toArray(), $newData['Spécialités']) || 
           !$this->arraysAreEqual($existingData['seasons']->pluck('label')->toArray(), $newData['Semestres']);
    }

    /**
     * Check if two arrays are equal.
     *
     * @param array $array1 The first array to compare.
     * @param array $array2 The second array to compare.
     * @return bool Returns true if the arrays are equal, otherwise false.
     */
    private function arraysAreEqual(array $array1, array $array2)
    {
        sort($array1);
        sort($array2);
        return $array1 === $array2;
    }

    /**
     * Attach specialities to the given course.
     *
     * @param Course $course The course to which specialities will be attached.
     * @param array $specialityCodes The codes of the specialities to attach.
     * @return void
     */
    private function attachSpecialities($course, $specialityCodes)
    {
        // Retrieve the 'Autres' speciality or create it if it doesn't exist
        $othersSpeciality = Speciality::firstOrCreate(['code' => "Autres"], ['label' => "Autres spécialités"]);

        if (empty($specialityCodes)) {
            // Attach the 'Autres' speciality to the course
            $course->specialities()->syncWithoutDetaching([$othersSpeciality->id]);
        } else {
            // Find the specialities associated with the provided speciality codes
            $specialities = Speciality::whereIn('code', $specialityCodes)->pluck('id');
            
            // Attach specialities that are not already attached to the course
            $course->specialities()->syncWithoutDetaching($specialities);

            // Detach the 'Autres' speciality from the course if it is not necessary
            if ($specialities->contains($othersSpeciality->id)) {
                $course->specialities()->detach($othersSpeciality->id);
            }
        }
    }

    /**
     * Attach seasons to the given course.
     *
     * @param Course $course The course to which seasons will be attached.
     * @param array $seasonLabels The labels of the seasons to attach.
     */
    private function attachSeasons($course, $seasonLabels)
    {
        // Retrieve the ids of the seasons already attached to the course
        $seasonCodes = array_map(fn($label) => mb_substr($label, 0, 1), $seasonLabels);

        // Find the seasons associated with the provided season codes
        $seasons = Season::whereIn('code', $seasonCodes)->pluck('id');

        // Attach seasons that are not already attached to the course
        $course->seasons()->syncWithoutDetaching($seasons);
    }

    /**
     * Api method that returns a list of all courses code.
     * @return \Illuminate\Http\JsonResponse
     */
    public function list(): \Illuminate\Http\JsonResponse
    {
        // Retrieve all courses
        $courses = Course::all(["code"]);

        return response()->json($courses);
    }

}
