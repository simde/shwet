<?php

namespace App\Http\Controllers;

use App\Services\ToastService;
use App\Enums\ToastMessage as ToastMessageEnum;
use Exception;
use Illuminate\Support\Facades\Http;

class UTCrawlController extends Controller
{
    /**
     * Fetch data from the UTCrawl API endpoint.
     *
     * @param  string  $endpoint
     * @return mixed
     */
    public function fetchDataFrom(string $endpoint)
    {
        $apiBaseUrl = config('services.utcrawl.url');
        $apiKey = config('services.utcrawl.key');

        try {
            $response = Http::withHeaders([
                'x-api-key' => $apiKey,
            ])->timeout(300)->get($apiBaseUrl . $endpoint);
            return $response->body();
        } catch (Exception $e) {
            // Check if an exception occurred and log the error information
            ToastService::addMessage('An exception occurred while fetching data from the UTCrawl API. Please check the logs for more information.', ToastMessageEnum::ERROR);
            return response()->json(['error' => 'An exception occurred while fetching data from the UTCrawl API. Please check the logs for more information.'], 500);
        }
    }
}
