<?php

namespace App\Http\Controllers;

use DB;
use Exception;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;
use App\Models\Document;
use App\Models\User;
use App\Models\Course;
use App\Models\Period;
use App\Models\FileExtension;
use App\Models\License;
use App\Enums\ToastMessage as ToastMessageEnum;
use App\Enums\DocumentType as DocumentTypeEnum;
use App\Enums\Activity as ActivityEnum;
use App\Enums\Format as FormatEnum;
use App\Services\ToastService;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\StreamedResponse;

class DocumentController extends Controller
{
    /**
     * Retrieve and display a document.
     *
     * @param string $code The code associated with the document.
     * @param string $document The name of the document.
     * @return StreamedResponse
     */
    public function show($code, $document)
    {
        // Retrieve the base storage path from the environment variable
        $storagePath = config('filesystems.storage_path.documents');

        // Construct the full path
        $filePath = $storagePath . $document;

        // Check if the file exists
        if (Storage::exists($filePath)) {
            // Return the file
            return Storage::response($filePath);
        } else {
            // Return an appropriate response if the file does not exist
            abort(404, 'File not found');
        }
    }

    /**
     * Store a newly created document.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        // Extract data from the request
        $courseCode = $request->course;
        list($periodId, $periodCode) = explode(',', $request->period);
        $type = $request->type;
        $activity = $request->activity;
        $format = $request->format;
        $file = $request->file('file');
        $link = $request->link;
        $licenseId = $request->license;

        try {
            $newDocument = $this->createDocument($link, $type, $activity, $format, $request->has('anonymous'),
                $courseCode, auth()->user()->id, $periodId, $licenseId, $file, null, $request->get('title'));
            ToastService::addMessage(__('Document uploaded successfully'), ToastMessageEnum::SUCCESS);
        }
        catch (Exception $e) {
            $errorMessage = $e->getMessage();
            ToastService::addMessage(__("Failed to publish the document: :errorMessage", ["errorMessage" => $errorMessage]), ToastMessageEnum::ERROR);
        }

        return redirect()->back();
    }

    /**
     * Check the validity of the document data.
     *
     * @param string $type The type of the document.
     * @param string $activity The activity associated with the document.
     * @param string $format The format of the document.
     * @param string $courseCode The code of the course associated with the document.
     * @param int $periodId The ID of the period associated with the document.
     * @param int|null $licenseId The ID of the license associated with the document.
     * @param $file The file to be uploaded.
     * @param string $link The link to the document.
     * @param bool $edit Whether the document is being edited or not.
     * @throws Exception
     */
    private function checkDocumentDataValidity(string $type, string $activity, string $format,
                                               string $courseCode, int $periodId, ?int $licenseId, $file, $link, $title, $edit = false): void
    {
        // Retrieve all types, activities, and formats
        $types = DocumentTypeEnum::values();
        $activities = ActivityEnum::allowedActivitiesvalues();
        $formats = FormatEnum::values();

        $errors = [];

        // Check if course in database by code
        if (!Course::where('code', $courseCode)->exists()) {
            $errors[] = __("course");
        }

        // Check if period in database by id
        if (!Period::where('id', $periodId)->exists()) {
            $errors[] = __("semester");
        }

        // Check if type in enum
        if (!in_array($type, $types)) {
            $errors[] = __("type");
        }

        // Check if activity in enum
        if (!in_array($activity, $activities)) {
            $errors[] = __("activity");
        }

        // Check if format in enum
        if (!in_array($format, $formats)) {
            $errors[] = __("format");
        }

        // Check if title is too long
        if ($title && strlen($title) > 200){
            $errors[] = __("title");
        }

        // Check if format is a file, if the extension is not null and exists in the database
        if ($format == FormatEnum::FILE->value) {
            // File is null and creation mode
            if ($file == null && !$edit) {
                $errors[] = __("file");
            }
            // File is not null
            if($file != null) {
                // Check if file extension is in database
                $allowedExtensions = FileExtension::onlyActive()->pluck('suffix')->toArray();
                $file_extension = $this->getFileExtension($file);
                if(!in_array($file_extension, $allowedExtensions)){
                    $errors[] = __("file extension");
                }

                // Check if license is in database
                if (!License::where('id', $licenseId)->exists()) {
                    $errors[] = __("license");
                }
            }
        }
        else {
            if(!$link) {
                $errors[] = __("link");
            }
        }

        if(!empty($errors)) {
            throw new Exception(__("Invalid data: ") . implode(', ', $errors));
        }
    }

    /**
     * Build the path of a document.
     * @param $document
     * @return string
     */
    public function buildDocumentPath($document): string
    {
        return $document->id . '-' . Str::slug($document->course->code) . '-' . $document->type->value . '-' .
               $document->activity->value . '-' . Str::slug($document->period->getLabel()) . '.' .
               $document->fileExtension->suffix;
    }

    /** Store a document file in storage
     *
     * @param $document
     * @param $file
     * @return String path of the stored file
     */
    private function storeDocumentInStorage($document, $file): String {
        $path = $this->buildDocumentPath($document);

        $storagePath = config('filesystems.storage_path.documents');

        if ($file instanceof UploadedFile) {
            $file->storeAs($storagePath, $path);
        } elseif ($file instanceof \SplFileInfo) {
            Storage::put($storagePath . $path, file_get_contents($file->getRealPath()));
        }

        return $path;
    }

    /**
     * Get the file extension of a file.
     *
     * @param $file
     * @return string
     */
    private function getFileExtension($file) {
        if ($file instanceof UploadedFile) {
            return strtolower($file->getClientOriginalExtension());
        } elseif ($file instanceof \SplFileInfo) {
            return strtolower($file->getExtension());
        }
    }

    /**
     * Create a new document instance after data validation and handle format type actions (storage if link).
     *
     * @param string|null $link The link of the document (if applicable).
     * @param string $type The type of the document.
     * @param string $activity The activity associated with the document.
     * @param string $format The format of the document.
     * @param bool $anonymous Whether the document is anonymous or not.
     * @param int $courseCode The course code associated with the document.
     * @param string $userId The ID of the user who created the document.
     * @param int $periodId The ID of the period associated with the document.
     * @param int|null $licenseId The ID of the license associated with the document.
     * @param mixed $file The file to be uploaded.
     * @param int|null $id The ID of the document, or null if there is not.
     * @param string|null $title The title of the document, or null if there is not.
     * @return Document|null The newly created document instance.
     * @throws Exception
     */
    public function createDocument(?string $link, string $type, string $activity, string $format, bool $anonymous,
                                    string $courseCode, string $userId, int $periodId, ?int $licenseId, $file,
                                    ?int $id = null, ?string $title = null): ?Document
    {
        return DB::transaction(function () use ($link, $type, $activity, $format, $anonymous, $courseCode, $userId,
            $periodId, $licenseId, $file, $id, $title) {
            
            // Create course if not exists and the request comes from the OldDataSeeder
            if ($id) {
                $courseId = Course::firstOrCreate(['code' => $courseCode], ['archived_at' => now()])->id;
            } else {
                $courseId = Course::where('code', $courseCode)->first()->id;
            }
            
            $this->checkDocumentDataValidity($type, $activity, $format, $courseCode, $periodId, $licenseId, $file, $link, $title);

            // Prepare document data
            $documentData = [
                'id' => $id,
                'title' => $title,
                'type' => $type,
                'path' => "",
                'activity' => $activity,
                'format' => $format,
                'anonymous' => $anonymous,
                'course_id' => $courseId,
                'user_id' => $userId,
                'period_id' => $periodId,
                'license_id' => $licenseId,
            ];
            
            $newDocument = new Document($documentData);
            $newDocument->save();

            // Handle file or link
            if ($format == FormatEnum::FILE->value) {
                $newDocument->file_extension_id = FileExtension::where('suffix', $this->getFileExtension($file))->first()->id;
                $newDocument->path = $this->storeDocumentInStorage($newDocument, $file);
            }
            else {
                $newDocument->path = $link;
            }

            $newDocument->save();

            return $newDocument;
        });
    }

    /**
     * Download selected documents.
     *
     * This function downloads selected documents for a given course and activity.
     * It can download individual documents or all documents associated with the activity.
     *
     * @param Request $request
     * @return BinaryFileResponse|RedirectResponse
     */
    public function downloadSelectedDocuments(Request $request): BinaryFileResponse|RedirectResponse
    {
        // Retrieve parameters from the request
        $courseCode = $request->input('course_code');
        $activity = $request->input('activity');
        $selectedDocuments = $request->input('selected_documents');

        // Retrieve the base storage path from the environment variable
        $storagePath = config('filesystems.storage_path.documents');

        // Check if any documents are selected
        if (isset($selectedDocuments)) {
            // If 'all' is selected, fetch all documents associated with the activity
            if (in_array('all', $selectedDocuments)) {
                $selectedDocuments = Document::where('course_id', Course::where('code', $courseCode)->first()->id)
                    ->where('activity', $activity)
                    ->where('format', 'file')
                    ->pluck('path');
            }

            // If only one document is selected, directly download it
            if (count($selectedDocuments) == 1) {
                return response()->download(storage_path('app/' . $storagePath . $selectedDocuments[0]));
            }

            // Prepare an array to store paths of selected documents
            $documentsPath = [];
            foreach ($selectedDocuments as $document) {
                $documentsPath[] = storage_path('app/' . $storagePath . $document);
            }

            // If multiple documents are selected, create a zip archive
            $zipFileName = strtolower($courseCode) . '-' . $activity . '.zip';
            $zipFilePath = storage_path('app/' . $storagePath . $zipFileName);

            // Create a new ZipArchive instance
            $zip = new \ZipArchive();
            if ($zip->open($zipFilePath, \ZipArchive::CREATE | \ZipArchive::OVERWRITE) === TRUE) {
                // Add each selected document to the zip archive
                foreach ($documentsPath as $path) {
                    $zip->addFile($path, basename($path));
                }
                $zip->close();
            }

            // Download the zip archive and delete it after sending
            return response()->download($zipFilePath)->deleteFileAfterSend(true);
        } else {
            ToastService::addMessage(__('No document selected'), ToastMessageEnum::ERROR);

            return redirect()->back();
        }
    }

    /**
     * Edit a document data.
     * @param Request $request
     * @return RedirectResponse
     */
    public function edit(Request $request): RedirectResponse
    {
        // Retrieve the document
        $document = Document::find($request->id);

        // Check if the document exists
        if (!$document) {
            ToastService::addMessage(__('The document does not exist'), ToastMessageEnum::ERROR);

            // Redirect to the previous page
            return redirect()->back();
        }

        // Check if the user is authorized to edit the document
        if ($document->user->id != auth()->user()->id
            && !auth()->user()->isAdmin()
            && !auth()->user()->isModerator()) {
            ToastService::addMessage(__('You are not authorized to edit this document'), ToastMessageEnum::ERROR);

            // Redirect to the previous page
            return redirect()->back();
        }

        // Fetch data from the request
        $title = $request->title;
        $link = $request->link;
        $type = $request->type;
        $activity = $request->activity;
        $format = $request->format;
        $anonymous = $request->has('anonymous');
        $courseCode = $request->course;
        list($periodId, $periodCode) = explode(',', $request->period);
        $file = $request->file('file');
        $license = $request->license;

        // Check data validity
        try {
            $this->checkDocumentDataValidity($type, $activity, $format, $courseCode, $periodId, $license, $file, $link, $title, true);

            if($document->format->value == FormatEnum::LINK->value && $format == FormatEnum::FILE->value && !$file){
                throw new Exception(__("File is missing"));
            }
        } catch (Exception $e) {
            $errorMessage = $e->getMessage();
            ToastService::addMessage(__("Failed to update the document : :errorMessage", ["errorMessage" => $errorMessage]), ToastMessageEnum::ERROR);
            return redirect()->back();
        }

        // Update the document data
        $document->title = $title;
        $document->type = $type;
        $document->activity = $activity;
        $document->format = $format;
        $document->anonymous = $anonymous;
        $document->course_id = Course::where('code', $courseCode)->first()->id;
        $document->period_id = $periodId;
        $document->license_id = $license;

        if($format == FormatEnum::FILE->value) {
            if($file) {
                $document->file_extension_id = FileExtension::where('suffix', $file->extension())->first()->id;
                // Delete from storage the old file
                Storage::delete(config('filesystems.storage_path.documents') . $document->path);

                // Store the new file
                $document->path = $this->storeDocumentInStorage($document, $file);
            }
            else {
                $newPath = $this->buildDocumentPath($document);

                // Compare new path with old path
                if($newPath != $document->path){
                    // If different, rename file in storage and update path in db
                    Storage::move(config('filesystems.storage_path.documents') . $document->path,
                                  config('filesystems.storage_path.documents') . $newPath);
                    $document->path = $newPath;
                }
            }
        }
        else if($format == FormatEnum::LINK->value) {
            $document->path = $link;
        }

        $document->save();

        ToastService::addMessage(__('The document has successfully been updated.'), ToastMessageEnum::SUCCESS);

        return redirect()->back();
    }

    /**
     * Soft delete a document.
     *
     * This function soft deletes a document if the authenticated user is the document owner, an admin or the moderator.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function delete(Request $request): RedirectResponse
    {
        // Retrieve the document
        $document = Document::find($request->id);

        // Check if the document exists
        if (!$document) {
            ToastService::addMessage(__('The document does not exist'), ToastMessageEnum::ERROR);

            // Redirect to the previous page
            return redirect()->back();
        }

        // Check if the user is authorized to delete the document
        if ($document->user->id != auth()->user()->id
            && !auth()->user()->isAdmin()
            && !auth()->user()->isModerator()) {
            ToastService::addMessage(__('You are not authorized to delete this document'), ToastMessageEnum::ERROR);

            // Redirect to the previous page
            return redirect()->back();
        }

        // Delete the document
        $document->delete();

        ToastService::addMessage(__('The document has successfully been deleted.'), ToastMessageEnum::SUCCESS);

        // Redirect to the previous page
        return redirect()->back();
    }
}
