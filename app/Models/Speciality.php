<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Speciality extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['code', 'label'];

    /**
     *  The courses that belong to the speciality.
     */
    public function courses(): BelongsToMany
    {
        return $this->belongsToMany(Course::class, 'course_specialities');
    }
}
