<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class DocumentReport extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['comment', 'commented_at', 'response', 'responded_at', 'status', 'deleted_at', 'document_id', 'user_id'];


    /**
     *  Get the user that owns the document.
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     *  Get the document concerned by the report.
     */
    public function document(): BelongsTo
    {
        return $this->belongsTo(Document::class);
    }

    /**
     * Set report comment and update the commented_at timestamp.
     * @param $comment
     * @return void
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
        $this->commented_at = now();
    }

    /**
     * Set report response and update the responded_at timestamp.
     * @param $response
     * @return void
     */
    public function setResponse($response)
    {
        $this->response = $response;
        $this->responded_at = now();
    }
}
