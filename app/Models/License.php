<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class License extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['label', 'title', 'description', 'url'];

    /**
     *  Get the documents for the file extension.
     */
    public function documents(): HasMany
    {
        return $this->hasMany(Document::class);
    }

    /**
     *  Get only instances where deleted_at is null.
     */
    public static function onlyActive()
    {
        return self::whereNull('deleted_at');
    }
}
