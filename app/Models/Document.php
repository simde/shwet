<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Enums\DocumentType as DocumentTypeEnum;
use App\Enums\Activity as ActivityEnum;
use App\Enums\Format as FormatEnum;

class Document extends Model
{
    use HasFactory;
    use SoftDeletes;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'title', 'path', 'type', 'activity', 'format', 'anonymous', 'views', 'reports', 'accepted_at', 'deleted_at', 'course_id', 'user_id', 'period_id', 'file_extension_id', 'license_id'];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'type' => DocumentTypeEnum::class,
        'activity' => ActivityEnum::class,
        'format' => FormatEnum::class,
    ];

    /**
     *  Get the course that owns the document.
     */
    public function course(): BelongsTo
    {
        return $this->belongsTo(Course::class);
    }

    /**
     *  Get the user that owns the document.
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     *  Get the period that owns the document.
     */
    public function period(): BelongsTo
    {
        return $this->belongsTo(Period::class);
    }

    /**
     *  Get the file extension that owns the document.
     */
    public function fileExtension(): BelongsTo
    {
        return $this->belongsTo(FileExtension::class);
    }

    /**
     *  Get the license that owns the document.
     */
    public function license(): BelongsTo
    {
        return $this->belongsTo(License::class);
    }

    public function isFile() {
        return $this->format->value == FormatEnum::FILE->value;
    }

    public function isLink() {
        return $this->format->value == FormatEnum::LINK->value;
    }

    /**
     * Scope a query to only include documents of a given type.
     */
    public function scopeFilterBySeason($query, $seasonCodes)
    {
        if ($seasonCodes !== null && !in_array('all', $seasonCodes)) {
            $query->whereHas('period.season', function ($query) use ($seasonCodes) {
                $query->whereIn('code', $seasonCodes);
            });
        }
    }
}
