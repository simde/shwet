<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use App\Enums\Season as SeasonEnum;

class Season extends Model
{
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['code', 'label'];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'code' => SeasonEnum::class,
    ];

    /**
     *  Get the periods for the season.
     */
    public function periods(): HasMany
    {
        return $this->hasMany(Period::class);
    }

    /**
     *  The courses that belong to the season.
     */
    public function courses(): BelongsToMany
    {
        return $this->belongsToMany(Course::class, 'course_seasons');
    }

    /**
     * Scope a query to only include seasons with UV.
     */
    public static function scopeUv($query)
    {
        return $query->whereIn('code', [SeasonEnum::SPRING, SeasonEnum::AUTUMN]);
    }
}
