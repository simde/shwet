<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use App\Enums\CourseType as CourseTypeEnum;

class Course extends Model
{
    use HasFactory;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['code', 'title', 'type'];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'type' => CourseTypeEnum::class,
    ];

    /**
     *  Get the documents for the course.
     */
    public function documents(): HasMany
    {
        return $this->hasMany(Document::class);
    }

    /**
     *  The users that have the course as a favorite.
     */
    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'favourite_courses');
    }

    /**
     *  The specialities that belong to the course.
     */
    public function specialities(): BelongsToMany
    {
        return $this->belongsToMany(Speciality::class, 'course_specialities');
    }

    /**
     *  The seasons that belong to the course.
     */
    public function seasons(): BelongsToMany
    {
        return $this->belongsToMany(Season::class, 'course_seasons');
    }

    /**
     * Scope a query to only include current courses (not archived).
     */
    public function scopeOnlyCurrent($query)
    {
        return $query->whereNull('archived_at');
    }

    /**
     * Scope a query to only include archived courses.
     */
    public function scopeOnlyArchived($query)
    {
        return $query->whereNotNull('archived_at');
    }

    /**
     * Scope a query to only include courses of a given type.
     */
    public function scopeFilterBySeason($query, $seasonCodes)
    {
        if ($seasonCodes !== null && !in_array('all', $seasonCodes)) {
            $query->whereHas('seasons', function ($query) use ($seasonCodes) {
                $query->whereIn('code', $seasonCodes);
            });
        }
    }

    /**
     * Scope a query to only include courses of a given type.
     */
    public function scopeFilterByType($query, $types)
    {
        if ($types !== null && !in_array('all', $types)) {
            $query->whereIn('type', $types);
        }
    }

    /**
     * Scope a query to only include courses of a given speciality.
     */
    public function scopeFilterBySpeciality($query, $specialityCodes)
    {
        if ($specialityCodes !== null && !in_array('all', $specialityCodes)) {
            $query->whereHas('specialities', function ($query) use ($specialityCodes) {
                $query->whereIn('code', $specialityCodes);
            });
        }
    }

    /**
     * Scope a query to only include courses of a given status.
     */
    public function scopeFilterByStatus($query, $statuses)
    {
        if ($statuses !== null && !in_array('all', $statuses) && !(in_array('archived', $statuses) && in_array('current', $statuses))) {
            if (in_array('archived', $statuses)) {
                $query->onlyArchived();
            }
            elseif (in_array('current', $statuses)) {
                $query->onlyCurrent();
            }
        }
    }
}