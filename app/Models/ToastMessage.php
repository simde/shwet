<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class ToastMessage extends Model
{
    use HasFactory;

    /**
     * @var string
     */
    protected $message;

    /**
     * @var App\Enums\ToastMessage
     */
    protected $type;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['message', 'type'];

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);
        $this->message = $attributes['message'] ?? null;
        $this->type = $attributes['type'] ?? null;
    }
}
