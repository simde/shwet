<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use App\Enums\Role as RoleEnum;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory;

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The data type of the primary key ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'firstname', 'lastname', 'email', 'role', 'dark_theme', 'language', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'role' => RoleEnum::class,
    ];

    /**
     *  Check if the user is an admin.
     */
    public function isAdmin(): bool
    {
        return $this->role->value == RoleEnum::ADMINISTRATOR->value;
    }

    /**
     *  Check if the user is a moderator.
     */
    public function isModerator(): bool
    {
        return $this->role->value == RoleEnum::MODERATOR->value;
    }

    /**
     *  Get the documents for the course.
     */
    public function documents(): HasMany
    {
        return $this->hasMany(Document::class);
    }

    /**
     *  The courses that belong to the user.
     */
    public function courses(): BelongsToMany
    {
        return $this->belongsToMany(Course::class, 'favourite_courses');
    }

    public function getFullName(): string
    {
        return $this->firstname . ' ' . $this->lastname;
    }
}
