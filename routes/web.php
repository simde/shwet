<?php

use App\Http\Controllers\CourseController;
use App\Http\Controllers\DocumentController;
use App\Http\Controllers\DocumentReportController;
use App\Http\Controllers\FavouriteCourseController;
use App\Http\Controllers\SpecialityController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::middleware('auth')->group(function () {
    Route::get('/', function () {
        return view('welcome');
    })->name('welcome');

    Route::get('/courses', [CourseController::class, 'index'])
        ->name('courses.index');

    Route::get('/courses/{code}', [CourseController::class, 'show'])
        ->name('courses.show');

    Route::get('/courses/{code}/{document}', [DocumentController::class, 'show'])
        ->name('documents.show');

    Route::post('/add-favourite-course/{course}', [FavouriteCourseController::class, 'addFavouriteCourse'])
        ->name('add-favourite-course');

    Route::post('/remove-favourite-course/{course}', [FavouriteCourseController::class, 'removeFavouriteCourse'])
        ->name('remove-favourite-course');

    Route::post('/documents', [DocumentController::class, 'store'])
        ->name('documents.store');

    Route::post('/documents/edit', [DocumentController::class, 'edit'])
        ->name('documents.edit');

    Route::post('/delete-document', [DocumentController::class, 'delete'])
        ->name('documents.delete');

    Route::post('/download-selected-documents', [DocumentController::class, 'downloadSelectedDocuments'])
        ->name('download-selected-documents');

    Route::get('/my-documents', function () {
        return view('my-documents');
    })->name('my-documents');

    Route::get('/document-reports', [DocumentReportController::class, 'index'])
        ->name('document-reports.index');

    Route::post('/document-reports', [DocumentReportController::class, 'store'])
        ->name('document-reports.store');

    Route::patch('/document-reports/{id}', [DocumentReportController::class, 'update'])
        ->name('document-reports.update');

    Route::post('/update-dark-theme-preference', [UserController::class, 'updateDarkThemePreference'])
        ->name('update-dark-theme-preference');

    Route::get('/language/{locale}', function ($locale) {
        app()->setLocale($locale);
        session()->put('locale', $locale);

        UserController::updateLanguagePreference($locale);

        return redirect()->back();
    });

    Route::get('/legal-notices', function () {
        return view('legal-notices');
    })->name('legal-notices');

    Route::get('/privacy-policies', function () {
        return view('privacy-policies');
    })->name('privacy-policies');

    Route::get('/sitemap', function () {
        return view('sitemap');
    })->name('sitemap');
});
Route::prefix('/admin')->name('admin.')->middleware('admin')->group(function () {
    Route::get('document-reports', [DocumentReportController::class, 'index'])
        ->defaults('admin', true)
        ->name('document-reports.index');

    Route::get('/crawl-diplomes', [SpecialityController::class, 'manageSpecialities'])
        ->name('crawl-diplomes');

    Route::get('/crawl-uvs', [CourseController::class, 'manageUVs'])
        ->name('crawl-uvs');
});

require __DIR__.'/auth.php';
