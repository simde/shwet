# Carnet de bord


## Infos
* Réu mardi matin (9h au GI)

## Boîte à idées
### Interface
* UVs des alternants
* Nombre de documents pour chaque activité
* Boutons pour nouvel onglet, télécharger et un pour télécharger le dossier
* Plusieurs langues (https://laravel.com/docs/10.x/localization#using-short-keys, https://lokalise.com/blog/laravel-localization-step-by-step/)
* Plusieurs thèmes ? (sombre, claire)
* Modification des infos d'un document après sa publication
* Dropdown account header avec : lien vers oauth, mes documents, mes statistiques, changement langues, taille police, deconnexion
* Cases à cocher pour choisir les documents que l'on souhaite télécharger
* Compteur de fichiers sur la page d'accueil

### BDD
* Longueur des types
* Table extensions de fichiers acceptées (.exe, .zip, .pdf, ...) récupérer anciennes puis message pour ajouter nouvelle
* Rôle utilisateur (admin/user)
* Supprimer dates exams
* Noter quand l’uv est fermée/archivée
* Masquage des commentaires et documents

### Accessibilité
* Contrastes de couleur pour les daltoniens
* Proposer sur la page d’accueil de changer la taille de la police pour les personne malvoyantes / dyslexiques

### Autres
* Weekpost BDE au moment des médians et finaux
* Check vieux catalogue uvs (jaune un peu moche) si refus API
* Fonctionnalités premium pour les cotisants
* Ajouter la date de début et de fin des médians/finaux pour les priorisée sur la vue d'une UV en fonction de l'avancement dans le semestre.
* Notifications administrateurs pour ajout de documents et signalement
* Formulaire de contact vers le SiMDE

### Questions
* Modération dans les commentaires au moins (dans les documents ?)


## Semaine 1
### Compte-rendu réunion
* Documentation -> tout sur le git
* Seeders (jeux de données) à mettre aussi sur le git
* Apprendre comment fonctionne Eloquent (ORM) et Laravel
* hackaton et travail ensemble
* 8,33h/semaine
* Utiliser les milestones (grosse tâches) et issues
* Carnet de bord sur le git (chaque semaine, on met pour chacun sur quoi on a avancé, les idées qu’on a eu, ce qu’on va faire ensuite)
* Limiter l’injection manuelle de données
* A faire :
    * Comment l’utilisateur interagit avec l’application
    * Définir les tâches avec priorités
    * Modèle de données
    * Auto-formation en laravel
    * Créer milestones et issues
    * Demande au 5000 pour les UVs (ou Service d’Administration des Etudes) Levallois
    * GoogleForm pour nouvelles fonctionnalités (voir avec Flo pour post fb et insta)

## Semaines 2-3
### Tâches effectuées
* Use Case Diagram (Q+E)
* Création Documentation (Q+E)
* Création Carnet de bord (Q)
* Définir les tâches avec priorités (Q+E)
* Créer milestones et issues (Q+E)
* Workflow git (E)
* Poker planning pour la durée des tâches (Q+E)
* Création Migrations, Models, Factories, Seeders, Controlers pour Activity, Course, DocumentType, FileExtension, FileFormat, Period, Program, User (Q+E)

### Compte-rendu réunion
* A faire :
    * Revoir schéma de données relationnel
    * Revoir workflow git

## Semaine 4
### Tâches effectuées
* Maquettes (E)
* Maj Workflow git (Q)
* Développement du crawler (Q - 10h)
* Controllers des tables Program et Course (Q)
* Maj MLD (Q)

### Compte-rendu réunion
* Présentation avancement crawler catalogue et modèles
* Discussion autour de la maquette pour le rendu final

## Semaine 5
### Tâches effectuées
* Mise à jour des migrations et modèles Course et Program pour suivre la mise à jour SR (E)
* Ajout des commentaires dans les migrations pour toutes les colonnes et les tables Course et Program (Q)
* Ajout de la possibilité de remplir Courses et Programs (Q)
* Transformation du crawler en FastAPI (Q)
* Vues basiques courses.index et courses.show (Q)
* Creation du controller pour l'API et mise à jour de ceux pour Course et Program pour utiliser l'API (Q)
* Configuration Flowbite avec Tailwind pour Laravel (Q)
* Ajout des routes pour récupérer les données depuis l'API UTCrawl (Q)

### Problèmes rencontrés
* Temps d'execution et de sortie trop bas
* Vue et données renvoyées par l'API fonctionnelles mais pas en même temps
* Filtres non fonctionnels
* Méthode isModified() non fonctionnelle

### Compte-rendu réunion
* Démo UTCrawl, vues courses basiques et Figma (modal dépôt de fichier)
* Discussion autour du stockage des fichiers

## Semaine 6-7
### Tâches effectuées
* Ajout des couleurs de la maquette Figma et de la police d'écriture du Logo (Q)
* Ajout de la fonction de sélection des documents à télécharger (Q)
* Boutons "Tout sélectionner" pour les filtres rendus fonctionnels (Q)
* Correction de relations oubliées et d'attributs non remplisables (Q)
* Modification de DocumentFactory pour stocker les liens externes et pour suivre la nouvelle arborescence (/storage/app/public/documents/id-uv-type-activity-"periods.code"."file_extensions.suffix") (Q)
* Ajout d'un compte administrateur dans UserSeeder et mise à jour des autres (Q)
* Nouvelle route pour télécharger les documents sélectionnés (Q)
* Création de la bannière d'information (Q)
* Traduction de vocabulaire utilisé avec le module de langue (Q)
* Nouveaux logos Shwet au format svg (Q)

### Compte-rendu réunion
* Stocker l'année des semestres en entier pour une plus longue durée de vie
* Afficher les semestres et conserver les filtres dans la vue courses.index

## Semaine 8
### Tâches effectuées
* Mise à jour de la vue welcome (Q)
* Création de vue basique pour les erreurs 400, 404 et 500 (Q)
* Avancées sur la vue navigation et mise à jour de la vue app pour afficher la bannière d'information et la police d'écriture du logo (Q)
* Création d'un composant basique comme modal pour les commentaires (Q)
* Développement de la vue courses.index avec trois filtres pour la branche, le type et le semestre avec un choix Tout séléctionner (Q)
* Développement de la vue courses.show avec un accordéon par activité affichant le nombre de documents par activité, le semestre du document, le type, les boutons pour télécharger, afficher les commentaires et ouvrir dans un nouvel onglet et le système de téléchargement d'une sélection (Q)

### Compte-rendu réunion
* Avoir une version présentable fin Mai pour tester et débogguer le site

## Semaine 9-10-11
### Tâches effectuées
* Développement du formulaire d'ajout de documents (Q)
* Développement du header avec les différentes catégories (E)
* Limiter les extensions de fichiers avec celles de la BDD (Q)
* Développement des Toast Messages (E)
* Ajout de l'autocomplétion de la barre de recherche du header (E)
* Schéma du déroulement de la connexion avec l'Oauth (E)
* Développement du la connexion avec l'OAuth (Q)
* Ajout/suppression d'UV à ses favorites (E)
* Affichage des UVs favorites
* Transformation des enums en BDD en propre classe Enum avec leurs méthodes (Q)
* Formulaire d'ajout de document depuis la page d'une UV autocomplétée (Q)
* Développement de la page 'Mes documents' (E)

### Compte-rendu réunion
* Mewen MICHEL pas MICHELIN
* Stocker le thème (sombre/clair) et la langue en BDD
* Modifier l'ordre des branches TC avant car effectif plus important
* Gérer animation des checkboxes à la sélection des documents
* Essayer de trouver une alternative au rechargement de page quand une uv est ajoutée aux favorites car trop long
* Emoji à remplacer par des svg
* Voir comment modifier la limite du php.ini pour l'upload de documents au moment du déploiement
* Suppression des pdfs du drive en les ajoutant directement sur Shwet dans une activité drive, mettre si possible les fichiers collaboratifs (Google Docs, etc) avec des liens
* Retirer les Toast Messages après 5 secondes pour un succès et mais laisser indéfiniement pour les erreurs
* Fusionner les deux Toast Messages pour le crawl d'UVs en un seul
* Peut être modifier la scrollbar

## Semaine 12
### Tâches effectuées
* Formulaire d'ajout de document plus joli, responsive et avec des select2 (Q)

### Compte-rendu réunion
* Ne pas autoriser à se connecter les utilisateurs avec un provider différent du CAS
* Trouver une solution lorsque nous n'affichons que les initiales pour les prénoms composés
* Ajouter une aide à la publication des documents

## Semaine 13
### Tâches effectuées
* Ajout d'une aide des différents champs de la publication d'un document (E)
* Développement de la modification de documents (E)
* Développemnt des signalements de documents (E)
* Commencement du développement des fonctions Python pour la migration des documents du drive (Q)

### Compte-rendu réunion
* Régler les bugs liés à l'OAuth et aux signalements
* Réaliser le footer
* Rédiger la documentation
* Ajouter le compteur de signalements en attente

## Semaine 14
### Tâches effectuées
* Ajout des dates de commentaire et réponse des signalements (E) 
* Correction du bug Invalid State de l'OAuth (Q)
* Développement des fonctions Python pour la migration des documents actuels (Q)

### Compte-rendu réunion
