﻿# SHWET - RAPPORT
BOYER Quentin - TC04   
DEWULF Eliot - GI04  
Semestre Printemps 2024

<p align="center" width="100%">
    <img width="100px" src="../resources/img/LogoShwetBde.png" alt="Logo Shwet">
</p>

## Table des matières

<!-- TOC -->
- [SHWET - RAPPORT](#shwet---rapport)
  - [Table des matières](#table-des-matières)
  - [Introduction](#introduction)
  - [Cahier des charges](#cahier-des-charges)
    - [Acteurs](#acteurs)
    - [Diagramme de cas d’utilisation](#diagramme-de-cas-dutilisation)
    - [Cas d’utilisation](#cas-dutilisation)
    - [Backlog](#backlog)
  - [Maquettage et StoryBoard](#maquettage-et-storyboard)
    - [Wireframes](#wireframes)
    - [Schémas (storyboard)](#schémas-storyboard)
    - [Maquettes](#maquettes)
  - [Base de données](#base-de-données)
    - [Diagramme UML](#diagramme-uml)
    - [Diagramme MLD](#diagramme-mld)
  - [Gestion de projet](#gestion-de-projet)
    - [Méthodologie](#méthodologie)
    - [Planification](#planification)
  - [Accessibilité et Inclusivité](#accessibilité-et-inclusivité)
<!-- TOC -->

## Introduction

Shwet est une application web développée par le Service informatique de la Maison des Étudiants (SiMDE), association utcéenne qui gère l’ensemble du parc applicatif destiné à la vie associative de l’UTC. qui a pour objectif le partage de ressources liées aux UV proposées à l’UTC. Ces ressources peuvent être de différentes nature (notes manuscrites, notes dactylographiées), sur différents supports (fichier, lien vers une autre ressource) et de différents types de fichiers (.pdf, .docx, .pptx, .zip, …).

La version actuelle de Shwet a vu le jour lors du Hackathon UTC de 2015 par Nicolas SZEWE et Mewen MICHELEN. Aujourd’hui, malgré son utilité indubitable pour tous les étudiants de l’école d’ingénieur de Compiègne, les documents publiés pour chaque UV sont de moins en moins nombreux, et sont parfois même obsolètes.

L’objectif de cette TZ-A est de faire une refonte de l’application Shwet pour ajouter de **nouvelles fonctionnalités**, **moderniser l’interface graphique**, et prendre en compte de nouveaux enjeux tels que l’**accessibilité**, l’**inclusivité**, le **responsive** et la **sécurité**.

## Cahier des charges
### Acteurs

Avant la refonte, Shwet ne comptait qu’un seul type d’acteur : l’*utilisateur* de base. Celui-ci pouvait soit déposer un document, soit en consulter, soit accéder à des statistiques. A présent, cet utilisateur de base pourra réaliser des actions supplémentaires, telles qu’une barre de favoris pour certaines UV par exemple.

Un nouveau type d’utilisateur est également ajouté : l’*administrateur*. Il pourra réaliser les mêmes parcours que l’utilisateur de base, mais pourra en plus masquer des commentaires ou ajouter des dates de médians/finaux.

### Diagramme de cas d’utilisation

![Use Case Diagram](../resources/img/UseCaseDiagram.png)

Le diagramme de cas d’utilisation a été réalisé avec LucidCharts ([version en ligne](https://lucid.app/lucidchart/6562e9bc-6fd2-47aa-96d1-cdddd1b00f6a/edit?existing=1&token=ad4a2f6caac630b0c223e00016c5328fc94c78e135517367582812380bc53ddc-eml%3Deliotdewulf91%2540gmail.com%26ts%3D1708887157%26uid%3D169039291&docId=6562e9bc-6fd2-47aa-96d1-cdddd1b00f6a&shared=true&invitationId=inv_053b0d5e-8357-41ea-8d7a-2b8d327bcc1a&page=.Q4MUjXso07N))

### Cas d’utilisation

| Description                                            | Acteur(s)      | Description                                                                                                                                                                                                                                                                                                                                  |
|--------------------------------------------------------|----------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| S'authentifier (connexion, déconnexion)                | Tout le monde  | L'utilisateur doit s'authentifier via le portail d'authentification du SIMDE avec son CAS ou par login/password pour pouvoir accéder à Shwet.                                                                                                                                                                                                |
| Rechercher une UV                                      | Tout le monde  | L'utilisateur peut saisir le code d'une UV ou son intitulé dans le formulaire de recherche pour essayer de trouver une UV en particulier. Il peut aussi faire une recherche avancée avec plus d'informations sur l'UV.                                                                                                                       |
| Consulter les documents d'une UV                       | Tout le monde  | Une fois sur la page d'une UV, l'utilisateur peut ouvrir un document pour le consulter, ou le télécharger. Il peut également télécharger tous les documents d'une section d'une UV d'un coup.                                                                                                                                                |
| Publier des fichiers / liens                           | Tout le monde  | L'utilisateur peut accéder à un formulaire pour déposer un document (fichier ou lien vers un document) en précisant à minima le semestre, l'UV, si le fichier est rédigé à la main ou dactylographié, s'il s'agit d'une correction ou d'un sujet, et enfin le contenu auquel il est lié (Final, Médian, TP, TD, Fiche de révision, Rapport). |
| Changer la langue de l'interface (Français, Anglais)   | Tout le monde  | L'utilisateur doit pouvoir sélectionner la langue du site entre le français et l'anglais pour une meilleure accessibilité.                                                                                                                                                                                                                   |
| Signaler le contenu d'un document                      | Tout le monde  | Si un document semble erroné, incomplet ou illisible, un utilisateur doit pouvoir le signaler aux administrateurs et aux autres utilisateurs.                                                                                                                                                                                                |
| Ajouter / Retirer une UV de ses favoris                | Tout le monde  | L'utilisateur dispose d'une liste d'UVs favorites qui sont mises en évidences. Il peut ajouter et supprimer n'importe qu'elle UV de ses favoris.                                                                                                                                                                                             |
| Commenter un document                                  | Tout le monde  | L'utilisateur peur ajouter un commentaire à propos d'un document spécifique, s'il a besoin de précisions sur son contenu ou s'il souhaite éclaircir un point obscur.                                                                                                                                                                         |
| Signaler le contenu d'un commentaire                   | Tout le monde  | Si un commentaire est déplacé ou offensant, l'utilisateur peut le signaler aux administrateurs qui regarderont ensuite s'il doit être masqué.                                                                                                                                                                                                |
| Visualiser les statistiques                            | Tout le monde  | L'utilisateur peut consulter des statistiques sur les document publiés sur Shwet et les documents qu'il a lui-même publié.                                                                                                                                                                                                                   |
| Masquer un commentaire                                 | Administrateur | L'administrateur peut masquer un commentaire qui est déplacé ou offensant.                                                                                                                                                                                                                                                                   |

Les cas d'utilisation correspondent aux milestones du projet de refonte de Shwet. 

### Backlog

| Milestones                        | Tâches                                                                  | Priorités | Durées estimées (en h) | Issues                                               | 
|-----------------------------------|-------------------------------------------------------------------------|:---------:|:----------------------:|:----------------------------------------------------:|
| Cadrage et Planification          | Rédaction du cahier des charges (use case)                              |    1      |          10h           | [#2](https://gitlab.utc.fr/simde/shwet/-/issues/2)   | 
| Cadrage et Planification          | Sondage, récupération du besoin                                         |    1      |           2h           | [#48](https://gitlab.utc.fr/simde/shwet/-/issues/48) | 
| Cadrage et Planification          | Initialisation de l'espace de travail (gitlab)                          |    1      |           1h           | [#49](https://gitlab.utc.fr/simde/shwet/-/issues/49) |
| Cadrage et Planification          | Workflow git                                                            |    1      |           2h           | [#50](https://gitlab.utc.fr/simde/shwet/-/issues/50) |
| Cadrage et Planification          | Rédaction de la backlog et estimation                                   |    2      |           4h           | [#4](https://gitlab.utc.fr/simde/shwet/-/issues/4)   |
| Conception                        | SR de la BDD                                                            |    3      |          10h           | [#46](https://gitlab.utc.fr/simde/shwet/-/issues/46) |
| Conception                        | UML diagramme de classes                                                |    4      |           3h           | [#1](https://gitlab.utc.fr/simde/shwet/-/issues/1)   |
| Conception                        | Création de wireframes, storyboard, maquettage                          |    4      |          30h           | [#45](https://gitlab.utc.fr/simde/shwet/-/issues/45) |
| Conception                        | Identification de la charte graphique                                   |    3      |           4h           | [#6](https://gitlab.utc.fr/simde/shwet/-/issues/6)   |
| Montée en compétences             | Montée en compétences Laravel, Tailwind (Flowbite), Gitlab              |    0      |          30h           | [#5](https://gitlab.utc.fr/simde/shwet/-/issues/5)   |
| Création du projet                | Création projet Laravel                                                 |    5      |           2h           | [#22](https://gitlab.utc.fr/simde/shwet/-/issues/22) |
| Création du projet                | Implémentation SQL du modèle de données                                 |    6      |           2h           | [#7](https://gitlab.utc.fr/simde/shwet/-/issues/7)   | 
| Création du projet                | Initialisation des modèles                                              |    6      |           2h           | [#53](https://gitlab.utc.fr/simde/shwet/-/issues/53) |
| Création du projet                | Initialisation du plugin langues                                        |    5      |           1h           | [#52](https://gitlab.utc.fr/simde/shwet/-/issues/52) |
| Création du projet                | Header                                                                  |    6      |           8h           | [#54](https://gitlab.utc.fr/simde/shwet/-/issues/54) | 
| Création du projet                | Footer                                                                  |    6      |           3h           | [#55](https://gitlab.utc.fr/simde/shwet/-/issues/55) | 
| Authentification                  | Lien avec le oauth du SIMDE                                             |    7      |           4h           | [#8](https://gitlab.utc.fr/simde/shwet/-/issues/8)   | 
| Recherche UV                      | Formulaire de recherche du header                                       |    7      |           2h           | [#43](https://gitlab.utc.fr/simde/shwet/-/issues/43) | 
| Recherche UV                      | Page de recherche avancée                                               |    8      |          10h           | [#44](https://gitlab.utc.fr/simde/shwet/-/issues/44) | 
| Consultation documents            | Description de l'UV                                                     |    7      |           1h           | [#31](https://gitlab.utc.fr/simde/shwet/-/issues/31) | 
| Consultation documents            | Pour chaque section, afficher les documents                             |    7      |           8h           | [#32](https://gitlab.utc.fr/simde/shwet/-/issues/32) | 
| Consultation documents            | Télécharger un document, télécharger tous les documents d'une section   |    7      |           2h           | [#25](https://gitlab.utc.fr/simde/shwet/-/issues/25) | 
| Consultation documents            | Consulter les documents publiés par l'utilisateur (mes documents)       |    8      |           4h           | [#24](https://gitlab.utc.fr/simde/shwet/-/issues/24) | 
| Publication documents / liens     | Formulaire de publication d'un document depuis le header (modal)        |    7      |          10h           | [#33](https://gitlab.utc.fr/simde/shwet/-/issues/33) | 
| Publication documents / liens     | Adapter le formulaire de publication depuis la page d'une UV            |    8      |           1h           | [#34](https://gitlab.utc.fr/simde/shwet/-/issues/34) | 
| Changement langue                 | Dropdown dans le header section utilisateur                             |    6      |           2h           | [#36](https://gitlab.utc.fr/simde/shwet/-/issues/36) | 
| Signalement contenu               | Ajout bouton pour signaler un document                                  |    9      |           2h           | [#37](https://gitlab.utc.fr/simde/shwet/-/issues/37) | 
| Signalement contenu               | Ajout bouton pour signaler un commentaire                               |    10     |           1h           | [#38](https://gitlab.utc.fr/simde/shwet/-/issues/38) | 
| Signalement contenu               | Notifier l'administrateur du signalement d'un document                  |   idée    |           -            | [#39](https://gitlab.utc.fr/simde/shwet/-/issues/39) | 
| UVs favorites                     | Afficher favoris                                                        |    7      |           5h           | [#35](https://gitlab.utc.fr/simde/shwet/-/issues/35) | 
| UVs favorites                     | Ajouter favoris                                                         |    8      |           1h           | [#27](https://gitlab.utc.fr/simde/shwet/-/issues/27) | 
| UVs favorites                     | Retirer des favoris (bouton clic droit + bouton page UV)                |    9      |           2h           | [#28](https://gitlab.utc.fr/simde/shwet/-/issues/28) | 
| Commenter un document             | Ajouter, un commentaire (modal)                                         |    9      |          15h           | [#40](https://gitlab.utc.fr/simde/shwet/-/issues/40) | 
| Commenter un document             | Supprimer (masquer) un commentaire                                      |    10     |           3h           | [#41](https://gitlab.utc.fr/simde/shwet/-/issues/41) | 
| Visualisation des statistiques    | Section mes statistiques (celles de l'utilisateur)                      |    7      |          10h           | [#29](https://gitlab.utc.fr/simde/shwet/-/issues/29) | 
| Visualisation des statistiques    | Section statistiques générales celles de tous les utilisateurs de Shwet |    7      |           5h           | [#30](https://gitlab.utc.fr/simde/shwet/-/issues/30) | 
| Masquage d'un commentaire (Admin) | Bouton pour masquer un commentaire                                      |    10     |           1h           | [#42](https://gitlab.utc.fr/simde/shwet/-/issues/42) | 
| Documentation                     | Rédaction du [carnet de bord](logbook.md)                               |    10     |          20h           |                                                      | 
| Documentation                     | Rédaction du rapport TSH                                                |    10     |          10h           |                                                      | 
| Documentation                     | Rédaction du [README](../README.md)                                     |    10     |           4h           |                                                      | 
| Marge d'erreur                    | Tâches imprévues                                                        |    -      |          13h           |                                                      | 
| **TOTAL**                         | -                                                                       |    -      |        **250h**        |                                                      |
## Maquettage et StoryBoard
### Wireframes
### Schémas (storyboard)
### Maquettes

## Base de données
### Diagramme UML
![Diagramme UML](database/uml_diagram.png)

### Diagramme MLD
![Diagramme MLD](database/mld_diagram.png)


##  Gestion de projet
### Méthodologie
- Planification et chiffrage
- Tableau Kanban avec les “issues” qui représentent la backlog
- Réunions hebdomadaires avec M. Bonnet
- Suivi avec des graphiques d’avancement (burndown charts)

### Planification
- diagramme de gantt ?
- backlog dans le temps

## Accessibilité et Inclusivité
